CC.Tabs.Tab.Number = Ext.extend(CC.Tabs.Base, {
    create: function () {

		var numberContent = {
			id: 'numberContent',
			border: false,
			autoScroll: true,
			style: {
					//'padding-left': '30px',
					'padding-left': '20px',
					'padding-top': '10px'
				},
			layout: 'table',
			layoutConfig: {
				tableAttrs: {
					style: {
						"width": "580px",
						//border: 'none',
						//margin: '20px 0px 0px 30px'
					}
				},
				columns: 2
			},
			items: [{
				xtype: 'panel',
				//labelWidth: 74,
				id: 'prefixSufix',
				layout: 'table',
				layoutConfig: {
					tableAttrs: {
						style: {
							'width': '100%',
							'border': 'none',
							'padding-right': '20px'//,
							//'height': '103px'
						}
					},
					columns: 2
				},
				//defaults: { border: false },
				items: [{
					xtype: 'label',
					text: CC.msgs.nmbr161,
					cls: 'subHead',
					colspan: 2,
					ccHelp: CC.tips.nmbr161
				},{
					xtype: 'label',
					//text: 'Prefix'
					html: CC.msgs.nmbr162,
					cls: 'x-form-item-sub'
				},
				{
					xtype: 'textfield',
					id: 'txtPrefix',
					listeners: {
					    change: function (_this, newVal) {
					        CC.med.publish(CC.util.tabsActivate('nmbrUpdt'), { id: this.id, val: newVal });
					    },
					    specialkey: function (field, e) {
					        if (e.getKey() == e.ENTER) {
					            CC.med.publish(CC.util.tabsActivate('nmbrUpdt'), { id: this.id, val: field.getValue() });
					        }
					    }
					}
				},{
					xtype: 'label',
					html: CC.msgs.nmbr163,
					cls: 'x-form-item-sub'
				},
				{
					xtype: 'textfield',
					id: 'txtSuffix',
					listeners: {
					    change: function (_this, newVal) {
					        CC.med.publish(CC.util.tabsActivate('nmbrUpdt'), { id: this.id, val: newVal });
					    },
					    specialkey: function (field, e) {
					        if (e.getKey() == e.ENTER) {
					            CC.med.publish(CC.util.tabsActivate('nmbrUpdt'), { id: this.id, val: field.getValue() });
					        }
					    }
					}
				}]
			},{
				xtype: 'panel',
				id: 'roundNo',
				layout: 'table',
				layoutConfig: {
					tableAttrs: {
						style: {
							width: '100%',
							border: 'none',
							'padding-left': '20px',
							'height': '10px'
						}
					},
					columns: 3
				},
				defaults: { border: false },
				items: [{
					xtype: 'label',
					text: CC.msgs.nmbr164,
					cls: 'subHead',
					colspan: 3,
					ccHelp: CC.tips.nmbr162
				},{
					xtype: 'label',
					text: CC.msgs.nmbr165,
					cls: 'x-form-item-sub',
					colspan: 2,
					style: {
						'display': 'inherit'
					}
				},{
					xtype: 'spinnerfield',
					id: 'spnrDcml',
					minValue: 0,
					maxValue: 10,
					width: 88,
					rightLeftBtns: true,
				    //ccTip: 'Show a box with legend item for each data series. Legend helps viewers in associating dataplots with their respective series.'
					listeners: {
					    change: function (t, n, o) {
					        ////Console.log(o, n);
					        //
					        if (n === this.spinner.field.maxValue)
					            this.spinner.trigger.setStyle('background-position', '-216px 0px');
					        else
					            this.spinner.trigger.setStyle('background-position', '-182px 0px');
					        if (n === this.spinner.field.minValue)
					            this.spinner.triggerLeft.setStyle('background-position', '-233px 0px');
					        else
					            this.spinner.triggerLeft.setStyle('background-position', '-199px 0px');
					        // if(v < this.field.minValue)
					        //     this.triggerLeft.setStyle('background-position','-233px 0px');
					        // else if(v > this.field.maxValue)
					        //     this.trigger.setStyle('background-position','-216px 0px');

					        CC.med.publish(CC.util.tabsActivate('nmbrUpdt'), { id: this.id, val: n });
					    }
					}
				}]
			},{
				xtype: 'panel',
				//labelWidth: 74,
				id: 'decimalStep',
				layout: 'table',
				layoutConfig: {
					tableAttrs: {
						style: {
							'width': '100%',
							'border': 'none',
							'padding-right': '20px',
							'padding-top': '5px'//,
							//'height': '103px'
						}
					},
					columns: 1
				},
				//defaults: { border: false },
				items: [
				// {
				// 	xtype: 'checkbox',
				// 	id: 'chkDecimal',
				// 	cls: 'css-checkbox elegant sme',
				// 	boxLabel: ' ',
				// 	//width: 15,
				// 	style: {
				// 		'margin-top': '-1px',
				// 		'vertical-align': 'middle'
				// 	},
				// 	listeners:{
				// 		afterrender:function(){
				// 			// Ext.getCmp('chkDecimal').wrap.dom.lastChild.classList.add('css-label','sme');
				// 			// if(Ext.getCmp('chkDecimal').wrap.dom.lastChild.classList){
				// 				// Ext.getCmp('chkDecimal').wrap.dom.lastChild.classList.add('css-label');
				// 				// Ext.getCmp('chkDecimal').wrap.dom.lastChild.classList.add('sme');
				// 				// Ext.getCmp('chkDecimal').wrap.dom.lastChild.classList.add('all-images');
				// 			// }
				// 			// else
				// 				// Ext.getCmp('chkDecimal').wrap.dom.lastChild.className += ' css-label sme all-images';
				// 			CC.util.addCssClass(Ext.getCmp('chkDecimal').wrap.dom.lastChild, ['css-label', 'sme', 'all-images']);
				// 		}
				// 	}
				// },
				{
				    xtype: 'label',
				    cls: 'subHead',
				    text: CC.msgs.nmbr166,
				    ccHelp: CC.tips.nmbr163,
				    style: {
				        // 'margin-left': '7px'//,
				        // 'vertical-align': 'middle'
				    }
				}
				// {
					// xtype:'toggleField',
					// //id: 'tglLabels',
					// checked: true,
					// boxLabel: "Number Format",
					// boxLabelCss: "subHead",
					// textLeft: true,
					// style: {
						// "margin-Bottom": '0px',
						// "width": '100%'
					// },
					// //width: 250,
					// colspan: 2,
					// listeners:{
						// scope: this,
						// 'toggle': function(ele){
							// //var nxt = $(ele.el.dom).closest("tr").next();
							// var nxt = $(ele.el.dom).closest("tr");
							// //while(nxt.length > 0){
							// while((nxt = nxt.next()).length > 0){
								// if(!ele.checked){
									// nxt.find("input,label").addClass("x-item-disabled");
									// nxt.find("input,label").prop("disabled", true);
								// }
								// else{
									// nxt.find("input,label").removeClass("x-item-disabled");
									// nxt.find("input,label").prop("disabled", false);
								// }
								// //nxt = nxt.next();
							// }
						// }
					// }
				// }
				,//{},
				{
				    xtype: 'fieldset',
				    cls: 'label-no-image',
				    items: [{
				        border: false,

				        xtype: 'radiogroup',
                        id: 'rdDecimalSep',
                        cls: 'x-form-item-sub',
				        itemCls: 'x-radiogroup',
				        hideLabel: true,
				        columns: 1,
				        items: [{
				            id: 'rdRegular',
				            name: 'rb-col',
				            columnWidth: 1,
				            boxLabel: CC.msgs.nmbr167,
				            inputValue: 1 //,
				            // style:{"margin-top:3px;"},
				            //checked: true
				        }, {
				            id: 'rdEurope',
				            name: 'rb-col',
				            columnWidth: 1,
				            boxLabel: CC.msgs.nmbr168,
				            inputValue: 2,
				            // style:{"margin-top:3px;"}
				        }],
				        listeners: {
				            afterrender: function () {
				                // $('#rdRegular').parent().css('top','2px');
				                $('#rdEurope').parent().css('margin-top', '5px');
				            },
				            change: function (rg, checked) {
				                //if(checked.id == 'rdEurope')
				                CC.med.publish(CC.util.tabsActivate('nmbrUpdt'), { id: CC.util.target('nmbrRd'), val: (checked.id == 'rdEurope') });
				            }
				        }

				    }]
				}]
			},{
				xtype: 'panel',
				id: 'scaleNo',
				layout: 'table',
				layoutConfig: {
					tableAttrs: {
						style: {
							width: '100%',
							border: 'none',
							'padding-left': '20px'//,
							//'height': '103px'
						}
					},
					columns: 3
				},
				defaults: { border: false },
				items: [
				// {
					// xtype:'toggleField',
					// checked: true,
					// boxLabel: "Scaling Numbers",
					// boxLabelCss: "subHead",
					// textLeft: true,
					// style: {
						// "margin-Bottom": '0px',
						// "width": '100%'
					// },
					// //width: 240,
					// colspan: 3,
					// listeners:{
						// scope: this,
						// 'toggle': function(ele){
							// //var _dom = ele.el.dom;
							// //var nxt = $(ele.el.dom).closest("tr").next();
							// var nxt = $(ele.el.dom).closest("tr");
							// //while(nxt.length > 0){
							// while((nxt = nxt.next()).length > 0){
								// if(!ele.checked){
									// nxt.find("input,label").addClass("x-item-disabled");
									// nxt.find("input,label").prop("disabled", true);
								// }
								// else{
									// nxt.find("input,label").removeClass("x-item-disabled");
									// nxt.find("input,label").prop("disabled", false);
								// }
								// //nxt = nxt.next();
							// }
						// }
					// }
				// }
				{
					xtype: 'checkbox',
					id: 'chkScaling',
					cls: 'css-checkbox elegant sme',
					boxLabel: ' ',
					//width: 15,
					style: {
						'margin-top': '-1px',
						'vertical-align': 'middle'
					},
					listeners:{
						afterrender:function(){
							CC.util.addCssClass(Ext.getCmp('chkScaling').wrap.dom.lastChild, ['css-label', 'sme', 'all-images']);
						},
						check: function(ele, checked){
						    if (ele.el) {
						        var nxt = $(ele.el.dom).closest("tr");
						        while ((nxt = nxt.next()).length > 0) {
						            if (!ele.checked) {
						                nxt.find("input,label").addClass("x-item-disabled");
						                nxt.find("input,label").prop("disabled", true);
						            }
						            else {
						                nxt.find("input,label").removeClass("x-item-disabled");
						                nxt.find("input,label").prop("disabled", false);
						            }
						        }

						        CC.med.publish(CC.util.tabsActivate('nmbrUpdt'), { id: this.id, val: checked });
						    }
						}
					}
				},{
					xtype: 'label',
					text: CC.msgs.nmbr169,
					cls: 'subHead',
					colspan: 2,
					ccHelp: CC.tips.nmbr164
				}
				,{},{
					xtype: 'label',
					text: CC.msgs.nmbr1610,
					cls: 'x-form-item-sub',
					style: {
						'display': 'inherit'
					},
					disabled:true
				},{
					xtype: 'textfield',
					id: 'txtScales',
					disabled: true,
					listeners: {
					    change: function (_this, newVal) {
					        CC.med.publish(CC.util.tabsActivate('nmbrUpdt'), { id: this.id, val: newVal });
					    },
					    specialkey: function (field, e) {
					        if (e.getKey() == e.ENTER) {
					            CC.med.publish(CC.util.tabsActivate('nmbrUpdt'), { id: this.id, val: field.getValue() });
					        }
					    }
					}
				},{},{
					xtype: 'label',
					text: CC.msgs.nmbr1611,
					cls: 'x-form-item-sub',
					style: {
						'display': 'inherit'
					},
					disabled:true
				},{
					xtype: 'textfield',
					id: 'txtScalesVal',
					disabled: true,
					listeners: {
					    change: function (_this, newVal) {
					        CC.med.publish(CC.util.tabsActivate('nmbrUpdt'), { id: this.id, val: newVal });
					    },
					    afterrender: function (_this) {
					        if (Ext.getCmp('chkScaling').getValue()) {

					            var nxt = $(_this.el.dom).closest("tr");
					            //nxt = nxt.next()
					            do {
					                nxt.find("input,label").removeClass("x-item-disabled");
					                nxt.find("input,label").prop("disabled", false);
					            } while ((nxt = nxt.prev()).length > 0);
					        }
					    },
					    specialkey: function (field, e) {
					        if (e.getKey() == e.ENTER) {
					            CC.med.publish(CC.util.tabsActivate('nmbrUpdt'), { id: this.id, val: field.getValue() });
					        }
					    }
					}
				}]
			}]
		};

		return {
            id: 'tabNumber',
            title: CC.msgs.tb114,
			header: false,
			//iconCls: 'x-numbers-icon',
            defaults: { autoScroll: false },
            items: [numberContent]
        }
    },

	validationStep: function () {
        var valid = true;
        return valid;
    },

	activateStep: function () {
	    this.disabled = false;

	    //CC.Tabs.Activate.numbers.activate();
	    CC.med.publish(CC.util.tabsActivate('numbers'));
	}
});
