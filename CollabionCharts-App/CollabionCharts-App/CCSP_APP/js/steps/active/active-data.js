﻿CC.Tabs.Active.prototype.data = {
    activate: function () {
        var data = CC.med.getSetting('dsData');
        var cp = CC.med.currentProvider();
        if (!data.id && !Ext.getCmp('ccList').getStore()) {
            cp.getAllDataSource(function (lists) {
                CC.Stores.Lists.loadData(lists);
                Ext.getCmp("ccList").bindStore(CC.Stores.Lists);
            });
        }
        else if (data.id && !Ext.getCmp('ccList').getValue()) {
            cp.getAllDataSource(function (lists) {

                //Re-check inside the callback function
                if (!Ext.getCmp('ccList').getValue()) {
                    CC.Stores.Lists.loadData(lists);
                    Ext.getCmp("ccList").bindStore(CC.Stores.Lists);

                    Ext.getCmp('ccList').setValue(data.id);

                    CC.med.publish("ccList", { listId: data.id, viewId: data.viewID });
                    CC.med.publish("ccView", { id: data.viewID });
                }
            });
        }
    },
    populateView: function (listId, selViewId) {


        CC.util.toggleProgressBar(true);
        var cp = CC.med.currentProvider();
        cp.getViews(function (views) {
            views.unshift({ 'Id': '-1', 'Title': '--- Select ---' });
            CC.Stores.Views.loadData(views);
            var ccView = Ext.getCmp("ccView");
            ccView.bindStore(CC.Stores.Views);

            if (selViewId)
                ccView.setValue(CC.med.getSetting('dsView'));

            CC.util.toggleProgressBar(false);
            ccView.setDisabled(false);
        }, listId);
    },
    selectFields: function (selViewId, chartConfig, autoConfig) {
        CC.util.toggleProgressBar(true);

        CC.med.currentProvider().getFields(function (fields) {

            CC.med.updateSetting('cacheData', []);

            var errorMsg = '';

            //If the chart is not configured, auto select fields
            if (autoConfig) {
                //resetting the List data only if no setting exist for the current app part
                CC.med.updateSetting('dsFld', []);

                var groupBy = '';
                var groupFields = [];
                var isDataGroupBy = false;
                $.each($(fields.viewQuery), function (index, value) {
                    if (value.nodeName === 'GROUPBY') {
                        $.each($(value.childNodes), function (index, value) {
                            isDataGroupBy = true;
                            groupBy = $(value).attr('Name');
                            return false;
                        });
                    }
                });

                if (isDataGroupBy) {
                    $.each($(fields.aggregations), function (index, value) {
                        groupFields.push($(value).attr('Name'));
                    });
                }

                /* for group by, select the first grouped field on the X axis otherwise look for Title */
                var str = (isDataGroupBy) ? CC.util.selectAutoField(groupBy, true) : CC.util.selectAutoField('Title');

                /* for group by, look for any aggregate/total function otherwise any number field */
                //var num = CC.util.selectAutoField('Number');
                var num = (isDataGroupBy) ? CC.util.selectAutoField(groupFields[0], true) : CC.util.selectAutoField('Currency');

                CC.med.updateSetting('dsId', Ext.getCmp('ccList').getValue());
                CC.med.updateSetting('dsView', selViewId);

                var getDefaultSetting = function (xAxis, yAxis) {
                    return {
                        "xAxisname": xAxis,
                        "yAxisName": yAxis,
                        "showLabels": "0",
                        "showValues": "0",
                        "formatNumber": "1",
                        "formatNumberScale": "1",
                        "formatStyle": "regular"
                    }
                }

                if (num.length > 0) {
                    /* auto number field found */

                    //CC.util.insertListField($.map(CC.Stores.ViewFields.data.items, function (dt) {
                    CC.Tabs.Active.prototype.fields.insertListField($.map(CC.Stores.ViewFields.data.items, function (dt) {
                        if (dt.data.InternalName == str[0].data.InternalName || dt.data.InternalName == num[0].data.InternalName) {
                            return dt.data.InternalName;
                        }
                    }), true);

                    var config = {
                        "xAxis": {
                            "column": str[0].data.InternalName
                        },
                        "series": [{
                            "column": num[0].data.InternalName,
                            "seriesName": num[0].data.Title
                        }],
                    };

                    config["theme"] = "zune";

                    CC.med.updateSetting('config', config);
                    CC.med.updateSetting('cType', CC.util.defaultSingleSeriesChart());

                    //CC.med.updateSetting('chart', {
                    //    "xAxisname": str[0].data.Title,
                    //    "yAxisName": num[0].data.Title,
                    //    "showLabels": "0",
                    //    "showValues": "0",
                    //    "formatNumber": "1",
                    //    "formatNumberScale": "1",
                    //    "formatStyle": "regular"
                    //});

                    CC.med.updateSetting('chart', getDefaultSetting(str[0].data.Title, num[0].data.Title));
                }
                else {
                    /* NO number field found */
                    errorMsg = CC.msgs.err181;
                    CC.med.updateSetting('config', {});
                    CC.util.enableDisableTreeNode(true, 1);

                    var config = {
                        "xAxis": {
                            "column": str[0].data.InternalName
                        },
                        "series": []
                    };

                    config["theme"] = "zune";
                    CC.med.updateSetting('config', config);
                    CC.med.updateSetting('cType', CC.util.defaultSingleSeriesChart());
                    CC.med.updateSetting('chart', getDefaultSetting(str[0].data.Title));

                    CC.med.updateSetting('cacheConfig', CC.util.valueCopyJSON(config));
                }
            }

            if (!errorMsg) {
                CC.med.updateSetting('cacheConfig', CC.util.valueCopyJSON(CC.med.getSetting('config')));
                CC.med.updateSetting('cacheFld', CC.util.valueCopyJSON(CC.med.getSetting('dsFld')));
                CC.med.updateSetting('cacheSingl', CC.util.defaultSingleSeriesChart());

                CC.med.createChart();

                //Ext.getCmp("fldXAxs").bindStore(CC.Stores.ViewFields);
                //Ext.getCmp("fldXAxs").setValue(CC.med.getSetting('xAxis').column);

                if (autoConfig)
                    Ext.getCmp('ccView').setValue(selViewId);

                //CC.Wizard.tabpanel.tabs[0].toggleEnable(true);
            }
            else {
                CC.med.createChart('', errorMsg);
            }

            Ext.getCmp("fldXAxs").bindStore(CC.Stores.ViewFields);
            Ext.getCmp("fldXAxs").setValue(CC.med.getSetting('xAxis').column);

        }, Ext.getCmp('ccList').getValue(), selViewId);
    }
}