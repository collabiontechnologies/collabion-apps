﻿CC.Tabs.Active.prototype.numbers = {
    componentLoaded: false,
    activate: function () {
        //some condition to make it conditional fire
        var decmlVal = Ext.getCmp('spnrDcml').getValue();

        if (decmlVal == "" || isNaN(decmlVal)) {
            //only update the fields on the first activate
            //Need a machanism to track it on data source change will also work if spnrDcml is set to ""

            var chartCosmetics = CC.med.getSetting('chart');

            //prefix/suffix/no. of decimal places
            Ext.getCmp('txtPrefix').setValue(chartCosmetics.numberPrefix);
            Ext.getCmp('txtSuffix').setValue(chartCosmetics.numberSuffix);
            Ext.getCmp('spnrDcml').setValue(chartCosmetics.decimals ? chartCosmetics.decimals : 2);

            //enable scaling
            Ext.getCmp('chkScaling').setValue(chartCosmetics.formatNumberScale == undefined ? "1" : chartCosmetics.formatNumberScale);
            Ext.getCmp('txtScales').setValue(chartCosmetics.numberScaleUnit ? chartCosmetics.numberScaleUnit : 'K');
            Ext.getCmp('txtScalesVal').setValue(chartCosmetics.numberScaleValue ? chartCosmetics.numberScaleValue : 1000);

            //decimal separator
            Ext.getCmp('rdDecimalSep').setValue(chartCosmetics.decimalSeparator ? chartCosmetics.decimalSeparator == "," ? 2 : 1 : 1);

            this.componentLoaded = true;
        }

        CC.util.generateTip();
    },

    update: function (id, val) {
        switch (id) {
            case "txtPrefix":
                CC.med.updateSetting('chart', val, 'numberPrefix');
                break;
            case "txtSuffix":
                CC.med.updateSetting('chart', val, 'numberSuffix');
                break;
            case "spnrDcml":
                CC.med.updateSetting('chart', val, 'decimals');
                break;
            case "chkScaling":
                CC.med.updateSetting('chart', val ? "1" : "0", 'formatNumberScale');
                break;
            case "txtScales":
                CC.med.updateSetting('chart', "1", 'formatNumberScale');
                CC.med.updateSetting('chart', val, 'numberScaleUnit');
                var scaleVal = CC.med.getSetting('chart').numberScaleValue;
                if (!scaleVal) {
                    /* set a default value */
                    CC.med.updateSetting('chart', '1000', 'numberScaleValue');
                    Ext.getCmp('txtScalesVal').setValue('1000');
                }
                break;
            case "txtScalesVal":
                CC.med.updateSetting('chart', "1", 'formatNumberScale');
                CC.med.updateSetting('chart', val, 'numberScaleValue');
                var scales = CC.med.getSetting('chart').numberScaleUnit;
                if (!scales) {
                    /* set a default value */
                    CC.med.updateSetting('chart', 'K', 'numberScaleUnit');
                    Ext.getCmp('txtScales').setValue('K');
                }
                break;
            case CC.util.target('nmbrRd'):
                if (val) {
                    CC.med.updateSetting('chart', ",", 'decimalSeparator');
                    CC.med.updateSetting('chart', ".", 'thousandSeparator');
                }
                else {
                    CC.med.updateSetting('chart', ".", 'decimalSeparator');
                    CC.med.updateSetting('chart', ",", 'thousandSeparator');
                }
                break;
        }
    }
}