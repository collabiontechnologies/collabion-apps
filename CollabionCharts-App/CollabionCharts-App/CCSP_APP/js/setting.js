﻿//********************************************
// App part Properties Setting functions
//
CC.Setting = function () { }

CC.Setting.prototype.LoadFromQueryString = function (p) {

    var dfd = $.Deferred();

    try {
        //var q = getQueryStringArray();
        p.hostWebUrl = decodeURIComponent(CC.util.getQueryStringParameter("SPHostUrl"));
        p.appWebUrl = decodeURIComponent(CC.util.getQueryStringParameter("SPAppWebUrl"));
        p.senderId = decodeURIComponent(CC.util.getQueryStringParameter("SenderId"));
        p.editMode = parseInt(decodeURIComponent(CC.util.getQueryStringParameter("editmode")));
        p.wpId = decodeURIComponent(CC.util.getQueryStringParameter("wpId"));
        p.wpq = decodeURIComponent(CC.util.getQueryStringParameter("wpq"));
        p.hostPage = CC.util.getHostPage();
        //p.scriptBase = p.hostWebUrl + "/_layouts/15/";
        var layoutUrl = _spPageContextInfo.layoutsUrl;
        if (!layoutUrl.startsWith("/"))
            layoutUrl = "/" + layoutUrl;
        if (!layoutUrl.endsWith("/"))
            layoutUrl += "/";
        p.scriptBase = p.hostWebUrl + layoutUrl;

        //p.pageInEdit = SP.Ribbon.PageState.Handlers.isInEditMode();

        dfd.resolve();
    }
    catch (args) {
        CA.med.logError({}, '130', 'ListsNotFetched', args.body ? args.body : args.message ? args.message : args);

        dfd.reject(args);
    }

    return dfd.promise();
}

CC.Setting.prototype.getAppConfig = function (__param) {
    //if (!PM.DataSource || CM.ChartConfig == undefined || !CM.ChartConfig.Configuration || !CM.Cosmetics) {
    //Checks if none of the above properties is not already initialized.

    var dfd = $.Deferred();

    var queryPayLoad = {
        '__metadata': { 'type': 'SP.CamlQuery' },
        'ViewXml': CC.util.getAppSettingViewXml(__param.wpId)
    }

    CC.Data.SPList.prototype.executeRequest(__param.appWebUrl + "/_api/Web/Lists/getbytitle('" + CC.util.getConfigListName() + "')/Items?$select=Id,%20AppPartId,%20Config&$filter=AppPartId%20eq%20'" + __param.wpId + "'")
        //CC.Data.SPList.prototype.executeRequest(__param.appWebUrl + "/_api/Web/Lists/getbytitle('" + CC.util.getConfigListName() + "')/Items")
        //this.executeRequest(documentHeader.param.appWebUrl + "/_api/Web/Lists/getbytitle('" + CC.util.getConfigListName() + "')/Items", "GET", null, queryPayLoad)
            .done(function (data) {

                dfd.resolve(data.d.results);
            })
            .fail(function (args) {


                //var ex = new CC.Exception("App Config Not Found", "Unable to fetch the app config for : " + __param.wpId, args);
                CC.med.logError({}, '131', 'AppPropNotFetched', args.body ? args.body : args.message ? args.message : args);

                dfd.reject(args);
            });

    return dfd.promise();
    //}
}

CC.Setting.prototype.setAppConfig = function (chartConfig, __param, itemId, eTag, createProperty, stopTimer, requests, _curAppSeting) {
    //Total number of pending requests at the start of sending the request
    //var requestCount = requestsContainer.length;

    //if (currentReqStatus != CC.util.requestsStatus().InProcess && CC.med.providerConfig().DataSource && (requestCount > 0 || stopTimer)) {
    //if (CC.med.providerConfig().DataSource) {
    var dfd = $.Deferred();

    //set the flag to in-process.
    //currentReqStatus = CC.util.requestsStatus().InProcess;

    var queryPayload = {
        //"__metadata": { 'type': CC.util.getItemTypeForListName() }
        //"__metadata": { 'type': "SP.Data.OData__x005f_CA_x005f_ConfigListItem" }
        "__metadata": { 'type': 'SP.Data.CollabionCharts_x005f_ConfigListItem' }
    };

    //queryPayload["Config"] = JSON.stringify(CC.util.getAppSetting());
    //queryPayload["Config"] = JSON.stringify({
    //    "ProviderType": CC.med.providerConfig().currentProvider.type,
    //    "ProviderConfig": CC.med.providerConfig().DataSource,
    //    "ChartConfig":{
    //        "Configuration": CC.med.chartConfig().Configuration,
    //        "Cosmetics": CC.med.chartConfig().Cosmetics
    //    }
    //});

    queryPayload["Config"] = chartConfig;
    queryPayload["AppPartId"] = __param.wpId;



    var header = null;
    var url = null;
    if (!createProperty) {
        //update a new setting
        header = {
            "accept": "application/json;odata=verbose",
            "IF-MATCH": "*",
            //"IF-MATCH": eTag,
            "content-type": "application/json;odata=verbose",
            "X-HTTP-Method": "MERGE"
        };
        url = __param.appWebUrl + "/_api/Web/Lists/getbytitle('" + CC.util.getConfigListName() + "')/Items(" + parseInt(itemId) + ")";
    }
    else {
        //create a new setting
        url = __param.appWebUrl + "/_api/Web/Lists/getbytitle('" + CC.util.getConfigListName() + "')/Items";
    }

    var currReqCount = requests.count;

    CC.Data.SPList.prototype.executeRequest(url, "POST", header, queryPayload)
        .done(function (data) {
            //if (!stopTimer) {
            //    //Remove the exact number of requests that were pending before the start of the request. Any new changes that were made later will
            //    //still persist
            //    //var arr = $.grep(requestsContainer, function (n, i) {
            //    //    return (i >= removeValFromIndex);
            //    //});
            //    //requestsContainer = arr;
            //}
            if(stopTimer) {
                //Clear everything and stop the timer. Once stopped trigger wizard reset.
                //requestsContainer = [];
                //clearTimer();

                CC.track.prototype.wizardClosed(true);

                CC.med.resetWizard();
            }

            requests.currentReqStatus = CC.util.requestsStatus().Done;
            requests.count = requests.count - currReqCount;

            if (data) {

                //if (_curAppSeting) {
                //    _curAppSeting.etag = data[0].__metadata.etag;
                //}

                if (data.d.results)
                    dfd.resolve(data.d.results[0]);
                else
                    dfd.resolve(data.d);
            }
            else
                dfd.resolve();
        })
        .fail(function (args) {
            requests.currentReqStatus = CC.util.requestsStatus().Fail;
            //var ex = new CC.Exception("App Config Not Found", "Unable to fetch the app config for : " + __param.wpId, args);
            CC.med.logError({}, '132', 'AppPropNotSet', args.body ? args.body : args.message ? args.message : args);

            dfd.reject(args);
        });

    return dfd.promise();
    //}
}


//#region OLD CODE

//CC.Setting = function () {
//    var propertyInstance;

//    //private properties
//    var requestsContainer = [];

//    var documentHeader = {
//        param: {
//            hostWebUrl: null,           // Host web url
//            appWebUrl: null,            // App web url
//            senderId: null,             // Sender id
//            editMode: null,             // Ensure app part is in edit mode = 1 or view mode = 0
//            wpId: null,                 // App part id
//            wpq: null,                  // App part container id
//            hostPage: null,             // Relative host page
//            scriptBase: null        // Default SP2013 scriptbase
//        }
//    }

//    var currentReqStatus = CC.util.requestsStatus().None;

//    var PM = new CC.ProviderManager();
//    var CM = new CC.ChartManager();
//    CM.Cache = [];

//    var id = CC.util.getUniqueId();

//    listItemProperties = function () {
//        //Determines if the config of this chart already exist or not. Based on it item create or update method will be triggered
//        this.exist = false;

//        //stores the etag value of the item to be used for item update. No delete event is handled otherwise it would have been required for delete as well
//        this.etag = "*";

//        //the unique SharePoint ListItem Id to be used for item update. If item is being created then this value will also get set there apart from
//        //the usual get
//        this.itemId = -1;
//    }

//    curAppSeting = new listItemProperties();
//    this.createProperty = function () {
//        return !curAppSeting.exist;
//    }

//    create = function () {
//        this.parse = function () {
//            var dfd = $.Deferred();
//

//            //$.when(privateProp, CC.Data.SPList.prototype.getAppConfig())
//            //LoadFromQueryString(documentHeader.param).done(function () {
//                $.when(PM, CM, curAppSeting, getAppConfig())
//                    .done(function (_PM, _CM, _curAppSeting, data) {
//                        if (data.length > 0) {
//                            //Set the private properties only once during the get
//                            _curAppSeting.exist = true;
//                            _curAppSeting.etag = data[0].__metadata.etag;
//                            _curAppSeting.itemId = data[0].Id;

//                            var config = JSON.parse(data[0].Config);
//                            _PM.selectProvider(config.ProviderType);
//                            _PM.DataSource = config.ProviderConfig;
//                            _CM.ChartConfig.Configuration = config.ChartConfig;
//                            _CM.Cosmetics = config.Cosmetics;
//                        }
//                        else if (!_PM.currentProvider) {
//                            _PM.selectProvider(0);
//                        }
//                        dfd.resolve();
//                    })
//                    .fail(function (err) {   dfd.reject(err); });

//                return dfd.promise();
//            //}).fail(function (sender, args) {
//            //    // query string parsing failed
//            //
//            //})
//        }

//        //gets the current app setting. It should only be called once during the lifespan of the wizard. If a new app part is added then, it will return blank
//        // and the setting will be get from Set method after creating a new item.
//        getAppConfig = function () {
//            if (!PM.DataSource || CM.ChartConfig == undefined || !CM.ChartConfig.Configuration || !CM.Cosmetics) {
//                //Checks if none of the above properties is not already initialized.

//                var dfd = $.Deferred();

//                var queryPayLoad = {
//                    '__metadata': { 'type': 'SP.CamlQuery' },
//                    'ViewXml': CC.util.getAppSettingViewXml(documentHeader.param.wpId)
//                }

//                CC.Data.SPList.prototype.executeRequest(documentHeader.param.appWebUrl + "/_api/Web/Lists/getbytitle('" + CC.util.getConfigListName() + "')/Items?$select=Id,%20AppPartId,%20Config&$filter=AppPartId%20eq%20'" + documentHeader.param.wpId + "'")
//                //this.executeRequest(documentHeader.param.appWebUrl + "/_api/Web/Lists/getbytitle('" + CC.util.getConfigListName() + "')/Items", "GET", null, queryPayLoad)
//                    .done(function (data) {
//
//                        dfd.resolve(data.d.results);
//                    })
//                    .fail(function (args) {
//

//                        var ex = new CC.Exception("App Config Not Found", "Unable to fetch the app config for : " + documentHeader.param.wpId, args);
//                        dfd.reject(ex);
//                    });

//                return dfd.promise();
//            }
//        }

//        return {
//            parsed: parse()
//        }
//    }

//    LoadFromQueryString = function (p) {

//        var dfd = $.Deferred();
//
//        try {
//            //var q = getQueryStringArray();
//            p.hostWebUrl = decodeURIComponent(CC.util.getQueryStringParameter("SPHostUrl"));
//            p.appWebUrl = decodeURIComponent(CC.util.getQueryStringParameter("SPAppWebUrl"));
//            p.senderId = decodeURIComponent(CC.util.getQueryStringParameter("SenderId"));
//            p.editMode = parseInt(decodeURIComponent(CC.util.getQueryStringParameter("editmode")));
//            p.wpId = decodeURIComponent(CC.util.getQueryStringParameter("wpId"));
//            p.wpq = decodeURIComponent(CC.util.getQueryStringParameter("wpq"));
//            p.hostPage = CC.util.getHostPage();
//            //p.scriptBase = p.hostWebUrl + "/_layouts/15/";
//            var layoutUrl = _spPageContextInfo.layoutsUrl;
//            if (!layoutUrl.startsWith("/"))
//                layoutUrl = "/" + layoutUrl;
//            if (!layoutUrl.endsWith("/"))
//                layoutUrl += "/";
//            p.scriptBase = p.hostWebUrl + layoutUrl;

//            dfd.resolve();
//        }
//        catch (err) {
//            dfd.reject(err);
//        }

//        return dfd.promise();
//    };

//    setAppConfig = function (itemId, eTag, stopTimer) {
//        //Total number of pending requests at the start of sending the request
//        var requestCount = requestsContainer.length;

//        if (currentReqStatus != CC.util.requestsStatus().InProcess && CC.med.providerConfig().DataSource && (requestCount > 0 || stopTimer)) {
//            var dfd = $.Deferred();

//            //set the flag to in-process.
//            currentReqStatus = CC.util.requestsStatus().InProcess;

//            var queryPayload = {
//                //"__metadata": { 'type': CC.util.getItemTypeForListName() }
//                "__metadata": { 'type': "SP.Data.OData__x005f_CA_x005f_ConfigListItem" }
//            };

//            //queryPayload["Config"] = JSON.stringify(CC.util.getAppSetting());
//            queryPayload["Config"] = JSON.stringify({
//                "ProviderType": CC.med.providerConfig().currentProvider.type,
//                "ProviderConfig": CC.med.providerConfig().DataSource,
//                "ChartConfig": CC.med.chartConfig().Configuration,
//                "Cosmetics": CC.med.chartConfig().Cosmetics
//            });
//            queryPayload["AppPartId"] = documentHeader.param.wpId;

//

//            var header = null;
//            var url = null;
//            if (!createProperty()) {
//                //update a new setting
//                header = {
//                    "accept": "application/json;odata=verbose",
//                    //"IF-MATCH": "*",
//                    "IF-MATCH": eTag,
//                    "content-type": "application/json;odata=verbose",
//                    "X-HTTP-Method": "MERGE"
//                };
//                url = documentHeader.param.appWebUrl + "/_api/Web/Lists/getbytitle('" + CC.util.getConfigListName() + "')/Items(" + parseInt(itemId) + ")";
//            }
//            else {
//                //create a new setting
//                url = documentHeader.param.appWebUrl + "/_api/Web/Lists/getbytitle('" + CC.util.getConfigListName() + "')/Items";
//            }

//            CC.Data.SPList.prototype.executeRequest(url, "POST", header, queryPayload)
//                .done(function (data) {
//                    if (!stopTimer) {
//                        //Remove the exact number of requests that were pending before the start of the request. Any new changes that were made later will
//                        //still persist
//                        var arr = $.grep(requestsContainer, function (n, i) {
//                            return (i >= removeValFromIndex);
//                        });
//                        requestsContainer = arr;
//                    }
//                    else {
//                        //Clear everything and stop the timer. Once stopped it should not get started again.
//                        requestsContainer = [];
//                        clearTimer();
//                    }

//                    currentReqStatus = CC.util.requestsStatus().Done;

//                    if (data) {
//                        if (data.d.results)
//                            dfd.resolve(data.d.results[0]);
//                        else
//                            dfd.resolve(data.d);
//                    }
//                    else
//                        dfd.resolve();


//                })
//                .fail(function (args) {
//                    currentReqStatus = CC.util.requestsStatus().Fail;
//                    var ex = new CC.Exception("App Config Not Found", "Unable to fetch the app config for : " + documentHeader.param.wpId, args);
//                    dfd.reject(ex);
//                });

//            return dfd.promise();
//        }
//    }

//    initializeModule = function () {
//        return {
//            saveSetting: function (sender) {
//                var dfd = $.Deferred();

//                //$.when(privateProp, CC.Data.SPList.prototype.setAppConfig(privateProp.itemId, privateProp.etag, stopTimer))
//                $.when(curAppSeting, setAppConfig(curAppSeting.itemId, curAppSeting.etag, (sender && sender.getXType() == 'button' && sender.id == 'btnDone')))
//                    .done(function (_curAppSeting, data) {
//                        if (data && !_curAppSeting.exist) {
//                            //Set the private properties only when creating the chart setting and NOT updating.
//                            _curAppSeting.exist = true;
//                            _curAppSeting.etag = data.__metadata.etag;
//                            _curAppSeting.itemId = data.Id;
//                        }
//                        dfd.resolve();
//                    }).fail(function (err) {
//                        dfd.reject(err);
//                    });

//                return dfd.promise();
//            },

//            docHeader: function () {
//                return documentHeader.param;
//            },

//            providerConfig: function () {
//                return PM;
//            },

//            charts: function(){
//                return CM;
//                //return null;
//            },

//            chartConfig: function () {
//                //return (CM.ChartConfig) ? CM.ChartConfig.Configuration : [];
//                return CM.ChartConfig;
//            },

//            chartCosmetics: function () {
//                return CM.Cosmetics;
//            },

//            loadProperties: function () {
//                if (!PM.currentProvider)
//                    create();
//            }
//        }
//    }


//    return {
//        getInstance: function () {
//            var dfd = $.Deferred();
//            if (!propertyInstance) {
//                $.when(propertyInstance, LoadFromQueryString(documentHeader.param)).done(function (_propertyInstance) {
//                    _propertyInstance = initializeModule();
//
//                    //create();
//                    dfd.resolve(_propertyInstance);
//                }).fail(function () { });

//            } else {
//                dfd.resolve(propertyInstance);
//            }

//            return dfd.promise();
//        }

//    }
//}


//CC.Properties = {

//    DocumentHeader: null,

//    AppProperty: null
//};

//CC.Properties.Property = function () {

//    readOnlyProperties = function () {
//        //Determines if the config of this chart already exist or not. Based on it item create or update method will be triggered
//        this.exist = false;

//        //stores the etag value of the item to be used for item update. No delete event is handled otherwise it would have been required for delete as well
//        this.etag = "*";

//        //the unique SharePoint ListItem Id to be used for item update. If item is being created then this value will also get set there apart from
//        //the usual get
//        this.itemId = -1;
//    }

//    privateProp = new readOnlyProperties();

//    this.createProperty = function () {
//        return !privateProp.exist;
//    }

//    this.Parse = function () {

//        var dfd = $.Deferred();
//

//        //$.when(privateProp, CC.Data.SPList.prototype.getAppConfig())
//        $.when(privateProp, CC.ST.getAppConfig())
//            .done(function (_privateProp, data) {
//                if (data.length > 0) {
//                    //Set the private properties only once during the get
//                    _privateProp.exist = true;
//                    _privateProp.etag = data[0].__metadata.etag;
//                    _privateProp.itemId = data[0].Id;

//                    var config = JSON.parse(data[0].Config);
//                    CC.med.providerConfig().selectProvider(config.ProviderType);
//                    CC.med.providerConfig().DataSource = config.ProviderConfig;
//                    CC.med.chartConfig().ChartConfig.Configuration = config.ChartConfig;
//                    CC.med.chartConfig().Cosmetics = config.Cosmetics;
//                }
//                dfd.resolve();
//            })
//            .fail(function (err) { dfd.reject(err); });

//        return dfd.promise();
//    }

//    this.Set = function (sender) {
//        var dfd = $.Deferred();

//        //$.when(privateProp, CC.Data.SPList.prototype.setAppConfig(privateProp.itemId, privateProp.etag, stopTimer))
//        $.when(privateProp, CC.ST.setAppConfig(privateProp.itemId, privateProp.etag, (sender && sender.getXType() == 'button' && sender.id == 'btnDone')))
//            .done(function (_privateProp, data) {
//                if (data && !_privateProp.exist) {
//                    //Set the private properties only when creating the chart setting and NOT updating.
//                    _privateProp.exist = true;
//                    _privateProp.etag = data.__metadata.etag;
//                    _privateProp.itemId = data.Id;
//                }
//                dfd.resolve();
//            }).fail(function (err) {
//                dfd.reject(err);
//            });

//        return dfd.promise();
//    }
//}

//CC.Properties.prototype = {
//    // Starts from here at Page load
//    init: function () {
//
//        CC.Properties.DocumentHeader = new CC.DocumentHeader();
//        CC.Properties.AppProperty = new CC.Properties.Property();
//        CC.Properties.DocumentHeader.Parse();
//        CC.Properties.AppProperty.Parse().done(function () {

//        }).fail(function (err) {
//            //unable to parse document header.
//            alert('unable to parse document header.');
//        });
//    }
//}

//********************************************
// Parse DocumentHeader
//
//CC.DocumentHeader = function () {
//    this.Param = {
//        hostWebUrl: null,           // Host web url
//        appWebUrl: null,            // App web url
//        senderId: null,             // Sender id
//        editMode: null,             // Ensure app part is in edit mode = 1 or view mode = 0
//        wpId: null,                 // App part id
//        wpq: null,                  // App part container id
//        hostPage: null,             // Relative host page
//        scriptBase: null        // Default SP2013 scriptbase
//    };

//    this.Parse = function () {
//        LoadFromQueryString(documentHeader.param)
//    }

//    function LoadFromQueryString(p) {

//        var dfd = $.Deferred();
//
//        try {
//            //var q = getQueryStringArray();
//            p.hostWebUrl = decodeURIComponent(CC.util.getQueryStringParameter("SPHostUrl"));
//            p.appWebUrl = decodeURIComponent(CC.util.getQueryStringParameter("SPAppWebUrl"));
//            p.senderId = decodeURIComponent(CC.util.getQueryStringParameter("SenderId"));
//            p.editMode = parseInt(decodeURIComponent(CC.util.getQueryStringParameter("editmode")));
//            p.wpId = decodeURIComponent(CC.util.getQueryStringParameter("wpId"));
//            p.wpq = decodeURIComponent(CC.util.getQueryStringParameter("wpq"));
//            p.hostPage = CC.util.getHostPage();
//            //p.scriptBase = p.hostWebUrl + "/_layouts/15/";
//            var layoutUrl = _spPageContextInfo.layoutsUrl;
//            if (!layoutUrl.startsWith("/"))
//                layoutUrl = "/" + layoutUrl;
//            if (!layoutUrl.endsWith("/"))
//                layoutUrl += "/";
//            p.scriptBase = p.hostWebUrl + layoutUrl;

//            dfd.resolve();
//        }
//        catch (err) {
//            dfd.reject(err);
//        }

//        return dfd.promise();
//    };
//};














CC.Context = function () {
};

CC.Context.Load = function () {

    CC.Context.Items = {
        clientContext: null,
        //appContextSite: null,
        web: null
    };

    var dfd = $.Deferred();

    try {
        dfd.resolve();
    }
    catch (err) {
        //reject if any unknown error occurs
        dfd.reject(err);
    }

    return dfd.promise();
}

//#endregion