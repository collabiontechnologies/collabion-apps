﻿<%@ Page Language="C#" Inherits="Microsoft.SharePoint.WebPartPages.WebPartPage, Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>

<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>

<WebPartPages:AllowFraming runat="server" />

<!doctype html>
<html>
<head>
    <title>Collabion Charts for SharePoint Online</title>
    <!-- <meta http-equiv="X-UA-Compatible" content="IE=8" /> -->

    <meta charset="utf-8">

    <script type="text/javascript" src="../Scripts/jquery-1.9.1.min.js"></script>

    <meta name="WebPartPageExpansion" content="full" />

    <!-- Add your CSS styles to the following file -->
    <link rel="Stylesheet" type="text/css" href="../Content/App.css" />

    <!--
    =============================
    STYLES
    =============================
    -->
    <!-- CCSP_APP -->
    <!-- Lato Fonts -->
    <link href="../Scripts/ext/resources/css/fonts/lato/latofonts.css" rel="stylesheet" />
    <link href="../Scripts/ext/resources/css/fonts/lato/latostyle.css" rel="stylesheet" />

    <!-- extJS -->
    <link rel="stylesheet" type="text/css" href="../Scripts/ext/resources/css/ccStyle.css" />
    <link rel="stylesheet" type="text/css" href="../Scripts/ext/resources/css/ext-all-custom.css" />

    <!-- ToggleField -->
    <link rel="stylesheet" type="text/css" href="../Scripts/ext/resources/css/ToggleField.css" />

    <!-- ToolTip -->
    <link rel="stylesheet" type="text/css" href="../Scripts/ext/resources/css/tooltipster.css" />
    <link rel="stylesheet" type="text/css" href="../Scripts/ext/resources/css/themes/tooltipster-shadow.css" />

    <!-- Horrizontal Scroll -->
    <link rel="stylesheet" type="text/css" media="screen" href="../Scripts/ext/resources/css/jquery.hoverscroll.css" />

    <!-- Custom Checkboxes -->
    <link href="../Scripts/ext/resources/css/CheckBox.css" rel="stylesheet" />

    <!--
    =============================
    JS
    =============================
    -->

    <!-- extJS -->
    <script type="text/javascript" src="../Scripts/ext/adapter/ext/ext-base.js"></script>
    <script type="text/javascript" src="../Scripts/ext/ext-all.js"></script>

    <!-- jQuery is already loaded -->

    <!-- SP -->
    <script type="text/javascript" src="/_layouts/15/MicrosoftAjax.js"></script>
    <script type="text/javascript" src="/_layouts/15/sp.runtime.js"></script>
    <script type="text/javascript" src="/_layouts/15/sp.js"></script>
    <script type="text/javascript" src="/_layouts/15/SP.RequestExecutor.js"></script>

    <!-- Plugins -->
    <script type="text/javascript" src="../Scripts/ext/plugins/plugins.js"></script>

    <!-- FusionCharts JS -->
    <script src="../Scripts/fusioncharts/fusioncharts.js"></script>
    <script src="../Scripts/fusioncharts/fusioncharts.charts.js"></script>
    <script src="../Scripts/fusioncharts/fusioncharts.powercharts.js"></script>
    <script src="../Scripts/fusioncharts/fusioncharts.widgets.js"></script>
    <script src="../Scripts/fusioncharts/themes/fusioncharts.theme.carbon.js"></script>
    <%--<script src="../CCSP_APP/js/fusioncharts/themes/fusioncharts.theme.fint.js"></script>--%>
    <script src="../Scripts/fusioncharts/themes/fusioncharts.theme.ocean.js"></script>
    <script src="../Scripts/fusioncharts/themes/fusioncharts.theme.zune.js"></script>

    <!-- Initializers -->
    <script type="text/javascript" src="../Scripts/ui/ccsp-ui.js"></script>

    <!-- Wizard messages & Tooltip -->
    <script src="../Scripts/data/message-en-Us.js"></script>
    <script src="../Scripts/data/tip-en-Us.js"></script>


    <style type="text/css">
        #panel-wizard {
            background-color: transparent;
            width: 645px;
            height: 580px;
            padding: 0;
            border: 0px solid rgb(77, 144, 254);
            /*margin: 50px;*/
        }
    </style>

</head>

<body style="height: 100%;">
    <form runat="server">
        <SharePoint:FormDigest ID="FormDigest1" runat="server"></SharePoint:FormDigest>
        <div id="ccParent" style="width: 100%; height: 100%;">
            <div class="div" id="panel-wizard"></div>
            <div id="pnlCallout" class="callout border-callout"><b class="border-notch notch"></b><b class="notch"></b></div>
            <div id="overlaydiv" runat="server" class="modal" style="text-align: center;">
                <img src="../Images/preloader.gif">
                <span style="font-family: 'LatoWebLight', tahoma, helvetica; font-size: 36px;" id="progressTextSpan"></span>
            </div>
        </div>
    </form>
</body>
</html>
