/*!
 * Collabion Charts Add-In for SharePoint Online
 * Copyright Collabion Technologies LLP
 * License Information at <https://go.microsoft.com/fwlink/?LinkID=521715&omkt=en-US>
 */


// Namespaces
Ext.ns('Collabion');
Ext.ns('Collabion.App');
Ext.ns('Collabion.App.Charts');

//
// Wrapper
//
// CC.App = function(){
// // CM: undefined,

// // util: undefined
// }

Ext.ns('Collabion.App.Charts.Tabs');

//holding the entire chart configuration
Ext.ns('Collabion.App.Charts.Var');

//Data Fetching
Ext.ns("Collabion.App.Charts.Data");

var CC = Collabion.App.Charts;

//
// Utility methods
//
CC.Common = function () {
    var idCnt = 0;

    //for tracking
    var seriesCount = -1;

    //determines if the given field is one of those special hexadecimal field
    var spclHexFields = '_x';

    var tipCls = 'tooltip';
    //var errCls = 'x-form-invalid';
    var errCls = 'x-form-invalid-custom';
    var configList = 'CollabionCharts_Config';
    var configKey = "Config";

    var xTypesKeys = {
        'cc:combo': 0,
        'dataview': 1,
        'checkbox': 2,
        'textfield': 3,
        'spinnerfield': 4,
        'button': 5,
        'combo': 6
    };

    var targetKeys = {
        ccList: 'ccList',
        ccView: 'ccView',
        //fields: 2,
        axisView: 'axisView',
        chartView: 'chartView',
        //activate: 5,
        pnlCat: 'pnlCat',
        pnlCType: 'pnlCType',
        chkLabels: 'chkLabels',
        btnYes: 'btnYes',
        fldXAxs: 'fldXAxs',
        fldYAxs: 'fldYAxs',
        nmbrRd: 'nmbrRd'
    };

    var tabsActivateKeys = {
        data: "DATA",
        fields: "FIELDS",
        charts: "CHARTS",
        labels: "LABELS",
        numbers: "NUMBERS",
        addY: "ADDY",
        swpFld: "SWPFLD",
        lblUpdt: 'lblUpdt',
        nmbrUpdt: 'nmbrUpdt',
        yaxis: 'yaxis',
        yaxisRender: 'yaxisRender',
        cmbTheme: 'cmbTheme'
        //delYes: "delYes"
    };

    var winHeight, winWidth;

    this.config = undefined;

    //
    // Merge array objects to one
    // objs - array objects
    //
    this.merge = function (objs) {
        var obj = {};
        for (var i = 0; i < objs.length; i++) {
            for (var p in objs[i]) obj[p] = objs[i][p];
        }
        return obj;
    };

    //
    // Retrieve new array based on source array
    // source - source array
    // mobj - mobj can contains add and del arrays
    //		add - array of items that must be added
    //		del - array of items that must be removed
    //
    this.modify = function (source, mobj) {
        var dest = [];
        for (var i = 0; i < source.length; i++) dest[i] = source[i];
        if (mobj) {
            for (var i = 0; mobj.del && i < mobj.del.length; i++) {
                dest.remove(mobj.del[i])
            }
            for (var i = 0; mobj.add && i < mobj.add.length; i++) {
                dest.push(mobj.add[i]);
            }
        }
        return dest;
    };

    //
    //Prevents the event chain prpagation both in IE and other browsers
    //
    this.stopPropagation = function (event) {
        //event.stopPropagation();

        if (!event) var event = window.event
        // handle event
        event.cancelBubble = true; //for IE
        if (event.stopPropagation) event.stopPropagation(); //for other browsers
    };

    //
    //returns a unique integer on each call starting from zero
    //
    this.getUniqueId = function () {
        return ++idCnt;
    };

    this.getSeriesCount = function () {
        return ++seriesCount;
    };

    this.yPanelId = function (id) {
        return CC.util.getPanelNumber(id) + '-pnl'
    };

    this.getPanelNumber = function (id) {
        return id.split('-')[0];
    };

    this.randomId = function (initials) {
        var id = this.getUniqueId();
        if (initials)
            id = initials + id;
        return id;
    };

    this.closePopup = function (speed) {
        //$("#pnlCallout").fadeOut(isFast?"fast":"slow");
        $("#pnlCallout").fadeOut(speed ? speed : "slow");
    };

    this.cancelFadeOut = function (event) {
        //$('#pnlCallout').stop(true, true).fadeOut();
        var pnl = $('#pnlCallout');
        if (pnl.is(':animated')) {
            ////Console.log('cancelFadeOut Fired: animated');

            pnl.stop(true, true).fadeOut();
            pnl.fadeIn(1);
            CC.util.stopPropagation(event);
        }
    };

    this.isEventBound = function (ele, event) {
        var ev = $._data(ele, 'events');
        return (ev && ev[event]);
    };

    this.getContainerItemIndex = function (cntrId, item) {
        return Ext.getCmp(cntrId).items.indexOf(item);
    };

    this.getContainerItem = function (cntrId, itemId) {
        return Ext.getCmp(cntrId).items.item(itemId);
    };

    this.generateTip = function (_position) {
        $('.' + this.getTooltipCls()).tooltipster({
            theme: 'tooltipster-shadow',
            onlyOne: true,
            maxWidth: 150,
            minWidth: 120,
            contentAsHTML: true,
            //arrow: false,
            //animation: 'fall',
            position: _position ? _position : 'left',
            debug: false//,
            //positionTracker: true
        });
    };

    this.getTooltipCls = function () {
        return tipCls;
    };

    this.getErrorCls = function () {
        return errCls;
    };

    this.getHelpIconImg = function () {
        //return 'ext/resources/images/default/collabion/help.png';
        return 'assets/help-hover.png';
    };

    this.chartPrevItemLimit = function () {
        //return 25;
        return 5;
    };

    this.getChartTypes = function () {
        return {
            //Single Series
            column2d: 0,
            column3d: 0,
            line: 0,
            area2d: 0,
            bar2d: 0,
            bar3d: 0,
            pie2d: 0,
            pie3d: 0,
            doughnut2d: 0,
            doughnut3d: 0,
            pareto2d: 0,
            pareto3d: 0,
            spline: 0,
            splinearea: 0,
            kagi: 0,
            funnel: 0,
            pyramid: 0,
            //Multi Series
            mscolumn2d: 1,
            mscolumn3d: 1,
            msline: 1,
            msbar2d: 1,
            msbar3d: 1,
            msarea: 1,
            marimekko: 1,
            msspline: 1,
            mssplinearea: 1,
            logmsline: 1,
            logmscolumn2d: 1,
            radar: 1,
            inversemsarea: 1,
            inversemscolumn2d: 1,
            inversemsline: 1,
            msstepLine: 1,
            //Stacked
            stackedcolumn2d: 2,
            stackedcolumn3d: 2,
            stackedbar2d: 2,
            stackedbar3d: 2,
            stackedarea2d: 2,
            //Combination
            mscombi2d: 3,
            mscombi3d: 3,
            mscolumnline3d: 3,
            stackedcolumn2dline: 3,
            msstackedcolumn2dlinedy: 3,
            stackedcolumn3dline: 3,
            mscombidy2d: 3,
            mscolumn3dlinedy: 3,
            stackedcolumn3dlinedy: 3,
            //Scroll Charts
            scrollcolumn2d: 4,
            scrollline2d: 4,
            scrollarea2d: 4,
            scrollstackedsolumn2d: 4,
            scrollsombi2d: 4,
            scrollcombidy2d: 4,
            //ZoomLine
            zoomline: 5,
            //XY
            scatter: 6,
            bubble: 6
        };

    };

    this.getChart = function (chartType) {
        //var chartTypes = CC.CM.Chart.getTypesByCategory();
        //$.grep(CC.CM.Chart.getTypesByCategory(), function (element, index) {
        var allCharts = CC.med.cmChart().getTypesByCategory();

        //Initialize with a default value of column 2d
        //var chart = jQuery.extend({}, allCharts[0]);
        var chart = (JSON.parse(JSON.stringify(allCharts[0])));
        chart.push(0);
        chart.push(0);

        var breakLoop = false;

        $.each(allCharts, function (index, element) {
            //res = $.grep(element, function (_element, _index) {
            //    //Internal Name check
            //    return _element[3] == chartType;
            //});

            $.each(element, function (_index, _element) {
                if (_element[3] == chartType) {
                    chart = (JSON.parse(JSON.stringify(_element)));
                    chart.push(_index);
                    chart.push(index);

                    breakLoop = true;
                    return false;
                }
            });

            if (breakLoop) return false;

            //if (res.length != 0) {
            //    //chart = jQuery.extend({}, res[0]);
            //    chart = (JSON.parse(JSON.stringify(res[0])));
            //    chart.push(index);
            //    return false;
            //}
        });

        return chart;
    };

    this.getChartSeries = function (chartType) {
        return CC.util.getChart(chartType)[4];
    };

    this.getQueryStringParameter = function (paramToRetrieve) {
        var params = document.URL.split("?")[1].split("&");
        for (var i = 0; i < params.length; i = i + 1) {
            var singleParam = params[i].split("=");
            if (singleParam[0] == paramToRetrieve)
                return singleParam[1];
        }
    };

    this.swapItem = function (item1, item2) {
        var temp = item1;
        item1 = item2;
        item2 = temp;

        return [item1, item2];
    };

    this.toggleProgressBar = function (enable, msg) {
        if (enable) {
            CC.util.setProgressMsg(msg);
            if (Ext.getCmp('cntrMain')) {
                //check if the component is rendered or not. The following items will appear only when the wizard is open
                Ext.getCmp('cntrMain').disable();
                //Ext.getCmp('overlayPanel').show();
                //CC.Wizard.tabpanel.tabs[0].toggleEnable(false);
            }
            $('#overlaydiv').css('display', 'inline-table');
        }
        else {
            if (Ext.getCmp('cntrMain')) {
                Ext.getCmp('cntrMain').enable();
                //Ext.getCmp('overlayPanel').hide();
                //CC.Wizard.tabpanel.tabs[0].toggleEnable(true);
            }
            $('#overlaydiv').css('display', 'none');
        }
    };

    this.togglePartialProgressBar = function (msg) {
        if (Ext.getCmp('overlayPanel').hidden) {
            Ext.getCmp('cntrPreview').disable();
            CC.util.setProgressMsg(msg);
            Ext.getCmp('overlayPanel').show();
        }
        else {
            Ext.getCmp('cntrPreview').enable();
            Ext.getCmp('overlayPanel').hide();
        }
        // var bot = $(window).height() - $('body').height();//-$('#wizPanel').offset().top;
        // var mid = (Ext.getCmp('cntrPreview').getHeight()/2)+bot-($('#overlaydiv').height()/2);
        var top = $('#cntrPreview').offset().top;
        var h = Ext.getCmp('cntrPreview').getHeight();
        var w = Ext.getCmp('cntrPreview').getWidth();
        $('#overlayPanel').css({ 'position': 'absolute', 'top': top + 'px' });
        $('#overlayPanel').find('.x-panel-body').css({ 'background': 'transparent', 'height': h + 'px', 'width': w + 'px' });
        $('#overlaydiv').css('position', 'absolute');
        // $('#overlaydiv').css('bottom', mid+'px');


    };

    this.setProgressMsg = function (msg) {
        $("#overlaydiv span").text(msg ? '  ' + msg : CC.msgs.wzrd174);
        $("#overlaydiv").css('margin-left', '-150px');
        //$("#overlaydiv img").css('margin-top', '-20px');

        if ($('#overlaydiv').width() + 150 >= this.windowWidth()) {
            $("#overlaydiv span").text('');
            $("#overlaydiv").css('margin-left', '0px');
            //$("#overlaydiv img").css('margin-top', '0px');
        }
    };

    this.getImgPath = function () {
        return "../CCSP_APP/assets/";
    };

    this.defaultSingleSeriesChart = function () {
        return "column2d";
    };

    this.defaultMultiSeriesChart = function () {
        return "mscombi2d";
    };

    this.valueCopyJSON = function (jsonData) {
        return (JSON.parse(JSON.stringify(jsonData)));
    };

    //loops through a json and returns the matching item based on the value.
    //You can either, directly test for a value or can provide a corresponding key
    this.jsonItemIndex = function (jsonData, val, key) {
        var seriesIndex = -1;
        $.each(jsonData, function (index, element) {
            //if (val == element.Column) {
            if ((key && val == element[key]) || (val == element)) {
                seriesIndex = index;
                return false;
            }
        });

        return seriesIndex;
    };

    //adds only if the item is not present already based on a given key value pair.
    this.insertUnique = function (jsonData, val, key, newData) {
        var itemIndex = this.jsonItemIndex(jsonData, val, key);
        if (itemIndex == -1) {
            jsonData.push(newData);
        }
        else {
            jsonData[itemIndex] = newData;
        }
    };

    var selectField = function (dataTitleType, isTitle, aggregateFieldName) {
        if (!isTitle) {
            if (!aggregateFieldName) {
                return $.grep(CC.Stores.ViewFields.data.items, function (element, index) { return (element.data.TypeAsString == dataTitleType && CC.med.getSetting('dsFld').indexOf(element.data.InternalName) == -1); });
            }
            else {
                return $.grep(CC.Stores.ViewFields.data.items, function (element, index) { return (element.data.InternalName == aggregateFieldName && CC.med.getSetting('dsFld').indexOf(element.data.InternalName) == -1); });
            }
        }
        else {
            return $.grep(CC.Stores.ViewFields.data.items, function (element, index) { return (element.data.Title == dataTitleType && CC.med.getSetting('dsFld').indexOf(element.data.InternalName) == -1); });
        }
    };

    this.selectAutoField = function (startField, isAggregate) {
        var ret = undefined;

        //var fieldsOrder = ['Title', 'Text', 'Computed', 'Number', 'Currency', 'Calculated'];
        var fieldsOrder = this.getSpAllFieldsType();
        var itemIndex = startField ? CC.util.jsonItemIndex(fieldsOrder, startField) : 0;
        itemIndex = itemIndex < 0 ? 0 : itemIndex;

        for (var i = itemIndex; i < fieldsOrder.length; i++) {
            var isTitle = !isAggregate && i == 0;
            //ret = selectField(fieldsOrder[i], i == 0);
            ret = selectField(fieldsOrder[i], isTitle, (isAggregate ? startField : undefined));
            if (ret.length > 0) break;
        }
        return ret;
    };

    this.addCssClass = function (ele, cls) {
        $.each(cls, function (index, value) {
            if (ele.classList)
                ele.classList.add(value);
            else
                ele.className += " " + value;
        });
    };

    this.removeCssClass = function (ele, cls) {
        if (ele && ele.classList) {
            $.each(cls, function (index, value) {
                ele.classList.remove(value);
            });
        }
    };

    this.convertToInt = function (val) {
        return val * 1;
    };

    this.getHostPage = function () {

        var url = (window.location != window.parent.location)
                ? document.referrer
                : document.location;

        url = typeof (url) == "object" ? url.href : url;


        var str = url.split("//")[1]
        var cnt = str.indexOf("/");
        return str.substring(cnt, str.length);
    };

    this.appendPrefix = function (suffix) {
        return "_ca_" + suffix;
    };

    // Get List Item Type metadata
    this.getItemTypeForListName = function (name) {
        name = !name ? this.getConfigListName() : name;
        return "SP.Data." + name.charAt(0).toUpperCase() + name.split(" ").join("").slice(1) + "ListItem";
    };

    this.getConfigListName = function () {
        return configList;
    };

    this.getSpAllFieldsType = function () {
        return ['Title', 'Text', 'Computed', 'Currency', 'Number', 'Calculated'];
    };

    this.getSpNumbersFieldType = function () {
        return this.getSpAllFieldsType().splice(3);
    };

    this.isFieldNumber = function (fieldType) {
        return jQuery.inArray(fieldType, this.getSpAllFieldsType()) > 2;
    };

    //TBD
    //Gets the ViewXml for fetching the current app setting
    this.getAppSettingViewXml = function (appPartId) {
        return "<View>" +
            "<ViewFields>" +
                        "<FieldRef Name='Config' />" +
                        "<FieldRef Name='AppPartId' />" +
                        "<FieldRef Name='ID' />" +
                    "</ViewFields>" +
            "<Query>" +
                        "<Where>" +
                            "<Eq>" +
                                "<FieldRef Name='AppPartId' />" +
                                "<Value Type='Text' >" +
                                    appPartId +
                                "</Value>" +
                            "</Eq>" +
                        "</Where>" +
                    "</Query>" +

                    "<RowLimit>1</RowLimit>" +
                    "</View>";
    };



    //////////////////////////////////////////////////////////////////////

    //check if needed from two files otherwise move to AppProperty.js
    this.requestsStatus = function () {
        return {
            InProcess: 0,
            None: 1,
            Done: 2,
            Fail: 3
        }
    };

    this.tabs = function () {
        return {
            data: 0,
            fields: 1,
            charts: 2,
            labels: 3,
            numbers: 4,
            activate: 5
        }
    };

    this.xTypes = function (key) {
        return xTypesKeys[key];
    };

    this.target = function (key) {
        return targetKeys[key];
    };

    this.tabsActivate = function (key) {
        return tabsActivateKeys[key];
    };

    /* checks if the given chartType in argument is combination chart or not. If no argument is provided, it checks the chart type from the setting */
    this.isCombinationChart = function (chartType) {
        return (chartType ? (chartType == 3) : (CC.util.getChartSeries(CC.med.getSetting('cType')) == 3));
    };

    this.isPageEdit = function () {
        return CC.med.docHeader('editMode');
    };

    this.isDualYAxis = function (chartType) {
        return (chartType ? chartType == 'mscombidy2d' : CC.med.getSetting('cType') == 'mscombidy2d');
    },

    this.comboChartOptions = function () {
        return ['bar', 'area', 'line'];
    },

    /* applies the error style to the given combo and also resets the error identifiers */
    this.markComboError = function (combo) {
        combo.cancelSelection = false;
        if (!combo.overriteValue) {
            combo.setValue('');
        }
        else {
            combo.setValue(combo.overriteValue);
        }
        combo.overriteValue = '';
        this.addCssClass(combo.el.dom, [this.getErrorCls()]);
    },

    this.enableDisableTreeNode = function (enable, nodeIndex) {
        var leaf = Ext.getCmp('tpDataSource').root.childNodes[nodeIndex];
        if (enable)
            leaf.enable();
        else {
            leaf.disable();
        }
    },

    this.getDomainFromEmail = function (url) {
        if (url) {
            return url.split('@')[1];
        }

        return url;
    },

    findWindowHeight = function () {
        return window.innerHeight || document.documentElement.clientHeight || document.getElementsByTagName('body')[0].clientHeight;
    },

    findWindowWidth = function () {
        return window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
    },

    this.windowWidth = function () {
        if (!winWidth) {
            winWidth = findWindowWidth();
        }

        return winWidth;
    },

    this.windowHeight = function () {
        if (!winHeight) {
            winHeight = findWindowHeight();
        }

        return winHeight;
    },

    //Key to be appended before the actual field to read it's value
    this.odataField = function () {
        return "OData_";
    },

    //determines if the given field requies the addition of OData
    this.isAppendODataField = function (fieldName) {
        return fieldName.startsWith(spclHexFields);
    },

    //Formats the actual field name if it starts with hex value to comply with OData standards
    this.hexFormattedFieldName = function (fieldName) {
        if (this.isAppendODataField(fieldName)) {
            fieldName = this.odataField() + fieldName;
        }

        return fieldName;
    }


};

CC.Exception = function (name, message, innerException) {
    this.name = name;
    this.message = message;
    this.innerException = innerException;
}

$.cachedScript = function (url, options) {

    // Allow user to set any option except for dataType, cache, and url
    options = $.extend(options || {}, {
        dataType: "script",
        cache: true,
        url: url
    });

    // Return the jqXHR object
    return $.ajax(options);
};

$.pluck = function (arr, key) {
    return $.map(arr, function (e) { return e[key]; });
};


(function () {
    CC.util = new CC.Common();
})();