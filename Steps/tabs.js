CC.Tabs.Panel = function (config) {
    this.config = config;
    this.currentStep = 0;
}

CC.Tabs.Panel.prototype = {
    // Initialize tabpanel components
    init: function () {        								
        //this.tabs = [new CC.Tabs.Tab.Data, new CC.Tabs.Tab.Chart, new CC.Tabs.Tab.Fields, new CC.Tabs.Tab.Label, new CC.Tabs.Tab.Number];	
        //this.steps = this.createTabs([this.tabs[0], this.tabs[1], this.tabs[2], this.tabs[3], this.tabs[4]]);
		
		this.tabs = [new CC.Tabs.Tab.Data, new CC.Tabs.Tab.Chart, new CC.Tabs.Tab.Label, new CC.Tabs.Tab.Number];	
        this.steps = this.createTabs([this.tabs[0], this.tabs[1], this.tabs[2], this.tabs[3]]);
	},
	
	createTabPanelItem: function (step) {
        return new Ext.Panel(Ext.apply({
            layout: 'fit',
            border: false,
            defaults: {
                autoScroll: true
            },
			activateStep : step.activateStep,
			validationStep : step.validationStep
        }, step.create()));
    },
	
	createTabs: function (steps) {
		var pnl = [];
		for (var k = 0; k < steps.length; k++) {
			len = pnl.length.toString();
			var p = this.createTabPanelItem(steps[k]);			
			p.name = len + '-step';
			p.index = len;
			p.disabled = true;
			pnl.push(p);
		}
		
		
		return pnl;
	},

    createTabPanel: function () {
        this.init();
		this.currentTab = undefined;
		this.win = new Ext.TabPanel({
			activeTab: 0,
			width: 10,
			resizeTabs: true,
			tabWidth: 100,
			id: 'tabs_panel',
			region: 'center',
			closable: false,
            items: [this.steps],
			listeners: {
				scope: this,
				'tabchange': function(tabPanel, tab, oldtab, opts){;
                    //this.step_onActive(tab.index);
					util.generateTip();
                },
				beforetabchange : function(tabPanel, tab, oldtab, opts) {
					var res = true;
					if(this.currentTab)
						res = this.currentTab.validationStep();
					if(res)
						this.currentTab = tab;
					
					return res;
				}
			}
        });
    },

    getPanel: function () {
        if (!this.win) {
			this.createTabPanel();
        }
        this.step_onActive(this.currentStep);
		return this.win;
    },

    // Called when user comes to step. Need activateStep method into step object
    step_onActive: function (index) {
		this.currentStep = index;
        this.steps[index].activateStep();
    },

    // Called when user leaves step. Need deactivate step method into step object
    step_onDeactive: function (index) {
        this.steps[index].deactivateStep();
    },

    // Validation step. Need validationStep method into step object
    step_onValidation: function (index) {
        return this.steps[index].validationStep();
    }
}