﻿/****************
 * Chart Builder class
 * This class will create chart from stored configuartion
 ******************/
CC.ChartBuilder = function (ChartConfig, ProviderConfig, RenderArea) {
    //If CharrtConfig option or Render div area is not provided in the parameter then refuse to build the object
    if (!ChartConfig || !RenderArea) {
        throw new CC.Exception("IncorrectParameter", "Both ChartConfiguration and Render Area id should be passed in this class");
    }
    this.ChartConfig = ChartConfig;
    this.ProviderConfig = ProviderConfig;
    //this.Cosmetics = Cosmetics;
    this.RenderArea = RenderArea;
    //Chart width and height are kept fixed but can be changed once object is created
    //Chart width
    this.ChartWidth = 900;
    //Chart height
    this.ChartHeight = 500;

    this.DataEmptyMessage = undefined;
};


//Build the chart from configuration
CC.ChartBuilder.prototype.createChart = function () {
    var cacheCurrentdata = false;

    //variable storing present scope
    var chartBuilderScope = this;

    //variable pointing only the configuration portion without data source portion
    var providerConfig = this.ProviderConfig;
    var chartConfig = this.ChartConfig.configuration;
    //var chartConfig = this.ChartConfig;
    //var cosmetics = this.Cosmetics;
    var cosmetics = this.ChartConfig.cosmetics;

    var distinct = chartConfig.distinct;

    var dataEmptyMessage = this.DataEmptyMessage;

    /* gets normal or groupBy value */
    var getValue = function (data, index, columnName, groupData) {
        return (data.hasOwnProperty("caKeys")) ? Number(groupData[index][data.caKeys[columnName]].replace(/[^0-9\.]+/g, "")) : data[index][columnName];
    };

    //Private function to build JSON data for single series chart
    var buildSingleSeriesData = function (data) {
        var dataSrc = [];
        var _data = data.hasOwnProperty("caKeys") ? data.Row : data;

        //build the data in single series format
        //for reference view
        //http://www.fusioncharts.com/dev/usage-guide/getting-started/your-first-charts/building-your-first-chart.html
        //getValue = function () {
        //    return (data.hasOwnProperty("caKeys")) ? Number(_data[i][data.caKeys[chartConfig.series[0].column]].replace(/[^0-9\.]+/g, "")) : data[i][chartConfig.series[0].column];
        //}

        var firstSeriesName = CC.util.hexFormattedFieldName(chartConfig.series[0].column);

        for (var i = 0; i < _data.length; i++) {
            //dataSrc[i] = { "label": data[i][chartConfig.XAxis.Column], "value": data[i][chartConfig.Series[0].Column] };
            //dataSrc[i] = { "label": _data[i][chartConfig.xAxis.column], "value": getValue(data, i, CC.util.hexFormattedFieldName(chartConfig.series[0].column), _data) };
            dataSrc[i] = { "label": _data[i][chartConfig.xAxis.column], "value": getValue(data, i, firstSeriesName, _data) };
        }

        return { "data": dataSrc };
    };

    //private function to build JSON data for multi series chart, stacked chart, scroll chart & combination chart
    var buildMultiSeriesData = function (data, isZoomLine) {

        //Basic structure of multi series chart
        //For reference visit
        //Applicable for multi series chart, stacked chart, combination chart, scroll chart
        //http://www.fusioncharts.com/dev/usage-guide/getting-started/more-chart-types/multi-series-charts.html
        //http://www.fusioncharts.com/dev/usage-guide/getting-started/more-chart-types/stacked-charts.html
        //http://www.fusioncharts.com/dev/usage-guide/getting-started/more-chart-types/combination-charts.html
        //http://www.fusioncharts.com/dev/usage-guide/getting-started/more-chart-types/3d-charts.html
        var dataSrc = {
            "categories": [{
                "category": []
            }],
            "dataset": []
        };

        /* Check if it's groupBy data or plain data */
        var _data = data.hasOwnProperty("caKeys") ? data.Row : data;

        //Add all the serieses and their attributes
        for (var i = 0; i < chartConfig.series.length; i++) {
            //copy all atributes
            dataSrc.dataset[i] = jQuery.extend(true, {}, chartConfig.series[i]);
            //create the blank array for series data
            dataSrc.dataset[i]["data"] = [];
            //Column attribute is needed for configuration definition but not needed for chart building
            if (dataSrc.dataset[i].column) {
                delete dataSrc.dataset[i].column;
            }
        }

        if (isZoomLine) {
            dataSrc.categories[0].category = $.pluck(data, chartConfig.xAxis.column).join('|');

            for (var k = 0; k < chartConfig.series.length; k++) {
                dataSrc.dataset[k].data = $.pluck(data, chartConfig.series[k].column).join('|');
            }
        }
        else {
            //Format all the columns first before looping through the data
            var seriesColumns = {};
            $.each(chartConfig.series, function (index, element) {
                seriesColumns[element.column] = CC.util.hexFormattedFieldName(element.column);
            });

            // Loop through the data
            for (var j = 0; j < _data.length; j++) {
                //create the label i.e. x axis label values
                dataSrc.categories[0].category[j] = { "label": _data[j][chartConfig.xAxis.column] };

                //loop over all the series
                //create the value i.e. y axis value for all the serieses
                for (var k = 0; k < chartConfig.series.length; k++) {
                    //dataSrc.dataset[k].data[j] = { "value": data[j][chartConfig.series[k].column] };
                    //dataSrc.dataset[k].data[j] = { "value": getValue(data, j, chartConfig.series[k].column, _data) };
                    //dataSrc.dataset[k].data[j] = { "value": getValue(data, j, CC.util.hexFormattedFieldName(chartConfig.series[k].column), _data) };
                    dataSrc.dataset[k].data[j] = { "value": getValue(data, j, seriesColumns[chartConfig.series[k].column], _data) };
                }
            }
        }
        return dataSrc;
    };

    var customChartConfig = {
        addNumberFormat: function (chartType) {
            return (chartType != 'pyramid' && chartType != 'funnel');
        }
    }

    //create object to fetch list data
    //var da = new CC.Data.SPList();

    //SharePoint list source from which data will be fetched
    //var spListSource = CC.med.providerConfig().DataSource.SPList;

    var buildChart = function (data) {
        //if (data.length > 0 && CC.Wizard.tabpanel) {
        if ((data.length > 0 || (data.Row && data.Row.length > 0)) && CC.Wizard.tabpanel) {
            CC.Wizard.tabpanel.tabs[0].toggleEnable(true);
        }

        var config = [];
        if (!dataEmptyMessage) {
            if (CC.util.isPageEdit()) {
                //var chartType = CC.util.getChartTypes()[chartConfig.ChartType];
                //var _cacheData = CC.med.charts().Cache.Data;
                //var _cacheData = CC.med.getSetting('cacheData');

                //if (!_cacheData || _cacheData.length == 0)
                //    CC.med.updateSetting('cacheData', data);
                //CC.med.charts().Cache.Data = data;
                if (cacheCurrentdata) {
                    cacheCurrentdata = false;
                    CC.med.updateSetting('cacheData', data);
                }
            }


            var chartType = CC.util.getChartSeries(chartConfig.chartType);
            //var config;
            switch (chartType) {
                case 0:
                    config = buildSingleSeriesData(data);
                    break;
                case 5:
                    config = buildMultiSeriesData(data, true);
                    break;
                default:
                    config = buildMultiSeriesData(data, false);
            }
        }
        else {
            config = { "data": [] };
        }

        var chartHeader = {};

        //$.extend(chartHeader, chartConfig.ChartCaptions || {}, cosmetics.DataPlot || {});
        $.extend(chartHeader, cosmetics.chart || {}, distinct || {});

        /* Compulsory settings */
        chartHeader["theme"] = chartConfig.theme;
        //chartHeader["showValues"] = cosmetics.dataPlot.showValues || "0";
        chartHeader["captionOnTop"] = "1";
        //chartHeader["showLegend"] = cosmetics.showLegend || "1";
        //chartHeader["showLabels"] = cosmetics.dataPlot.showLabels || "1";
        //chartHeader["caption"] = cosmetics.chart.caption || "0";
        //chartHeader["subcaption"] = cosmetics.chart.subCaption || "0";

        if (cosmetics.NumberFormatting && customChartConfig.addNumberFormat(chartConfig.chartType)) {
            chartHeader["formatNumber"] = cosmetics.NumberFormatting.formatNumber || "1";
            chartHeader["formatNumberScale"] = cosmetics.NumberFormatting.formatNumberScale || "1";
            if (chartHeader["formatNumber"] && cosmetics.NumberFormatting.formatStyle) {
                /*Apply this setting if a custom setting is applied && this setting is applicable to the current chart */
                if (cosmetics.NumberFormatting.formatStyle == "european") {
                    chartHeader["decimalSeparator"] = ',';
                    chartHeader["thousandSeparator"] = '.';
                }
                else {
                    chartHeader["decimalSeparator"] = '.';
                    chartHeader["thousandSeparator"] = ',';
                }
            }
        }

        //if (errorMsg) {
        //    chartHeader["dataLoadErrorMessage"] = errorMsg;
        //    chartHeader["renderErrorMessage"] = errorMsg;
        //    chartHeader["dataEmptyMessage"] = errorMsg;
        //}

        //#region test compulsory settings
        //if (chartConfig.chartType == 'pyramid') {
        //    /* Pyramid charts custom setting */
        //    chartHeader["alignCaptionWithCanvas"] = "1";
        //    chartHeader["showPercentValues"] = "0";
        //    chartHeader["chartLeftMargin"] = "40";
        //    chartHeader["captionPadding"] = "25";
        //}
        //else if (chartConfig.chartType == 'funnel') {
        //    /* Pyramid charts custom setting */
        //    chartHeader["isHollow"] = "1";
        //    chartHeader["showPercentValues"] = "0";
        //}

        ////for zoomline chart only
        //if (chartType == 5) {
        //    chartHeader["compactdatamode"] = "1";
        //    chartHeader["dataseparator"] = "|";
        //}
        //#endregion

        config["chart"] = chartHeader;

        ////Console.log(config);
        ////Console.log(chartBuilderScope);

        //#region fixed values for testing

        //config["chart"] = {
        //    "theme": "fint",
        //    "caption": "The Global Wealth Pyramid",
        //    "captionOnTop": "0",
        //    "captionPadding": "25",
        //    "alignCaptionWithCanvas": "1",
        //    "subcaption": "Credit Suisse 2013",
        //    "subCaptionFontSize": "12",
        //    "borderAlpha": "20",
        //    "is2D": "1",
        //    "bgColor": "#ffffff",
        //    "showValues": "1",
        //    "numberPrefix": "$",
        //    "numberSuffix": "M",
        //    "plotTooltext": "$label of world population is worth USD $value tn ",
        //    "showPercentValues": "1",
        //    "chartLeftMargin": "40"
        //}

        //config["data"] = [
        //{
        //    "label": "Top 32 mn (0.7%)",
        //    "value": "98.7"
        //},
        //{
        //    "label": "Next 361 mn (7.7%)",
        //    "value": "101.8"
        //},
        //{
        //    "label": "Next 1.1 bn (22.9%)",
        //    "value": "33"
        //},
        //{
        //    "label": "Last 3.2 bn (68.7%)",
        //    "value": "7.3"
        //}
        //];

        //#endregion

        FusionCharts.ready(function () {
            var collabChart = new FusionCharts({
                "type": chartConfig.chartType,
                "renderAt": chartBuilderScope.RenderArea,
                "width": chartBuilderScope.ChartWidth,
                //"height": chartBuilderScope.ChartHeight,
                "height": '100%',
                "dataFormat": "json",
                "dataSource": config,
                "dataEmptyMessage": dataEmptyMessage
            });

            collabChart.render();

            CC.util.toggleProgressBar(false);
        });
    };

    var addEditButton = function (renderArea) {
        if (chartBuilderScope.RenderArea == 'panel-wizard' && CC.util.isPageEdit() && !Ext.getCmp('cntrMain')) {
            /*Insert the edit button when
             * render-area is full
             * app is in edit mode
             * the wizard has not yet been triggerred
             */

            new Ext.Panel({
                id: 'invokeButton',
                renderTo: 'ccParent',
                border: false,
                html: '<div id="invokeBtn" onclick="javascript:InitWiz()"><span class="all-images editIcon"></span>' + CC.msgs.wzrd173 + '</div>'
            });
            $("#invokeButton").insertBefore("#panel-wizard");

            $('#panel-wizard').height($('#ccParent').height() - (($('#invokeBtn').height() + $('#lblPrevChartMsg').height()) * 2));
        }
    };

    //
    if (providerConfig.dataSource.data.id)
        CC.util.toggleProgressBar(true);



    var _cacheData = CC.med.getSetting('cacheData');
    var useCacheData = true;

    if (CC.util.isPageEdit() && _cacheData && _cacheData.length > 0) {
        var selFields = CC.med.getSetting('dsFld');
        $.each(selFields, function (index, element) {
            if (_cacheData[0][element] == undefined) {
                /* re-load data */
                useCacheData = false;
                return false;
            }
        });
    }
    else {
        useCacheData = false;
    }

    if (useCacheData || this.DataEmptyMessage) {
        //load cached data
        buildChart(_cacheData);
    }
    else if (providerConfig.dataSource.data.id) {
        /* Re-load data only if the source has already been configured */
        CC.med.currentProvider().getData(function (data) {
            if (CC.util.isPageEdit()) {
                var chartCosmetics = CC.med.getSetting('chart');
                if (chartCosmetics) {
                    CC.track.prototype.chartConfig({
                        isGrouped: data.hasOwnProperty("caKeys"),
                        isFiltered: data.hasOwnProperty("isViewFiltered") && data.isViewFiltered,
                        numberFormat: (chartCosmetics.decimalSeparator ? chartCosmetics.decimalSeparator == "," ? 'European' : 'Regular' : 'Regular'),
                        fieldCount: providerConfig.dataSource.selectedFields.length,
                        theme: CC.med.getSetting('theme'),
                        chart: CC.med.getSetting('cType')
                    });
                }
            }

            cacheCurrentdata = true;
            //da.getViewData(spListSource.ListID, spListSource.ViewID, spListSource.SelectedFields.join()).done(function (data) {
            buildChart(data);
            //}).fail(function (obj) {
            //    throw new CC.Exception("DataNotFetched", "Unable to fetch data from the data source.");
            //});
            //}
            //else
            //    buildChart(CC.med.charts().Cache.Data);

            addEditButton(chartBuilderScope.RenderArea);


        }, providerConfig.dataSource.data.id, providerConfig.dataSource.data.viewID, providerConfig.dataSource.selectedFields.join());
    }
    else {
        addEditButton(chartBuilderScope.RenderArea);
        CC.util.toggleProgressBar(false);
    }


}