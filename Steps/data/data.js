/*
 *
 * Data file
 *
 */
// Enums defenition
CC.Enums = {
    Provider: {
        Scope: { None: -1, Entity: 1, EntityAndView: 2, Query: 3, EntityAndQuery: 4, All: 5, CSV: 6, ConnectedWebPart: 7, ODBC: 8, SQLView: 9 },
        DataLayout: { Connection: 1, Plain: 2, All: 3 }
    },
    Styles: {
        Features: { Animation: 0, Font: 1, Blur: 2, Shadow: 3, Glow: 4, Bevel: 5 },
        AnimParams: {
            Alpha: ['_alpha', 'Alpha'], X: ['_x', 'X'], Y: ['_y', 'Y'], XScale: ['_xScale', 'X Scale'],
            YScale: ['_yScale', 'Y Scale'], Rotation: ['_rotation', 'Rotation']
        }
    },
    EntityScope: {
        Scope: { CurrentSite: 1, Url: 2, Others: 3 }
    },
    DynamicFilter: {
        Scope: { EnableFilter: true, CustomFields: 1, ChartFields: 2 },
        FilterType: { None: 0, FilterOnChartField: 1, FilterOnCustomField: 2 }
    },
    xlsRange: {
        Scope: { Worksheet: 1, NamedRange: 2 },
        RangeType: { Worksheet: 1, NamedRange: 2 }
    },
    numFormat: {
        Scope: { Regular: 1, European: 2 }
    }
}

// Enums aliases for easy usage
CC.E = {
    F: CC.Enums.Styles.Features,
    P: CC.Enums.Styles.AnimParams
}

 
 
// Templates definitions
CC.Templates = CC.Tpl = {
    Axis: {
        SingleSeries: {
            Categories: ['_fontbox'],
            DataSets: ['_ColumnNameX', '_ColumnNameZ', '_common', '_drawAnchors', '_line', '_showPlotBorder']
        }
    },

    Defaults: {
        Common: {
            palette: 1,
            paletteColors: '',
            captionPadding: 10,
            xAxisNamePadding: 5,
            yAxisNamePadding: 5,
            yAxisValuesPadding: 2,
            labelPadding: 3,
            valuePadding: 2,
            caption: '',
            subCaption: '',
            xAxisName: '',
            yAxisName: '',
            animation: true,
            defaultAnimation: true,
            logoURL: '',
            logoPosition: 'TL',
            logoAlpha: 100,
            logoLink: '',
            logoScale: 100,
            showFCMenuItem: true,
            aboutMenuItemLabel: 'About Collabion Charts',
            aboutMenuItemLink: 'n-http://www.collabion.com/',
            showPrintMenuItem: true,
            showExportDataMenuItem: false,
            exportDataMenuItemLabel: 'Copy data to clipboard',
            exportDataSeparator: ',',
            exportDataFormattedVal: 0,
            exportDataQualifier: '{quot}',
            exportDataLineBreak: '\r\n',
            showToolTip: true,
            toolTipBgColor: 'FFFFFF',
            toolTipBorderColor: '545454',
            toolTipSepChar: ', ',
            showToolTipShadow: false,
            seriesNameInToolTip: true,
            clickURL: '',
            bgSWF: '',
            bgSWFAlpha: 100,
            baseFont: 'Verdana',
            baseFontSize: 10,
            baseFontColor: '555555',
            outCnvBaseFont: 'Verdana',
            outCnvBaseFontSize: 10,
            outCnvBaseFontColor: '555555',
            showVLineLabelBorder: true,
            numberScaleUnit: 'K,M',
            numberScaleValue: '1000,1000',
            numberPrefix: '',
            numberSuffix: '',
            exportEnabled: false,
            exportShowMenuItem: false,
            exportFormats: 'JPG=Save as JPEG Image|PNG=Save as PNG Image|PDF=Save as PDF',
            exportAtClient: 0,
            exportAction: 'download',
            exportTargetWindow: '_self',
            //exportHandler: CC.Config.DefaultServerExportHandler,
            //html5ExportHandler: CC.Config.DefaultHtml5ExportHandler,
            exportFileName: 'Collabion Charts',
            exportParameters: '',
            exportCallback: 'FC_Exported',
            showExportDialog: true,
            exportDialogMessage: 'Capturing Data : ',
            exportDialogColor: 'FFFFFF',
            exportDialogBorderColor: '999999',
            exportDialogFontColor: '666666',
            exportDialogPBColor: 'E2E2E2',
            exportDataCaptureCallback: 'FC_ExportDataReady',
            unescapeLinks: true,
            showLabels: true,
            labelDisplay: 'AUTO',
            chartLeftMargin: 15,
            chartRightMargin: 15,
            chartTopMargin: 15,
            chartBottomMargin: 15,
            adjustDiv: true,
            formatNumber: true,
            formatNumberScale: true,
            defaultNumberScale: '',
            decimalSeparator: '.',
            thousandSeparator: ',',
            inDecimalSeparator: '',
            inThousandSeparator: '',
            yAxisValueDecimals: '',
            decimals: '',
            forceDecimals: false,
            forceYAxisValueDecimals: false,
            bgColor: '', //'CBCBCB,E9E9E9',
            bgAlpha: '50,50',
            bgRatio: '0,100',
            bgAngle: 270,
            showBorder: true,
            borderColor: '767575',
            borderThickness: 1,
            borderAlpha: 50,
            yAxisMinValue: undefined,
            yAxisMaxValue: undefined,
            yAxisNameWidth: ''
        },

        DivLine: {
            showDivLineValues: true,
            numDivLines: 4,
            divLineColor: '717170',
            divLineThickness: 1,
            divLineAlpha: 40,
            divLineIsDashed: false,
            divLineDashLen: 4,
            divLineDashGap: 2
        },

        Canvas: {
            canvasBgColor: 'FFFFFF',
            canvasBgAlpha: '100',
            canvasBgRatio: '',
            canvasBgAngle: 0,
            canvasBorderColor: '545454',
            canvasBorderThickness: 2,
            canvasBorderAlpha: 100,
            canvasPadding: 0,
            canvasLeftMargin: -1,
            canvasRightMargin: -1,
            canvasTopMargin: -1,
            canvasBottomMargin: -1
        },

        Legend: {
            showLegend: true,
            legendPosition: 'BOTTOM',
            legendCaption: '',
            legendBorderColor: '545454',
            legendBorderThickness: 1,
            legendBorderAlpha: 100,
            legendBgColor: 'FFFFFF',
            legendBgAlpha: 100,
            legendShadow: true,
            legendAllowDrag: false,
            legendScrollBgColor: 'CCCCCC',
            legendScrollBarColor: '545454',
            legendScrollBtnColor: '545454',
            reverseLegend: false,
            legendPadding: 6,
            interactiveLegend: true,
            legendIconScale: 1
        },

        Anchors: {
            drawAnchors: true,
            anchorSides: 10,
            anchorRadius: 3,
            anchorBorderColor: '767575',
            anchorBorderThickness: 1,
            anchorBgColor: 'FFFFFF',
            anchorAlpha: 100,
            anchorBgAlpha: 100
        },

        PlotBorder: {
            showPlotBorder: true,
            plotBorderColor: '333333',
            plotBorderThickness: 1,
            plotBorderAlpha: 95,
            plotBorderDashed: false,
            plotBorderDashLen: 5,
            plotBorderDashGap: 4,
            plotFillAngle: 270,
            plotFillAlpha: '100',
            plotGradientColor: 'FFFFFF',
            showGradientColor: true,
            plotSpacePercent: 20
        }
    },

    Related: [
    //commented due to export settings changes, should be removed
    //{base:'_exportEnabled', relativeCheck: ['_showExportDialog']},
        {base: '_exportEnabled' },
		{ base: '_rotateyaxisname', relativeUncheck: ['atfsFitYAxisTitle'] }
	],

    Locked: [
    // Chart & Axis Titles
		'_SYAXisName',
    // Chart Functionality
		'dpLineDataPlotTab',
		'dpScrollSettingsTab',
		'dpStackedSettingsTab',
		'dpAnchorsTab',
		'dpFunnelPyramidTab',
		'dpWaterfallCascadeTab',
		'dpRadarTab',
		'dpParetoTab',
		'dpBubbleScatterTab',
		'dpMarimekkoTab',
		'dpZoomLineTab',
		'dpPieDoughnutTab',
		'_maxLabelWidthPercent',
		'_manageLabelOverflow',
		'_valuePosition',
		'_showSum',
		'_showPercentageValues',
		'btnDATAPLOTCOLUMN',
		'btnDATAPLOTAREA',
		'_useRoundEdges',
		'_showPercentInToolTip',
    // Visual Appearance
		'ccfs3DCanvas',
		'ccZeroPlaneTab3DChart',
		'ccSmartLabelsTab',
    // Axis Grid Lines
		'_showdivlinesecondaryvalue',
		'dlDualYAttr',
		'dlVerticalGridLinesTab',
    // Number Formatting
		'nfSecondNumberFormat',
		'nfSecondNumberParetoFormat',
    // Trend Lines
		'vTrendLines'
	] ,

    Styles: {
        Features: {
            Def1: [CC.E.F.Animation, CC.E.F.Font, CC.E.F.Blur, CC.E.F.Bevel, CC.E.F.Glow, CC.E.F.Shadow],
            Def2: [CC.E.F.Animation, CC.E.F.Blur, CC.E.F.Bevel, CC.E.F.Glow, CC.E.F.Shadow],
            Def3: [CC.E.F.Font, CC.E.F.Blur, CC.E.F.Bevel, CC.E.F.Glow, CC.E.F.Shadow],
            Def4: [CC.E.F.Blur, CC.E.F.Bevel, CC.E.F.Glow, CC.E.F.Shadow],
            Def5: [CC.E.F.Font]
        }
    }
}

// Personal information for each chart.
// Chart object structure:
//  locked - locked (disabled) controls on the wizrd;
//  styles - style features for chart;
//  defaults - default walues for chart (xml) attributes.
CC.Data = function(){}
CC.Data.prototype = {
	// Empty chart configuration structure
	Empty: {
		limitDataSets: undefined,
		locked: {
			categories: [],
			datasets: [],
			datasetsadd: {
				renderas: {},
				parenty: {}
			},
			chart: [],
			trendlines: []
		},
		defaults: {},
		forcedDefaults: {},
		related:[],
		styles: {
			features: [],
			animationParams: []
		}
	},
	
	Column2D: {
		limitDataSets: 1,
		
		defaults: util.merge([
			CC.Tpl.Defaults.Common,
			CC.Tpl.Defaults.Canvas,
			CC.Tpl.Defaults.DivLine,
			CC.Tpl.Defaults.PlotBorder,
			{
			canvasPadding: undefined,
		
			setAdaptiveYMin: false,
			rotateLabels: '',
			slantLabels: false,
			labelStep: 1,
			staggerLines: 2,
			showValues: true,
			rotateValues: false,
			placeValuesInside: false,
			showYAxisValues: 1,
			showLimits: true,
			yAxisValuesStep: 1,
			showColumnShadow: false,
			rotateYAxisName: true,
			yAxisNameWidth: undefined,
			useRoundEdges: 0,
			showZeroPlane: true,
			zeroPlaneColor: '717170',
			zeroPlaneThickness: 2,
			zeroPlaneAlpha: 80,
			showAlternateHGridColor: true,
			alternateHGridColor: 'EEEEEE',
			alternateHGridAlpha: 50
		}]),
		
		
		locked: {
			categories: CC.Tpl.Axis.SingleSeries.Categories,
			datasets: CC.Tpl.Axis.SingleSeries.DataSets,
			
			chart: util.modify(CC.Tpl.Locked, {
				add: ['_canvasPadding', 'legendStep'],
				del: ['_useRoundEdges']
			}),
			
			trendlines: ['_parentYAxis']
		},
		
		related: CC.Tpl.Related.concat([
			{base:'_showGradientColor', relativeCheck: ['_plotGradientColor']},
			{base:'_useRoundEdges', relativeUncheck: ['_showShadow']}
		]),
		
		styles: {
			BACKGROUND: {
				features: CC.Tpl.Styles.Features.Def2,
				animationParams: [CC.E.P.Alpha, CC.E.P.X, CC.E.P.Y, CC.E.P.XScale, CC.E.P.YScale]
			},
			
			CANVAS: {
				features: CC.Tpl.Styles.Features.Def2,
				animationParams: [CC.E.P.Alpha, CC.E.P.X, CC.E.P.Y, CC.E.P.XScale, CC.E.P.YScale]
			},
			
			CAPTION: {
				features: CC.Tpl.Styles.Features.Def1,
				animationParams: [CC.E.P.Alpha, CC.E.P.X, CC.E.P.Y]
			},
			
			DATALABELS: {
				features: CC.Tpl.Styles.Features.Def1,
				animationParams: [CC.E.P.Alpha, CC.E.P.X, CC.E.P.Y]
			},
			
			DATAPLOT: {
				features: CC.Tpl.Styles.Features.Def2,
				animationParams: [CC.E.P.Alpha, CC.E.P.X, CC.E.P.Y, CC.E.P.XScale, CC.E.P.YScale]
			},
			
			DATAVALUES: {
				features: CC.Tpl.Styles.Features.Def1,
				animationParams: [CC.E.P.Alpha, CC.E.P.X, CC.E.P.Y]
			},
			
			DIVLINES: {
				features: CC.Tpl.Styles.Features.Def2,
				animationParams: [CC.E.P.Alpha, CC.E.P.Y, CC.E.P.YScale]
			},
			
			HGRID: {
				features: CC.Tpl.Styles.Features.Def2,
				animationParams: [CC.E.P.Alpha, CC.E.P.Y, CC.E.P.XScale, CC.E.P.YScale]
			},
			
			SUBCAPTION: {
				features: CC.Tpl.Styles.Features.Def1,
				animationParams: [CC.E.P.Alpha, CC.E.P.X, CC.E.P.Y]
			},
			
			TOOLTIP: {
				features: [CC.E.F.Font]
			},
			
			TRENDLINES: {
				features: CC.Tpl.Styles.Features.Def2,
				animationParams: [CC.E.P.Alpha, CC.E.P.Y, CC.E.P.XScale, CC.E.P.YScale]
			},
			
			TRENDVALUES: {
				features: CC.Tpl.Styles.Features.Def1,
				animationParams: [CC.E.P.Alpha, CC.E.P.X, CC.E.P.Y]
			},
			
			VLINES: {
				features: CC.Tpl.Styles.Features.Def2,
				animationParams: [CC.E.P.Alpha, CC.E.P.X, CC.E.P.Y, CC.E.P.YScale]
			},
			
			VLINELABELS: {
				features: CC.Tpl.Styles.Features.Def1,
				animationParams: [CC.E.P.Alpha]
			},
			
			XAXISNAME: {
				features: CC.Tpl.Styles.Features.Def1,
				animationParams: [CC.E.P.Alpha, CC.E.P.X, CC.E.P.Y]
			},
			
			YAXISNAME: {
				features: CC.Tpl.Styles.Features.Def1,
				animationParams: [CC.E.P.Alpha, CC.E.P.X, CC.E.P.Y]
			},
			
			YAXISVALUES: {
				features: CC.Tpl.Styles.Features.Def1,
				animationParams: [CC.E.P.Alpha, CC.E.P.X, CC.E.P.Y, CC.E.P.Rotation]
			}
		}
	}
};
