CC.Wizard = {    
    // Wizard window handler
    hwnd: undefined,
	renderArea: undefined,
	tabpanel: undefined
};

CC.Wizard = function (config) {
    this.config = config;
}

CC.Wizard.prototype = {
    // Initialize wizard components
    init: function () {
		CC.Wizard.tabpanel = new CC.Tabs.Panel({level: 1});
    },

    // Create wizard window
    createWizard: function () {
        this.init();
		
		var pnl = new Ext.Container({
			width: CC.Wizard.renderArea.getWidth(),
			height: CC.Wizard.renderArea.getHeight(),
			items: [{
				xtype: 'container',
				id: 'cntrMain',
				layout: 'border',
				height: "100%",
				items: [CC.Wizard.tabpanel.getPanel(),{
					split  : true,
					region: 'south',
					height: CC.Wizard.renderArea.getHeight()/2.2,
					autoScroll: true,
					items: [{
						xtype: 'combo',
						width: 200,
						listWidth: 200,
						fieldLabel: 'Select List',
						editable: false
					}, {
						xtype: 'combo',
						width: 200,
						listWidth: 200,
						fieldLabel: 'Select View',
						editable: false
					}],
					listeners:{
						scope: this,
						resize: function(){
							util.closePopup(100);
						}
					}
				}]
			}]
		});
		
		this.win = new Ext.Panel({
            title: this.config.title,
            id: 'wizPanel',			
            normal: true,
            layout: 'fit',
			autoScroll: false,
			height: CC.Wizard.renderArea.getHeight(),
			width: CC.Wizard.renderArea.getWidth(),
            renderTo: CC.Wizard.renderArea.id,	
			buttonAlign: 'left',
			items: [pnl],
			bodyStyle: 'padding: 0px 6px 6px 6px;',
			bodyCfg : { cls:'x-wiz-body' },			
			listeners: {
                scope: this,
                afterrender: function (s) {	
					var header = s.header;
					header.dom.style.borderBottomWidth = 0;					
					header.setHeight(50);
					
                    //s.el.first().dom.style.height = 60;
					s.el.first().dom.style.position = 'relative';					
					s.el.first().dom.firstChild.style.position = 'absolute';
					s.el.first().dom.firstChild.style.top = '25%';
					s.el.first().dom.firstChild.style.marginLeft ='30';
					
					var para = document.createElement("div");
					para.setAttribute('class', 'x-app-logo');
				    s.el.first().dom.insertBefore(para, s.el.first().dom.firstChild);
					
					Ext.getCmp('wizPanel').setHeight(CC.Wizard.renderArea.getHeight());
                }
            }
        });
		
		
		
        this.win.on({
            destroy: this.afterclose,
            scope: this
        });
    },

    // Show wizard window
    show: function () {
        if (!this.win) {
            this.createWizard();
        }
        this.win.show();
    },

    // This event is rised after close the wizard window
    afterclose: function () {
        this.onafterclose();
    },

    // Close wizard window
    close: function () {
        this.win.destroy();
		this.win = null;
    },

    // Finish button event
    finish: function () {
		this.confirmedFinish();		
    },

    confirmedFinish: function () {
		this.onfinish();
		this.close(this)
    },
	
	cancel: function () {
		this.oncancel();
		this.close(this)
	},
    // User's handlers
    onfinish: Ext.emptyFn,
    oncancel: Ext.emptyFn

}

function InitWiz() {	
	CC.Wizard.renderArea = Ext.get('panel-wizard');
	
    Ext.onReady(function () {		
		var config = { title: ' '};
		CC.Wizard.hwnd = new CC.Wizard(config);
		
		CC.Wizard.hwnd.onfinish = function () {	
			alert('Finish');
		};
		
		CC.Wizard.hwnd.onbeforeclose = function () {		
			alert('onbeforeclose');
		};
		
		CC.Wizard.hwnd.onafterclose = function () {	
			alert('onafterclose');
		};
		
		CC.Wizard.hwnd.oncancel = function () {	
			alert('oncancel');
		};
		CC.Wizard.hwnd.show();				
    });
};