CC.Wizard = {
    // Wizard window handler
    hwnd: undefined,
    renderArea: undefined,
    tabpanel: undefined
};

CC.Wizard = function (config) {
    this.config = config;
}

CC.Wizard.prototype = {
    // Initialize wizard components
    init: function () {
        CC.Wizard.tabpanel = new CC.Tabs.Panel({ level: 1 });
    },

    // Create wizard window
    createWizard: function () {
        this.init();

        var pnl = new Ext.Container({
            width: CC.Wizard.renderArea.getWidth(),
            height: CC.Wizard.renderArea.getHeight(),
            items: [{
                xtype: 'container',
                id: 'cntrMain',
                layout: 'border',
                height: "100%",
                items: [CC.Wizard.tabpanel.getPanel(), {
                    id: 'cntrPreview',
                    split: true,
                    region: 'south',
                    height: CC.Wizard.renderArea.getHeight() / 2.2,
                    autoScroll: true,
                    minHeight: 235,
                    maxHeight: 311,
                    items: [{
                        // xtype: 'label',
                        // text: '=',
                        //cls: 'x-layout-split x-layout-split-south x-splitbar-v x-form-item',
                        id: 'lblSplitter',
                        cls: 'x-form-item',
                        border: false,
                        html: '<span class="all-images" style="height: 6px; width: 20px; background-position: -16px -158px;"></span>',
                        style: {
                            'fontSize': '27px',
                            'width': '100%',
                            'height': '25px',
                            'background-color': 'transparent',
                            // 'background': 'url("../CCSP_APP/assets/splitter.png")',
                            // 'background-repeat': 'no-repeat',
                            // 'background-position': '300px 15px',
                            // 'color': '#B1B1B1',
                            'cursor': 'row-resize',
                            'text-align': 'center',
                            'margin-top': '-10px',
                            'margin-bottom': '-4px'
                        },
                        listeners: {
                            afterrender: function () {
                                this.el.dom.firstChild.firstChild.style.backgroundColor = "transparent";
                            }
                        }
                    }, {
                        xtype: 'container',
                        id: 'frmTheme',
                        border: false,
                        layout: 'table',
                        layoutConfig: { columns: 2 },
                        items: [{
                            id: 'lblTheme',
                            xtype: 'label',
                            //cls: 'x-form-item',
                            cls: 'x-form-item-sub',
                            text: CC.msgs.wzrd171
                        }, {
                            id: 'cmbTheme',
                            cls: 'preview-panel-elements',
                            //xtype: 'combo',
                            xtype: 'cc:combo',
                            width: 140,
                            height: 50,
                            listWidth: '127px',
                            fieldLabel: 'Theme',
                            //editable: false,
                            //triggerAction: 'all',
                            store: ['Standard', 'Zune', 'Carbon', 'Ocean'],
                            //store: ['Zune', 'Carbon', 'Ocean'],
                            value: 'Zune',
                            listeners: {
                                afterrender: function (sender) {
                                    this.el.parent().dom.children[0].style.paddingRight = "25px";
                                    this.el.parent().dom.children[1].style.left = "106px";
                                    this.listWidth += 5;

                                    var selTheme = CC.med.getSetting('theme');
                                    if (selTheme) {
                                        sender.setValue(selTheme.charAt(0).toUpperCase() + selTheme.slice(1));
                                    }
                                },
                                select: function (c, r, i) {
                                    CC.med.publish(CC.util.tabsActivate(this.id), { val: r.data.field1.toLowerCase() });
                                },
                                beforeselect: function (c, r, i) {
                                    if (c.getValue() != r.data.field1) {
                                        CC.track.prototype.themeUpdate(r.data.field1, c.getValue());
                                    }
                                }
                            }
                        }]
                    }, {
                        id: 'pnlPrev',
                        xtype: 'panel',
                        cls: 'preview-panel-elements',
                        BoxMinHeight: 250,
                    }, {
                        xtype: 'container',
                        layout: 'table',
                        layoutConfig: { columns: 2 },
                        items: [{
                            xtype: 'label',
                            cls: 'x-form-item-sub',
                            style: {
                                'bottom': '12px',
                                'position': 'absolute',
                                'padding-left': '12px',
                                'color': 'grey',
                                'font-size': '11px'
                            },
                            text: CC.msgs.wzrd175
                        }, {
                            id: 'btnDone',
                            cls: 'preview-panel-elements',
                            xtype: 'button',
                            multiSelect: true,
                            width: 74,
                            height: 28,
                            text: CC.msgs.wzrd172,
                            config: {
                                ui: 'plain',
                                style: 'background-color:white;color:red'
                            },
                            handler: function () {
                                //CC.Properties.prototype.SetPropertyValue();
                                //CC.Data.SPList.prototype.setAppConfig();
                                //CC.Properties.AppProperty.Set(this);

                                CC.med.saveSetting(this);

                                //Ext.Msg.show({
                                //    title: "Alert",
                                //    msg: "Chart made successfully",
                                //    buttons: Ext.Msg.OK,
                                //    fn: function (btn) {
                                //        if (btn === 'ok') {
                                //            /* close the wizard */
                                //            CC.med.resetWizard();
                                //            //CC.med.createChart('panel-wizard');
                                //        }
                                //    }
                                //});
                            }
                        }]
                    }],
                    listeners: {
                        afterlayout: function () {
                            // Adding icons to tabs
                            var parentspan = $('.x-tab-strip-inner');
                            if (parentspan.find('.all-images').length == 0) {
                                $.each(parentspan, function (index, value) {
                                    var el = document.createElement('span');
                                    el.style["float"] = "left";
                                    el.style["margin-top"] = "2px";
                                    CC.util.addCssClass(el, ['all-images']);
                                    switch (index) {
                                        case 0: CC.util.addCssClass(el, ['x-data-icon']);
                                            break;
                                        case 1: CC.util.addCssClass(el, ['x-charts-icon']);
                                            break;
                                        case 2: CC.util.addCssClass(el, ['x-labels-icon']);
                                            break;
                                        case 3: CC.util.addCssClass(el, ['x-numbers-icon']);
                                            break;
                                    }
                                    $(value).prepend(el);
                                });
                            }
                        },
                        resize: function (a, size) {
                            CC.util.closePopup(100);
                        },
                        beforeresize: function (a) {
                            //Console.log("Resize called");
                        },
                        move: function (a, newSize) {
                        }
                    }
                }]
            }],
            listeners: {
                afterlayout: function () {
                    // Ext.select ("div#ext-comp-1051-xsplit").on ("mousedown", (
                    // function (e, t) {
                    // e.cancelEvent();   // doesn't work
                    // e.stopEvent();
                    // return false;        // doesn't work either
                    // }
                    // ));

                    var splitPnl = $('#cntrMain').find('div.x-splitbar-v');
                    if (splitPnl.length == 1) {
                        //splitPnl.removeClass();
                        splitPnl.css("cursor", "default");
                    }
                }
            }
        });

        this.win = new Ext.Panel({
            title: this.config.title,
            id: 'wizPanel',
            normal: true,
            layout: 'fit',
            autoScroll: false,
            height: CC.Wizard.renderArea.getHeight(),
            width: CC.Wizard.renderArea.getWidth(),
            renderTo: CC.Wizard.renderArea.id,
            buttonAlign: 'left',
            items: [pnl],
            bodyStyle: 'padding: 0px 6px 6px 6px;',
            bodyCfg: { cls: 'x-wiz-body' },
            listeners: {
                scope: this,
                afterrender: function (s) {
                    var header = s.header;
                    header.dom.style.borderBottomWidth = 0;
                    header.setHeight(50);

                    //s.el.first().dom.style.height = 60;
                    s.el.first().dom.style.position = 'relative';
                    s.el.first().dom.firstChild.style.position = 'absolute';
                    s.el.first().dom.firstChild.style.top = '25%';
                    s.el.first().dom.firstChild.style.marginLeft = '30';

                    var para = document.createElement("span");
                    para.setAttribute('class', 'all-images app-logo');
                    para.setAttribute('style', 'margin-top: 5px;');
                    s.el.first().dom.insertBefore(para, s.el.first().dom.firstChild);

                    Ext.getCmp('wizPanel').setHeight(CC.Wizard.renderArea.getHeight());
                }
            }
        });

        this.win.on({
            destroy: this.afterclose,
            scope: this
        });
    },

    // Show wizard window
    show: function () {
        if (!this.win) {
            this.createWizard();
        }
        this.win.show();
    },

    // This event is rised after close the wizard window
    afterclose: function () {
        this.onafterclose();
    },

    // Close wizard window
    close: function () {
        this.win.destroy();
        this.win = null;
    },

    // Finish button event
    finish: function () {
        this.confirmedFinish();
    },

    confirmedFinish: function () {
        this.onfinish();
        this.close(this)
    },

    cancel: function () {
        this.oncancel();
        this.close(this)
    },
    // User's handlers
    onfinish: Ext.emptyFn,
    oncancel: Ext.emptyFn

}

function InitWiz() {
    CC.util.toggleProgressBar(true);

    if (Ext.getCmp('invokeButton')) {
        //Permanently remove the button triggering the wizard
        //This should be re-created after the user hits the DONE button
        Ext.getCmp('invokeButton').destroy();
    }
    //CC.Med = CC.Mediator.getInstance();

    ////Trigger the auto-save timer applicable only when the wizard is active
    //CC.med.loadAppProperty();

    CC.Wizard.renderArea = Ext.get('panel-wizard');

    Ext.onReady(function () {
        CC.util.removeCssClass($('#panel-wizard')[0], ['noSetting']);

        var config = { title: ' ' };
        CC.Wizard.hwnd = new CC.Wizard(config);

        CC.Wizard.hwnd.onfinish = function () {
            alert('Finish');
        };

        CC.Wizard.hwnd.onbeforeclose = function () {
            alert('onbeforeclose');
        };

        CC.Wizard.hwnd.onafterclose = function () {
            alert('onafterclose');
        };

        CC.Wizard.hwnd.oncancel = function () {
            alert('oncancel');
        };

        try {
            CC.track.prototype.wizardInitiated();

            $('#panel-wizard').css('border-width', '0px');

            //Trigger the auto-save timer applicable only when the wizard is active
            CC.med.loadAppProperty();

            CC.Wizard.hwnd.show();
        }
        catch (error) {


            CC.med.logError({}, '120', 'TabCreate', error.body ? error.body : error.message ? error.message : error);
        }

        //if (!CC.med.providerConfig().currentProvider) {
        //    //setting entity as SPList
        //    CC.med.providerConfig().selectProvider(0);
        //}

        //CC.Stores.create();
        //FetchSpList();

        //new Ext.Panel({
        //    id: 'overlayPanel',
        //    //header: false,
        //    renderTo: 'wizPanel',
        //    hidden: true,
        //    html: '<div id="overlaydiv" runat="server" class="modal" style="text-align: center;"><img src="../CCSP_APP/assets/preloader.gif"><span id="ProgressTextSpan" class="ms-accentText" style="font-family: &quot;Segoe UI Light&quot;, &quot;Segoe UI&quot;, Tahoma, Helvetica, Arial, sans-serif; font-size: 36px;">  Rendering Data...</span></div>',
        //    // items: [{
        //    // // html: '<dialog class="modal"> Rendering Data...</dialog>'
        //    // html: '<div id="overlaydiv" runat="server" class="modal" style="text-align: center;"><img src="data:image/gif;base64,R0lGODlhGAAYAJECAP///5mZmf///wAAACH/C05FVFNDQVBFMi4wAwEAAAAh+QQFCgACACwAAAAAGAAYAAACQJQvAGgRDI1SyLnI5jr2YUQx10eW5hmeB6Wpkja5SZy6tYzn+g5uMhuzwW6lFtF05CkhxGQm+HKuoDPplOlDFAAAIfkEBQoAAgAsFAAGAAQABAAAAgVUYqeXUgAh+QQFCgACACwUAA4ABAAEAAACBVRip5dSACH5BAUKAAIALA4AFAAEAAQAAAIFVGKnl1IAIfkEBQoAAgAsBgAUAAQABAAAAgVUYqeXUgAh+QQFCgACACwAAA4ABAAEAAACBVRip5dSACH5BAUKAAIALAAABgAEAAQAAAIFVGKnl1IAIfkECQoAAgAsBgAAAAQABAAAAgVUYqeXUgAh+QQJCgACACwAAAAAGAAYAAACJZQvEWgADI1SyLnI5jr2YUQx10eW5omm6sq27gvH8kzX9o3ndAEAIfkECQoAAgAsAAAAABgAGAAAAkCULxFoAAyNUsi5yOY69mFEMddHluYZntyjqY3Vul2yucJo5/rOQ6lLiak0QtSEpvv1lh8l0lQsYqJHaO3gFBQAACH5BAkKAAIALAAAAAAYABgAAAJAlC8RaAAMjVLIucjmOvZhRDHXR5bmGZ7co6mN1bpdsrnCaOf6zkOpzJrYOjHV7Gf09JYlJA0lPBQ/0ym1JsUeCgAh+QQJCgACACwAAAAAGAAYAAACQJQvEWgADI1SyLnI5jr2YUQx10eW5hme3KOpjdW6XbK5wmjn+s5Dqcya2Dox1exn9PSWJeRNSSo+cR/pzOSkHgoAIfkECQoAAgAsAAAAABgAGAAAAkCULxFoAAyNUsi5yOY69mFEMddHluYZntyjqY3Vul2yucJo5/rOQ6nMmtg6MdXsZ/T0liXc6jRbOTHR15SqfEIKACH5BAkKAAIALAAAAAAYABgAAAJAlC8RaAAMjVLIucjmOvZhRDHXR5bmGZ7co6mN1bpdsrnCaOf6zkO4/JgBOz/TrHhC9pYRpNJnqURLwtdT5JFGCgAh+QQJCgACACwAAAAAGAAYAAACPpQvEWgADI1SyLnI5jr2YUQx10eW5jme3NOpTWe5Qpu6tYzn+l558tWywW4lmk/IS6KOr2UtSILOYiYiUVAAADs="><span id="ProgressTextSpan" class="x-form-item" style="font-size: 36px; font-weight: 300; display: initial;">  Rendering Data...</span></div>'
        //    // }]
        //});




        //var splitBar = $('#cntrMain').children('.x-splitbar-v');
        $("#lblSplitter").on("mousedown", function (a) {

            var e1 = document.createEvent("MouseEvents");
            e1.initMouseEvent("mousedown", true, true, window, 1, a.originalEvent.screenX, (a.originalEvent.screenY), a.originalEvent.clientX, (a.originalEvent.clientY), false, false, false, false, 0, null);
            e1.ccNotOverTarget = true;

            if (e1.pageX == 0 && e1.pageY == 0) {
                e1.ccx = a.originalEvent.pageX;
                e1.ccy = a.originalEvent.pageY;
            }

            //jQuery event
            // var ev = jQuery.Event( "mousedown", {
            // which: 1,
            // pageX: a.originalEvent.pageX,
            // pageY: a.originalEvent.pageY,
            // screenX: a.originalEvent.screenX,
            // screenY: a.originalEvent.screenY,
            // clientX: a.originalEvent.clientX,
            // clientY: a.originalEvent.clientY,
            // offsetX: a.originalEvent.offsetX,
            // offsetY: a.originalEvent.offsetY
            // });

            //$('#ext-comp-1051-xsplit')[0].dispatchEvent(e1);

            //dispatch the actual splitter event
            $('#cntrMain').children('.x-splitbar-v')[0].dispatchEvent(e1);


            //$('#ext-comp-1051-xsplit')[0].dispatchEvent(a);
            //$('#ext-comp-1051-xsplit')[0].fireEvent("on" + e1.eventType, e1);
            //$('#ext-comp-1051-xsplit').trigger(ev);
        });

        // Ext.select ("div#ext-comp-1051-xsplit").on ("mousedown", {
        // function (e, t) {
        // //
        // //e.cancelEvent();   // doesn't work
        // e.stopEvent();
        // //return false;        // doesn't work either
        // }
        // });

        //Ext.select('ext-comp-1051-xsplit').on('mousedown', function(e){
        //
        //e.stopEvent();
        //});
        //}
    });
};

////Loads the current sites Lists into the select List combo
//function FetchSpList() {
//    CC.med.currentProvider().getAllDataSource(function (lists) {
//        CC.Stores.Lists.loadData(lists);
//        Ext.getCmp("ccList").bindStore(CC.Stores.Lists);

//        CC.util.toggleProgressBar();
//    });
//}