/*!
 * Ext JS Library 3.1.0
 * Copyright(c) 2006-2009 Ext JS, LLC
 * licensing@extjs.com
 * http://www.extjs.com/license
 */
Ext.ns('Ext.cux.form');

/**
 * @class Ext.cux.form.SpinnerField
 * @extends Ext.form.NumberField
 * Creates a field utilizing Ext.cux.Spinner
 * @xtype spinnerfield
 */
Ext.cux.form.SpinnerField = Ext.extend(Ext.form.NumberField, {
    actionMode: 'wrap',
	width: 50,
	//width: 145,
    deferHeight: true,
    autoSize: Ext.emptyFn,
    //onBlur: Ext.emptyFn,
    adjustSize: Ext.BoxComponent.prototype.adjustSize,

	constructor: function(config) {
		var spinnerConfig = Ext.copyTo({}, config, 'incrementValue,alternateIncrementValue,accelerate,defaultValue,triggerClass,splitterClass,rightLeftBtns,ccTip');
		
		var spl = this.spinner = new Ext.cux.Spinner(spinnerConfig);

		var plugins = config.plugins
			? (Ext.isArray(config.plugins)
				? config.plugins.push(spl)
				: [config.plugins, spl])
			: spl;

		Ext.cux.form.SpinnerField.superclass.constructor.call(this, Ext.apply(config, {plugins: plugins}));
	},

    // private
    getResizeEl: function(){
        return this.wrap;
    },

    // private
    getPositionEl: function(){
        return this.wrap;
    },

    // private
    alignErrorIcon: function(){
        if (this.wrap) {
            this.errorIcon.alignTo(this.wrap, 'tl-tr', [2, 0]);
        }
    },

    validateBlur: function(){
        return true;
    }
});

Ext.reg('spinnerfield', Ext.cux.form.SpinnerField);

//backwards compat
Ext.form.SpinnerField = Ext.cux.form.SpinnerField;
