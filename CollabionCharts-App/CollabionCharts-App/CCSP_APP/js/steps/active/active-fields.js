﻿CC.Tabs.Active.prototype.fields = {

    //componentLoaded: false,
    activate: function () {
        var yAxis = Ext.getCmp('cntrYAxis');

        if (CC.med.isChartConfigExist()) {
            //var _chartConfig = CC.med.chartConfig().Configuration;
            var chartSeries = CC.med.getSetting('series');

            //var itemExist = (yAxis.items != undefined);

            if (yAxis.items && yAxis.items.items) {
                if (yAxis.items.items.length > chartSeries.length) {
                    //remove extra items
                    for (var i = yAxis.items.items.length - 1; i > chartSeries.length - 1; i--) {
                        yAxis.remove(yAxis.items.items[i].id);
                    }
                }
            }

            //this.alterChartSeries();

            var currentScope = this;
            //var _addYAxis = this.addYAxis;
            $.each(chartSeries, function (index, value) {

                //check if yAxis has child items. If yes, then check if it has item at the same index
                if (yAxis.items != undefined && yAxis.items.items[index] != undefined) {
                    //Set value of existing item
                    yAxis.items.items[index].items.items[0].setValue(value.column);
                }
                else {
                    //Add new item
                    //CC.Wizard.tabpanel.tabs[0].steps[1].addYAxis(value.Column);
                    currentScope.addYAxis(value.column);
                }

                //set the combination axis settings
                if (CC.util.isCombinationChart()) {
                    //combination chart
                    //var outVal = currentScope.getComboChartConfig(1, 'bar');

                    if (yAxis.items.itemAt(index).el) {
                        //$(yAxis.items.itemAt(index).el.dom).find(".yaxis2 span").css({ "background-position": currentScope.getComboChartConfig(1, value.renderAs) + "px -143px" });
                        //$(yAxis.items.itemAt(index).el.dom).find(".yaxis1 span").css({ "background-position": currentScope.getComboChartConfig(0, value.renderAs) + "px -143px" });

                        //

                        currentScope.applyComboChartSetting(index, 1, value.renderAs);
                        //currentScope.applyComboChartSetting(index, 0, value.renderAs);
                        //if (CC.med.getSetting('cType') == 'mscombidy2d') {
                        if (CC.util.isDualYAxis()) {
                            /* Dual Y Axis*/
                            currentScope.applyComboChartSetting(index, 0, CC.med.getSetting('series')[index].parentYAxis);
                        }
                        else {
                            /* remove Dual Y Axis style */
                            currentScope.removeSelectedStyles(index, 1);
                        }
                    }
                    //else {
                    //    return false;
                    //}
                }
                else {
                    /* remove all combo-chart styles */
                    currentScope.removeSelectedStyles(index, 1);
                    currentScope.removeSelectedStyles(index, 2);
                }
            });

            //this.alterChartSeries();

            //this.componentLoaded = true;

            var layoutTask = new Ext.util.DelayedTask(function () {
                yAxis.doLayout();
            });
            this.clearAllComboErrorCls();

            layoutTask.delay(20);
            //yAxis.doLayout();
        }
        else if (!yAxis.items || yAxis.items.items.length == 0) {
            /* add a default blank dropDown */
            this.addYAxis();
        }

        CC.util.generateTip('top');
    },

    /* Adds a new series */
    addYAxis: function (selVal) {

        var rowCnt = Ext.id().replace("ext-gen", "");
        var yAxis = Ext.getCmp('cntrYAxis');
        //var axisPanel = new CC.Tabs.Axis;
        var axisPanel = new CC.Tabs.Tab.Data.DataFields.Axis;

        //var newCmpnt = yAxis.insert(yAxis.items.length, axisPanel.create((rowCnt + '-pnl'), (rowCnt + '-btn')));
        var newCmpnt;

        var seriesCount = CC.util.getSeriesCount();
        if (yAxis.items != undefined && yAxis.items.length > 0) {
            newCmpnt = yAxis.insert(yAxis.items.length, axisPanel.create((rowCnt + '-pnl'), (rowCnt + '-btn'), seriesCount));

            CC.track.prototype.productAction('add', seriesCount, {});

            if (yAxis.items.items[0].items.items[3].el) {
                //run only when the item is rendered

                yAxis.items.items[0].items.items[3].el.dom.firstChild.firstChild.lastChild.disabled = false;

                //Disabling Add Axis option
                if (yAxis.items.items.length >= yAxis.items.items[0].items.items[0].store.data.length - 1) {
                    $('#addYBtn')[0].disabled = true;
                    if ($('#addYBtn')[0].classList)
                        $('#addYBtn')[0].classList.add('x-item-disabled');
                    else
                        $('#addYBtn')[0].className += ' x-item-disabled';
                }
            }
        }
        else
            newCmpnt = yAxis.insert(0, axisPanel.create((rowCnt + '-pnl'), (rowCnt + '-btn'), seriesCount));

        if (yAxis.items.items.length == 2) {
            /* change the tool-tip of the first item as well */
            if (yAxis.items.items[0].el) {
                var firstItem = yAxis.items.items[0].el.dom;

                $(firstItem).find('.yaxis1').tooltipster('content', CC.tips.df134);
                $(firstItem).find('.yaxis2').tooltipster('content', CC.tips.df135);
                $(firstItem).find('.yaxis3').tooltipster('content', CC.tips.df136);
            }
        }

        //CC.util.generateTip('top');

        //track
        CC.track.prototype.productAction('add', seriesCount, yAxis.items.items.length - 1);

        if (selVal) {
            newCmpnt.items.items[0].setValue(selVal);
        }

        yAxis.doLayout();

        //Enabling delete Y axis option
        if (yAxis.items.items.length == 2) {
            //loop through both the items to enable the delete button
            for (var i = 0 ; i < yAxis.items.items.length ; i++)
                if (yAxis.items.items[i].items.items[3].el)
                    yAxis.items.items[i].items.items[3].el.dom.firstChild.firstChild.lastChild.disabled = false;
        }
        else if (yAxis.items.items.length > 2) {
            if (yAxis.items.items[yAxis.items.items.length - 1].items.items[3].el) {
                //enable the delete button of the last child only as others must already be enabled
                yAxis.items.items[yAxis.items.items.length - 1].items.items[3].el.dom.firstChild.firstChild.lastChild.disabled = false;
            }
        }

        var item = yAxis.items.items
        //var l = item.length;
        //item[l - 1].items.items[0].setValue(l);

        //var searchValueInCombo = function (v) {
        //    var ret = false;
        //    if (v === Ext.getCmp('fldXAxs').getValue())
        //        ret = true;
        //    else {
        //        for (var k = 0 ; k < l ; k++) {
        //            if (item[k].items.items[0].getValue() === v)
        //                ret = true;
        //        }
        //        return ret;
        //    }
        //}

        //if (searchValueInCombo(l)) {
        //    for (i = 0; i <= l; i++) {
        //        if (searchValueInCombo(i) == false) {
        //            item[l - 1].items.items[0].setValue(CC.Stores.ViewFields[i].InternalName);
        //            break;
        //        }
        //    }
        //}

        ////Auto Scroll Down
        //if ($('.x-box-inner').length>0) {
        //	lastElementTop = $('.x-box-inner').children().position().top ;
        //	scrollAmount = $('.x-box-inner').height() - lastElementTop;
        //	//Console.log(lastElementTop);
        //	//Console.log(scrollAmount);
        //	$('.x-box-inner').animate({scrollTop: scrollAmount},1000);
        //}
        //Ext.getCmp('cntrParent').doLayout();
    },

    //genarets a multiSeries/singleSeries chart
    alterChartSeries: function (otherAxis, otherAxisIndx) {

        /* check if the selected value isn't secondary because then the value will always be true */
        //var isSecondaryExist = otherAxis && otherAxis === 'P';
        //var isPrimaryExist = otherAxis && otherAxis === 'S';

        var axisType = {
            isSecondaryExist: otherAxis && otherAxis === 'P',
            isPrimaryExist: otherAxis && otherAxis === 'S'
        }

        /* gets the corresponding chart name of for the combination charts */
        var combinationChartName = function (combChartName) {
            /* default val */
            var chartName = CC.util.defaultSingleSeriesChart();
            $.each(CC.med.cmChart().getAxisChart(1), function (index, val) {
                if (combChartName === val[3]) {
                    chartName = CC.med.cmChart().getFullChartName(val[3]);
                    return false;
                }
            });
            return chartName;
        }

        /* to be used to retain the current single series chart for the combination chart setting, if applicable */
        var singleSeriesChart = '';

        //var _chartConfig = CC.med.chartConfig().Configuration;
        var _chartConfig = CC.med.getSetting('config');
        var chartType = CC.util.getChartSeries(_chartConfig.chartType);
        //var yAxis = Ext.getCmp('cntrYAxis');

        if (_chartConfig.series.length > 1 && chartType == 0) {
            //if the existing preview is of single series and the new one is multi series
            //if (CC.med.charts().Cache.MultiChartType) {

            /*Multi Series */
            singleSeriesChart = CC.med.getSetting('cType');
            if (CC.med.getSetting('cacheMulti')) {
                //if a cached value exist of multi series
                if (CC.util.isDualYAxis(CC.med.getSetting('cacheMulti'))) {
                    /* if the cached value is of dual Y axis, reset it to single Y axis, default behaviour */
                    CC.med.updateSetting('cacheMulti', CC.util.defaultMultiSeriesChart());
                }
                CC.med.updateSetting('cType', CC.med.getSetting('cacheMulti'));
            }
            else {
                //default multi series
                CC.med.updateSetting('cType', CC.util.defaultMultiSeriesChart());
                CC.med.updateSetting('cacheMulti', CC.util.defaultMultiSeriesChart());
            }
        }
        else if (_chartConfig.series.length == 1 && chartType > 0) {
            /*Single Series if the existing preview is of multi series and the new one is single series
              Alter only when the existing multi-series is a combination series otherwise,
              retain the multi-series with only 1 series.*/
            if (CC.util.isCombinationChart()) {
                var firstSeries = CC.med.getSetting('series')[0];
                if (firstSeries.renderAs) {
                    /* apply the combination chart setting to the new single series */
                    var chartName = combinationChartName(firstSeries.renderAs);
                    CC.med.updateSetting('cType', chartName);
                    CC.med.updateSetting('cacheSingl', chartName);
                }
                else if (CC.med.getSetting('cacheSingl')) {
                    //if a cached value exist of single series
                    CC.med.updateSetting('cType', CC.med.getSetting('cacheSingl'));
                }
                else {
                    //default single series
                    CC.med.updateSetting('cType', CC.util.defaultSingleSeriesChart());
                    CC.med.updateSetting('cacheSingl', CC.util.defaultSingleSeriesChart());
                }
            }
        }

        var yAxis = Ext.getCmp('cntrYAxis');
        //if (CC.util.getChartSeries(_chartConfig.ChartType) == 3) {
        if (CC.util.isCombinationChart(CC.util.getChartSeries(_chartConfig.chartType))) {
            //Combination Chart

            var axisSetting = [];
            for (var i = 0; i < _chartConfig.series.length; i++) {

                //#region Combination Chart Config
                if (!_chartConfig.series[i].renderAs) {
                    var j = i >= 3 ? 0 : i;

                    if (!_chartConfig.series[i].renderAs) {
                        var _renderAs;
                        if (singleSeriesChart && CC.med.cmChart().getFullComboName(singleSeriesChart)) {
                            /* apply the last selected chart for the combination chart */
                            _renderAs = CC.med.cmChart().getFullComboName(singleSeriesChart);
                            singleSeriesChart = '';
                        }
                        _chartConfig.series[i].renderAs = _renderAs ? _renderAs : CC.med.cmChart().getAxisChart(1)[j][3];
                        _chartConfig.series[i].renderAs = _renderAs ? _renderAs : CC.med.cmChart().getAxisChart(1)[j][3];
                    }
                    else {
                        _chartConfig.series[i].renderAs = _chartConfig.series[i].renderAs;
                        j = CC.util.jsonItemIndex(CC.med.cmChart().getAxisChart(1), _chartConfig.series[i].renderAs, 3);
                    }


                    //this.applyComboChartSetting(i, 1, _chartConfig.series[i].renderAs);
                    //if (CC.med.getSetting('cType') == 'mscombidy2d') {
                    if (CC.util.isDualYAxis()) {
                        /* Dual Y Axis*/
                        //this.applyComboChartSetting(i, 0, CC.med.getSetting('series')[i].parentYAxis);
                        this.applyComboChartSetting(i, 0, _chartConfig.series[i].parentYAxis);

                        //Ensure if the axis setting is properly applied to the corresponding series
                        axisSetting.push(_chartConfig.series[i].parentYAxis);
                    }

                    CC.med.setUpdateFlag();
                }

                this.applyComboChartSetting(i, 1, _chartConfig.series[i].renderAs);

                /* since renderAs should always be present at this level moving it out of the if block */
                if (i == 1) {
                    //if index is 1 set the combination setting for current as well as 0th item.
                    this.applyComboChartSetting(0, 1, _chartConfig.series[0].renderAs);
                    //if (CC.med.getSetting('cType') == 'mscombidy2d') {
                    if (CC.util.isDualYAxis()) {
                        /* Dual Y Axis*/
                        this.applyComboChartSetting(0, 0, _chartConfig.series[0].parentYAxis);

                        //Ensure if the axis setting is properly applied to the corresponding series
                        axisSetting.push(_chartConfig.series[0].parentYAxis);
                    }
                }

                //#endregion

                //#region Combination Chart Axis Config
                //if (isUpdtAxSt) {

                if (otherAxis) {
                    this.updateCombinationAxis(otherAxis, otherAxisIndx, i, _chartConfig.series, axisType);
                }

                axisSetting.push(_chartConfig.series[i].parentYAxis);
                //#endregion
            }

            /* Axis setting based on the following conditions
             * if the user has updated the axis through button click
             * OR
             * For dual y axis charts, the series setting does not have the definition of either P or S
             */
            ////if ((!isUpdtAxSt && otherAxis) ||
            ////if ((!isUpdtAxSt) ||
            if (!isNaN(otherAxisIndx)) {
                //if ((CC.util.isDualYAxis() &&
                //    (!axisSetting.includes('P') ||
                //    !axisSetting.includes('S')))) {
                //if (!isSecondaryExist && otherAxis != 'P') {
                if (!axisType.isPrimaryExist) {

                    //if (!otherAxis && axisSetting.length > 0) {
                    //    otherAxis = !axisSetting.includes('P') ? 'P' : !axisSetting.includes('S') ? 'S' : '';
                    //}
                    otherAxis = 'P';

                    if (otherAxis) {
                        var anotherAxisIndex = otherAxisIndx - 1 >= 0 ? otherAxisIndx - 1 : otherAxisIndx + 1;
                        _chartConfig.series[anotherAxisIndex].parentYAxis = otherAxis;

                        //apply the selected css
                        this.applyComboChartSetting(anotherAxisIndex, 0, _chartConfig.series[anotherAxisIndex].parentYAxis);
                    }
                }
            }
        }
        else {
            //re-Set the Axis and Chart buttons for Data Fields
            this.resetComboSettings();
        }

        return axisType;
    },

    /* Sets the selected image position for the Y container items*/
    applyComboChartSetting: function (itemIndex, type, renderAs) {
        /* Check if the container has items or not
         * AND
         * Also check if the setting is avilable for that item or not. For ex, add series button will also trigger this but without applying any setting */

        if (Ext.getCmp('cntrYAxis').items && renderAs) {
            $(Ext.getCmp('cntrYAxis').items.itemAt(itemIndex).el.dom)
                        .find(type == 1 ? ".yaxis2 span" : ".yaxis1 span")
                        .css({ "background-position": this.getComboChartConfig(type, renderAs)[0][2] + "px -143px" });
        }
    },

    clearAllComboErrorCls: function () {
        var xAxis = Ext.getCmp('fldXAxs');

        /* check if anything has been rendered or not */
        if (xAxis.el) {
            if (xAxis.getValue()) {
                CC.util.removeCssClass(xAxis.el.dom, [CC.util.getErrorCls()]);
            }

            var yAxis = Ext.getCmp('cntrYAxis');
            $.each(yAxis.items.items, function (index, value) {
                if (value.items.items[0].getValue()) {
                    if (value.items.items[0].el) {
                        CC.util.removeCssClass(value.items.items[0].el.dom, [CC.util.getErrorCls()]);
                    }
                }
            });
        }
    },

    /* Removes a series */
    deleteYAxis: function (chartConfig) {

        CC.util.closePopup('fast');

        var yAxis = Ext.getCmp('cntrYAxis');
        if (yAxis.items.items.length > 1) {

            CC.util.toggleProgressBar(true);

            var currRow = this.getCurrentRowIndex('yaxis3');
            var senderId = yAxis.items.items[currRow].id;

            var chartSeries = CC.med.getSetting('series');

            var isDeletedAxisHasSetting = chartSeries[currRow];
            var isSecondaryAxis = isDeletedAxisHasSetting ? chartSeries[currRow].parentYAxis == 'S' : false;

            if (isDeletedAxisHasSetting) {
                //only yAxis 2 has the def of the series index
                CC.track.prototype.productAction('delete', $(Ext.getCmp('cntrYAxis').items.items[currRow].el.dom).find('.yaxis2').data('cc-series'), chartSeries[currRow]);
            }
            else {
                CC.track.prototype.productAction('delete', $(Ext.getCmp('cntrYAxis').items.items[currRow].el.dom).find('.yaxis2').data('cc-series'), -1);
            }

            var fieldVal = yAxis.items.items[CC.util.getContainerItemIndex('cntrYAxis', CC.util.getContainerItem('cntrYAxis', CC.util.yPanelId(senderId)))].items.items[0].getValue();
            yAxis.remove(CC.util.yPanelId(senderId));

            this.removeSpField(fieldVal);

            if (CC.util.isDualYAxis() && chartSeries.length > 1) {
                //var chartSeries = CC.med.getSetting('series');
                var selAxis = chartSeries[0].parentYAxis;
                var otherAxis = selAxis == 'P' ? 'S' : 'P';
                var axisType = this.alterChartSeries(otherAxis, 0);

                if (!axisType.isSecondaryExist) {
                    this.resetAxisSetting(chartSeries);
                }
            }
            else {
                this.alterChartSeries();

                if (chartSeries.length == 1) {
                    /* reset the combination chart setting */
                    chartSeries[0].parentYAxis = '';
                    chartSeries[0].renderAs = '';
                }
            }

            yAxis.doLayout();

            if (yAxis.items.items.length <= 1) {
                /* change the tool-tip of the first item to no delete */
                var firstItem = yAxis.items.items[0].el.dom;

                $(firstItem).find('.yaxis1').tooltipster('content', CC.tips.df131);
                $(firstItem).find('.yaxis2').tooltipster('content', CC.tips.df132);
                $(firstItem).find('.yaxis3').tooltipster('content', CC.tips.df133);
            }


            //Preventing single Y axis deletion
            if (yAxis.items.items.length == 1) {

                /* Disable the delete button*/
                yAxis.items.items[0].items.items[3].el.dom.firstChild.firstChild.lastChild.disabled = true;

                /* Remove combo chart type setting*/
                this.removeSelectedStyles(0, 2);

                /* Remove combo chart axis setting*/
                this.removeSelectedStyles(0, 1);
            }

            CC.med.createChart();
        }
    },

    resetAxisSetting: function (chartSeries) {
        /* For single Y Axis reset the axis setting */
        var currentScope = this;
        var axisPanel = $('#axisView .chrtAxTemplate ');

        $.each(chartSeries, function (index) {
            currentScope.removeSelectedStyles(index, 1);
            CC.util.removeCssClass(axisPanel[index], ['x-view-selected']);
            chartSeries[index].parentYAxis = '';
        });

        /* set the update flag for the timer to incorporate the latest change */
        CC.med.setUpdateFlag();
    },

    getComboChartConfig: function (type, internalName) {
        //var chart = CC.med.cmChart().getAxisChart(type);
        return jQuery.grep(CC.med.cmChart().getAxisChart(type), function (n) {
            return (n[3] == internalName);
        });
    },

    /* Get's the series index for which the callout is triggerred */
    getCurrentRowIndex: function (btnCls) {
        //get the top position of the Y-axis container
        var yTop = Math.round($('#cntrYAxis')[0].getBoundingClientRect().top);

        //get the rendered top position of the callout div. It will also get the surplus count like, padding, margin.
        var renderedCaloutTop = Math.round($('#pnlCallout')[0].getBoundingClientRect().top);

        //var pnlNo = util.getPanelNumber($('#cntrYAxis')[0].children[0].id);
        //var pnlPad = parseFloat($('#cntrYAxis')[0].children[0].style.paddingTop);
        //var btnHt = $("." + btnCls)[0].offsetHeight;
        //var caloutMrg = parseFloat($('#pnlCallout').css("marginTop"));

        //get only the surplus count of the callout div. margin, padding, & height of the triggered button
        var surplusCaloutTop = parseFloat($('#cntrYAxis')[0].children[0].style.paddingTop) + $("." + btnCls)[0].offsetHeight + parseFloat($('#pnlCallout').css("marginTop"));

        //get the row index of the item of the Y-axis container to which the callout div is pointing
        //var rowIndx = (caloutTop - (pnlPad + btnHt + caloutMrg) - yTop)/parseFloat($('#cntrYAxis')[0].children[0].offsetHeight);
        return (renderedCaloutTop - surplusCaloutTop - yTop) / parseFloat($('#cntrYAxis')[0].children[0].offsetHeight);
    },

    insertListField: function (jsonData, initialize) {
        if (initialize) {
            CC.med.updateSetting('dsFld', jsonData);
        }
        else {
            CC.med.getSetting('dsFld').push(jsonData);
        }

        //cache the new value
        CC.med.updateSetting('cacheFld', CC.util.valueCopyJSON(CC.med.getSetting('dsFld')));
    },

    loadComboChartSetting: function (_thisId) {
        if (CC.util.isCombinationChart()) {
            //auto selection of combo setting
            var itemIndex = -1;
            var yAxis = Ext.getCmp('cntrYAxis');
            $.each(yAxis.items.items, function (index, value) {
                if (value.id == _thisId)
                    itemIndex = index;
            });



            if (itemIndex > -1 && itemIndex < CC.med.getSetting('series').length) {
                var chartSeries = CC.med.getSetting('series');
                this.applyComboChartSetting(itemIndex, 1, CC.med.getSetting('series')[itemIndex].renderAs);
                if (CC.util.isDualYAxis(CC.med.getSetting('cType'))) {
                    /* Dual Y Axis*/
                    if (CC.med.getSetting('series')[itemIndex].parentYAxis) {
                        this.applyComboChartSetting(itemIndex, 0, CC.med.getSetting('series')[itemIndex].parentYAxis);
                    }
                    else {
                        //default behaviour
                        this.applyComboChartSetting(itemIndex, 0, 'P');
                    }
                }
            }
        }

        CC.util.generateTip('top');
    },

    // itemIndex --> is the index of the Series
    // itemPosition --. For the given series,  the position of the control. Axis or Charts
    removeSelectedStyles: function (itemIndex, itemPosition) {
        /* check if the control is rendered or not */
        if (Ext.getCmp('cntrYAxis').items.items[itemIndex].items.items[itemPosition].el) {
            //$(yAxis.items.items[0].items.items[2].el.dom).find('span').css('background-position', '');
            $(Ext.getCmp('cntrYAxis').items.items[itemIndex].items.items[itemPosition].el.dom).find('span').css('background-position', '');
        }
    },

    removeSpField: function (fieldVal) {
        /* Check if delete is triggerred on an exisiting setting or an empty record */
        if (fieldVal) {
            var spliced = undefined;
            $.each(CC.med.getSetting('series'), function (index, value) {
                if (value.column == fieldVal) {
                    spliced = CC.med.getSetting('series').splice(index, 1);
                    return false;
                }
            });



            if (CC.util.isDualYAxis(CC.med.getSetting('cType')) && spliced[0].parentYAxis) {
                var newAxisIndex = -1;
                /* If the spliced field has dual-Y-Axis setting. assign it to other field*/
                $.each(CC.med.getSetting('series'), function (index, value) {
                    if (!value.parentYAxis) {
                        value.parentYAxis = spliced[0].parentYAxis;
                        newAxisIndex = index;
                        return false;
                    }
                });
                if (newAxisIndex > -1) {
                    this.applyComboChartSetting(newAxisIndex, 0, spliced[0].parentYAxis);
                }
            }

            /* pop is only removing the last item hence used remove*/
            CC.med.getSetting('dsFld').remove(fieldVal);
        }
    },

    //resets the combo chart configuration from the Y Axis buttons -------------------- TBD
    resetComboSettings: function () {
        //if (Ext.getCmp('cntrYAxis').items) {
        //    $.each(Ext.getCmp('cntrYAxis').items.items, function (index, value) {
        //        //CC.util.removeCssClass($(value.items.items[2].el.dom).find('input')[0], CC.CM.Chart.getSelComboCls());
        //        //CC.util.addCssClass($(value.items.items[2].el.dom).find('input')[0], CC.CM.Chart.getAxisChart(1)[0][4]);
        //    });
        //}
    },

    resetSeries: function () {
        var res = false;
        var chartSeries = CC.med.getSetting('series');

        if (chartSeries.length == 0) {
            var yAxis = Ext.getCmp('cntrYAxis');
            if (yAxis.items.items.length == 1) {

                var yAxisCombo = Ext.getCmp($('#' + yAxis.items.items[0].id).find('input').attr('id'));
                var selectedStore = yAxisCombo.getStore().data.items[yAxisCombo.selectedIndex];


            }
        }

    },

    selectXAxis: function (r, chartConfig) {
        var yAxis = Ext.getCmp('cntrYAxis');

        CC.med.updateSetting('xAxis', r.data.InternalName, "column");
        CC.med.updateSetting('cacheXAxis', r.data.InternalName, "column");

        if (CC.med.getSetting('dsFld').indexOf(r.data.InternalName) == -1) {
            this.insertListField(r.data.InternalName);
        }

        if (yAxis.items.items.length > 0 && CC.med.getSetting('series').length > 0) {
            CC.med.createChart();
        }

        this.clearAllComboErrorCls();
    },

    selectYAxis: function (c, r, chartConfig) {

        CC.util.toggleProgressBar(true);

        //chart refresh logic
        var yAxis = Ext.getCmp('cntrYAxis');

        var itemIndex = CC.util.getContainerItemIndex('cntrYAxis', CC.util.getContainerItem('cntrYAxis', CC.util.yPanelId(c.id)));

        var chartSeries = CC.med.getSetting('series');
        var cacheSeries = CC.med.getSetting('cacheSeries');
        var seriesIndex = CC.util.jsonItemIndex(chartSeries, r.data.InternalName, "column");

        if (seriesIndex > -1) {
            //if (seriesIndex != itemIndex && itemIndex < CC.med.chartConfig().Series.length) {
            if (seriesIndex != itemIndex) {
                if (itemIndex < chartSeries.length) {
                    //Current value is > -1 and is swapped with an existing value

                    var res = CC.util.swapItem(chartSeries[seriesIndex].column, chartSeries[itemIndex].column);
                    chartSeries[seriesIndex].column = res[0];
                    chartSeries[itemIndex].column = res[1];
                    res = CC.util.swapItem(chartSeries[seriesIndex].seriesName, chartSeries[itemIndex].seriesName);
                    chartSeries[seriesIndex].seriesName = res[0];
                    chartSeries[itemIndex].seriesName = res[1];

                    res = CC.util.swapItem(cacheSeries[seriesIndex].column, cacheSeries[itemIndex].column);
                    cacheSeries[seriesIndex].column = res[0];
                    cacheSeries[itemIndex].column = res[1];
                    res = CC.util.swapItem(cacheSeries[seriesIndex].seriesName, cacheSeries[itemIndex].seriesName);
                    cacheSeries[seriesIndex].seriesName = res[0];
                    cacheSeries[itemIndex].seriesName = res[1];

                    //change the selected value of the swapped combo as well
                    yAxis.items.items[seriesIndex].items.items[0].setValue(chartSeries[seriesIndex].column);
                }
                else {
                    //current value is -1 and an existing value is selected
                    //Add the css x-form-invalid

                    CC.util.addCssClass(Ext.getCmp('cntrYAxis').items.items[seriesIndex].items.items[0].el.dom, CC.util.getErrorCls());
                }
            }
        }
        else {
            if (chartSeries.length > itemIndex) {
                //item already exist
                //update an exisiting item

                chartSeries[itemIndex].column = r.data.InternalName;
                chartSeries[itemIndex].seriesName = r.data.Title;

                cacheSeries[itemIndex].column = r.data.InternalName;
                cacheSeries[itemIndex].seriesName = r.data.Title;
            }
            else {
                //add a new item

                chartSeries.push({
                    "column": r.data.InternalName, "seriesName": r.data.Title
                });

                CC.util.insertUnique(cacheSeries, r.data.InternalName, "column", {
                    "column": r.data.InternalName, "seriesName": r.data.Title
                });
                //CC.med.charts().Cache.Configuration.Series.push({
                //    "Column": r.data.InternalName, "seriesName": r.data.Title
                //});
            }
        }

        this.alterChartSeries();

        if (CC.util.isDualYAxis()) {
            /* for combination chart add the new field with default P value */
            this.applyComboChartSetting(itemIndex, 0, 'P');
        }

        if (CC.med.getSetting('dsFld').indexOf(r.data.InternalName) == -1) {
            this.insertListField(r.data.InternalName);
        }

        CC.med.createChart();

        this.clearAllComboErrorCls();

        CC.track.prototype.productAction('update', $(Ext.getCmp('cntrYAxis').items.items[itemIndex].el.dom).find('.yaxis2').data('cc-series'), itemIndex);
    },

    /* Changes the callout panel's selected item's image position */
    swapBtnImages: function (btnCls, position) {
        var rowIndx = this.getCurrentRowIndex(btnCls);

        //set the image background-position of the triggered button
        $(Ext.getCmp('cntrYAxis').items.itemAt(rowIndx).el.dom).find("." + btnCls).find('span').css({ "background-position": position + "px -143px" });

        return rowIndx;
    },

    swapValues: function (record, c, excludeCurrent) {
        var isXAxis = false;

        if (c.id != 'fldXAxs') {
            /* for X axis swapping change, clear the series and change the xAxis value */
            if (record.data.InternalName === Ext.getCmp('fldXAxs').getValue()) {
                if (c.getValue()) {
                    /* swap Y Axis with X Axis only if Y Axis has value */
                    Ext.getCmp('fldXAxs').setValue(c.getValue());
                    CC.med.getSetting('xAxis').column = c.getValue();
                    this.removeSpField(c.getValue());
                }
                else {
                    CC.util.addCssClass(Ext.getCmp('fldXAxs').el.dom, [CC.util.getErrorCls()]);
                    c.cancelSelection = true;
                }
            }
        }
        else {
            isXAxis = true;
        }

        /* check if the selection is not invalidated already with X Axis */
        if (!c.cancelSelection) {
            var xAxisValue = Ext.getCmp('fldXAxs').getValue();

            var isValueSwapped = false;
            var items = Ext.getCmp('cntrYAxis').items.items;
            var len = items.length;
            for (var k = 0 ; k < len ; k++) {
                excludeCurrent = excludeCurrent ? c != items[k].items.items[0] : true;

                var curItemVal = items[k].items.items[0].getValue();

                if (!isXAxis && record.data.InternalName === curItemVal && excludeCurrent) {
                    if (c.getValue()) {
                        //check if the swapping combo has some value to swap or not
                        items[k].items.items[0].setValue(c.getValue());
                    }
                    else {
                        /* swap has been triggerred for a blank value, so cancel it */
                        /* add the error class to the matching combo as well */
                        CC.util.addCssClass(items[k].items.items[0].el.dom, [CC.util.getErrorCls()]);

                        c.cancelSelection = true;
                    }
                    isValueSwapped = true;
                }
                else if (isXAxis && curItemVal === record.data.InternalName) {
                    /* Swapping needs to be done for xAxis */
                    items[k].items.items[0].setValue(c.getValue());
                    this.removeSpField(curItemVal);

                    var chartSeries = CC.med.getSetting('series');
                    var cacheSeries = CC.med.getSetting('cacheSeries');

                    var selectedRecord = c.findRecord(c.valueField || c.displayField, c.getValue())

                    chartSeries.push({
                        "column": selectedRecord.data.InternalName, "seriesName": selectedRecord.data.Title
                    });

                    CC.util.insertUnique(cacheSeries, selectedRecord.data.InternalName, "column", {
                        "column": selectedRecord.data.InternalName, "seriesName": selectedRecord.data.Title
                    });

                    //CC.med.getSetting('dsFld').push(selectedRecord.data.InternalName);
                    if (CC.med.getSetting('dsFld').indexOf(selectedRecord.data.InternalName) == -1) {
                        this.insertListField(selectedRecord.data.InternalName);
                    }

                    isValueSwapped = true;
                }
            }

            if (!isValueSwapped) {
                /* if no swapping is done, i.e., the user selected a new field, exclude the old field */
                CC.med.getSetting('dsFld').remove(c.getValue());
            }
        }
    },

    /* Dual Y Axis chart */
    switchAxisView: function (selRec) {
        if (Ext.getCmp('cntrYAxis').items.items.length > 1) {

            var chartCosmetics = CC.med.getSetting('chart');
            var chartSeries = CC.med.getSetting('series');

            //clear the exisiting setting
            if (chartCosmetics.yAxisName) {
                chartCosmetics.pYAxisName = chartCosmetics.yAxisName;
            }
            chartCosmetics.yAxisName = '';

            //this.swapBtnImages("yaxis1", this.axisView.getSelectedRecords()[0].data.pos);
            var rowIndx = this.swapBtnImages("yaxis1", selRec[0].data.pos);


            //check the Axis type, P or S.
            var selAxis = selRec[0].data.internalName;

            if (rowIndx < chartSeries.length) {
                chartSeries[rowIndx].parentYAxis = selAxis;

                var otherAxis = selAxis == 'P' ? 'S' : 'P';
                var axisType = this.alterChartSeries(otherAxis, rowIndx);

                CC.med.updateSetting('cType', axisType.isSecondaryExist ? 'mscombidy2d' : 'mscombi2d');
                CC.med.updateSetting('cacheMulti', axisType.isSecondaryExist ? 'mscombidy2d' : 'mscombi2d');

                if (!axisType.isSecondaryExist) {
                    this.resetAxisSetting(chartSeries);
                }

                CC.med.createChart();
            }

            CC.util.closePopup(2000);

            CC.track.prototype.productAction('update', $(Ext.getCmp('cntrYAxis').items.items[rowIndx].el.dom).find('.yaxis2').data('cc-series'), rowIndx);
        }
        else {
            CC.util.closePopup();
        }
    },

    /* Combination chart config  */
    switchChartView: function (chartConfig) {
        if (Ext.getCmp('cntrYAxis').items.items.length > 1) {
            var _dataFields = CC.Wizard.tabpanel.tabs[0].steps[1];
            var selRec = _dataFields.chartView.getSelectedRecords()[0].data;
            var rowIndx = this.swapBtnImages("yaxis2", _dataFields.chartView.getSelectedRecords()[0].data.pos);

            var chartSeries = CC.med.getSetting('series');
            if (rowIndx < chartSeries.length) {
                //do modification only if there's a selected value in the corresponding Select Field combo

                chartSeries[rowIndx].renderAs = selRec.internalName;

                /* if the existing chart is not a combination chart, force the setting */
                if (!CC.util.isCombinationChart()) {
                    /* check if cache has saved setting on the type of combination chart */

                    if (!CC.med.getSetting('cacheMulti') || !CC.util.isCombinationChart(CC.util.getChartSeries(CC.med.getSetting('cacheMulti').chartType))) {
                        //get the default Combination Chart

                        CC.med.updateSetting('cType', CC.util.getChartSeries(CC.med.getSetting('cacheConfig').chartType) != 3 ? CC.util.defaultMultiSeriesChart() : CC.med.getSetting('cacheConfig').chartType);
                        CC.med.updateSetting('cacheMulti', CC.med.getSetting('cType'));
                    }
                    else {
                        //get the cached Combination Chart
                        CC.med.updateSetting('cType', CC.med.getSetting('cacheMulti'));
                    }

                    this.alterChartSeries();
                }
                CC.med.createChart();
            }

            CC.util.closePopup(2000);

            CC.track.prototype.productAction('update', $(Ext.getCmp('cntrYAxis').items.items[rowIndx].el.dom).find('.yaxis2').data('cc-series'), rowIndx);
        }
        else {
            CC.util.closePopup();
        }
    },

    /* Displays the single calloout panel at the proper position. */
    toggleCalloutPnl: function (event, sender) {
        if (Ext.getCmp('cntrYAxis').items.items.length > 1) {
            /* position of the button which triggered this event */
            var rect = sender.getBoundingClientRect();
            var pnl = $("#pnlCallout");

            /* check which out of the three buttons triggered it. */
            var btnCount = sender.id.substring(sender.id.length - 1) * 1

            /* gets the series index of the clicked item by generating the unique parent panel id using the clicked button's id. */
            var currRowIndex = CC.util.jsonItemIndex(Ext.getCmp('cntrYAxis').items.items, sender.id.split('-')[0] + '-pnl', 'id');
            var chartType = -1;

            //pnl.css('left', ((rect.left - 20) + "px")) //.left = rect.left - 20;
            //pnl.css('top',  (rect.bottom + "px"));
            //pnl.css('display', 'block');

            // var _left = ((rect.left - 20) + "px");
            // var _top = (rect.bottom + "px");

            //Takes care of the scroll bar position as well, in both, IE and non-IE browsers
            var _top = (document.documentElement && document.documentElement.scrollTop) ||
                        document.body.scrollTop;
            var _left = (document.documentElement && document.documentElement.scrollLeft) ||
                        document.body.scrollLeft;

            // pnl.animate({left: ((rect.left - 20) + "px")}, 400, function(){
            // pnl.animate({top: (rect.bottom + "px")}, 400);
            // });

            //20 is deducted because the arrow of the panel starts at 20px
            pnl.animate({
                left: ((_left + (rect.left - 20)) + "px"),
                top: ((_top + rect.bottom) + "px")
            }, 400);

            switch (btnCount) {
                case 1:
                    Ext.getCmp('axisView').show();
                    Ext.getCmp('chartView').hide();
                    Ext.getCmp('pnlConfirmation').hide();

                    chartType = 0;
                    break;
                case 2:
                    Ext.getCmp('axisView').hide();
                    Ext.getCmp('chartView').show();
                    Ext.getCmp('pnlConfirmation').hide();

                    chartType = 1;
                    break;
                case 3:
                    Ext.getCmp('axisView').hide();
                    Ext.getCmp('chartView').hide();
                    Ext.getCmp('pnlConfirmation').show();
                    //deleteRow(sender);
                    break;
            }

            if (currRowIndex > -1) {
                /* There are exclusive combo charts settings to be applied to the callout */
                var curChartSeries = CC.med.getSetting('series')[currRowIndex];
                if (chartType == 1) {
                    var selectIndex = -1;
                    if (curChartSeries.renderAs && CC.util.isCombinationChart()) {
                        /* gets the index of the selected callout panel
                         * first id, 0 stores the index value */
                        selectIndex = this.getComboChartConfig(chartType, curChartSeries.renderAs)[0][0];
                    }

                    if (selectIndex > -1) {
                        /* trigger selection only when there exist a corresponding value in the series */
                        Ext.getCmp('chartView').select(selectIndex, false, true);
                    }
                    else {
                        /* clear any selection from any other series */
                        Ext.getCmp('chartView').clearSelections(true);
                    }

                    var chartTableRows = $(Ext.getCmp('chartView').el.dom).find(Ext.getCmp('chartView').itemSelector);
                    for (var i = 0; i < 3; i++) {
                        /* set the selected for chart types and clear the others. chart types has 3 option */
                        if (i == selectIndex) {
                            /* selected style */
                            //chartTableRows[i].children[0].firstChild.firstChild.style["background-position-y"] = "-143px";
                            //chartTableRows[i].children[1].firstChild.style["color"] = "rgb(73, 101, 123)";
                            chartTableRows[i].children[0].firstChild.firstChild.style["background-position-y"] = "-134px";
                        }
                        else {
                            /* clear style */
                            //chartTableRows[i].children[0].firstChild.firstChild.style["background-position-y"] = "-134px";
                            //chartTableRows[i].children[1].firstChild.style["color"] = "rgb(170, 170, 170)";
                            chartTableRows[i].children[0].firstChild.firstChild.style["background-position-y"] = "-143px";
                        }
                    }
                }
                else if (chartType == 0) {
                    /* gets the index of the selected callout panel
                     * first id, 0 stores the index value */
                    var selectIndex = -1;
                    if (curChartSeries.parentYAxis && CC.util.isDualYAxis()) {
                        /* set the selected index for Dual Y Axis only. otherwise -1 will reset the callout setting. */
                        selectIndex = this.getComboChartConfig(chartType, curChartSeries.parentYAxis)[0][0];
                    }

                    if (selectIndex > -1) {
                        /* trigger selection only when there exist a corresponding value in the series */
                        Ext.getCmp('axisView').select(this.getComboChartConfig(chartType, curChartSeries.parentYAxis)[0][0], false, true);
                    }
                    else {
                        /* clear any selection from any other series */
                        Ext.getCmp('axisView').clearSelections(true);
                    }

                    var axisTableRows = $(Ext.getCmp('axisView').el.dom).find(Ext.getCmp('axisView').itemSelector);
                    for (var i = 0; i < 2; i++) {
                        /* set the selected for axis types and clear the others. axis types has 2 option */
                        if (i == selectIndex) {
                            /* selected style */
                            //axisTableRows[i].children[0].firstChild.firstChild.style["background-position-y"] = "-143px";
                            //axisTableRows[i].children[1].firstChild.style["color"] = "rgb(73, 101, 123)";
                            axisTableRows[i].children[0].firstChild.firstChild.style["background-position-y"] = "-134px";
                            axisTableRows[i].children[1].firstChild.style["color"] = "rgb(170, 170, 170)";
                        }
                        else {
                            /* clear style */
                            //axisTableRows[i].children[0].firstChild.firstChild.style["background-position-y"] = "-134px";
                            //axisTableRows[i].children[1].firstChild.style["color"] = "rgb(170, 170, 170)";
                            axisTableRows[i].children[0].firstChild.firstChild.style["background-position-y"] = "-143px";
                            axisTableRows[i].children[1].firstChild.style["color"] = "rgb(73, 101, 123)";

                        }
                    }
                }
            }

            pnl.fadeIn(50);
        }

        CC.util.stopPropagation(event);

        //get the index of the item of the Y container for future use
        ////Console.log("Index of the clicked item " + util.findRowIndexofY(sender.id));
        ////Console.log("Index of the clicked item " + CC.util.getContainerItemIndex('cntrYAxis', CC.util.getContainerItem('cntrYAxis', CC.util.yPanelId(sender.id))));
    },

    updateCombinationAxis: function (otherAxis, otherAxisIndx, curSeriesIndx, chartSeries, axisType) {
        if (otherAxis) {
            if (curSeriesIndx != otherAxisIndx) {
                //if (chartSeries[curSeriesIndx].parentYAxis) {
                //if (chartSeries[curSeriesIndx].parentYAxis == selAxis) {
                //if (chartSeries[curSeriesIndx].parentYAxis == 'S' && otherAxis == 'S') {
                if (chartSeries[curSeriesIndx].parentYAxis === 'S') {
                    axisType.isSecondaryExist = true;
                }
                else if (chartSeries[curSeriesIndx].parentYAxis === 'P') {
                    axisType.isPrimaryExist = true;
                }
                else if (!axisType.isSecondaryExist && !chartSeries[curSeriesIndx].parentYAxis) {
                    /* if no secondary Y axis exists, add a default one & update the flag */
                    chartSeries[curSeriesIndx].parentYAxis = 'S';
                    axisType.isSecondaryExist = true;
                    this.applyComboChartSetting(curSeriesIndx, 0, 'S');
                }
                else if (!chartSeries[curSeriesIndx].parentYAxis) {
                    /* remove the exisiting setting of the axis as a new setting has been applied */
                    chartSeries[curSeriesIndx].parentYAxis = 'P';

                    axisType.isPrimaryExist = true;

                    /* also re-set the style of the Axis */
                    //this.removeSelectedStyles(curSeriesIndx, 1);
                    this.applyComboChartSetting(curSeriesIndx, 0, 'P');
                    //}
                    //else if (chartSeries[curSeriesIndx].parentYAxis == otherAxis) {
                    //    isSettingExist = true;
                    //}
                    //}
                }
            }
            else if (chartSeries[curSeriesIndx].parentYAxis === 'S' && !axisType.isSecondaryExist) {
                axisType.isSecondaryExist = true;
            }
            else if (chartSeries[curSeriesIndx].parentYAxis === 'P' && !axisType.isPrimaryExist) {
                axisType.isPrimaryExist = true;
            }
        }
    }
}