﻿<?xml version="1.0" encoding="utf-8"?>
<feature xmlns:dm0="http://schemas.microsoft.com/VisualStudio/2008/DslTools/Core" dslVersion="1.0.0.0" Id="5ddf9a6f-b029-4a1b-be0a-c88f2525e575" description="Transform your SharePoint list data into insightful, customizable 2D and 3D charts" featureId="5ddf9a6f-b029-4a1b-be0a-c88f2525e575" imageUrl="" solutionId="00000000-0000-0000-0000-000000000000" title="CollabionCharts for SharePoint Online" version="" deploymentPath="$SharePoint.Project.FileNameWithoutExtension$_$SharePoint.Feature.FileNameWithoutExtension$" xmlns="http://schemas.microsoft.com/VisualStudio/2008/SharePointTools/FeatureModel">
  <projectItems>
    <projectItemReference itemId="b4462470-e002-44ba-a7d0-051d0713802c" />
    <projectItemReference itemId="068141d2-cf1c-4800-b7ad-978149fbfd15" />
    <projectItemReference itemId="21876423-2dfc-45af-aef1-b1f66cd78862" />
    <projectItemReference itemId="cbf6bf1b-69a1-40f8-aeee-8c74bcf6c3a3" />
    <projectItemReference itemId="302ab935-cafd-44d5-bef0-e80b4ba943da" />
    <projectItemReference itemId="ea9b158e-b228-4d3b-a163-f67fe1a1d597" />
    <projectItemReference itemId="bda053a1-e17c-4b16-a9d1-b539f43122e6" />
  </projectItems>
</feature>