
CC.Tabs.Tab = function () {
};

CC.Tabs.Base = function () {
};

CC.Tabs.Base.prototype = {
	// Level of wizard steps visibility
	// 1 - only first step;
	// 2 - select source and select fields steps;
	// 3 - all steps.
	level: 3,


	// Indicate if user press finish button.
	isFinish: false,

	//Confirmation message
	confirmationMsg: "",

	// If confirmation is true before leaving wizard step show confirmation message box.
	confirmation: false,

	isTitleChanged: false,

	// Create new step. Return new panel for wizard steps.
	create: function () { return new Ext.Panel({ title: 'BaseStep' }) },

	// This method is called after the transition to a next wizard step.
	activateStep: Ext.emptyFn,

	// This method is called before leaving step on the wizard.
	deactivateStep: Ext.emptyFn,

	// This method is called after user click any button on the confirmation message box.
	// If method return true then wizard open next step.
	confirmStep: function () { return true },

	// Validate step. Called before deactivateStep. Returns true or false.
	validationStep: function () { return true },

	// This method returns active tab index of main tab panel of step for open documentation. Or returns 0, when no tab panel at the step.
	getCurrentTabIndex: function () { return 0; }
};

CC.Providers = {
	SPProvider: {
		type: 0,

		entity: undefined,

		getData: function (callback, listID, viewID, selectedFields) {
			//return this.execute(callback, "getViewData", CC.med.providerConfig().DataSource.Data.ID, CC.med.providerConfig().DataSource.Data.ViewID, CC.med.providerConfig().DataSource.selectedFields.join());
			return this.execute(callback, "getViewData", listID, viewID, selectedFields);
		},

		getAllDataSource: function (callback) {
			return this.execute(callback, "getSPLists");
		},

		getViews: function (callback, listID) {
			return this.execute(callback, "getSPViews", listID);
		},

		getViewDetails: function (callback, listID, viewID) {
			return this.execute(callback, "getViewDetails", listID, viewID);
		},

		getFields: function (callback, listID, viewID) {
			return this.execute(callback, "getFields", listID, viewID);
		},

		setEntity: function () {
			//intialize on load
			this.entity = new CC.Data.SPList();
		},

		execute: function (callback, methodName, param1, param2, param3) {
			this.entity[methodName](param1, param2, param3).done(function (result) {
				callback(result);
			}).fail(function (args) {
				//var ex = new CC.Exception("FieldsNotFetched", "Unable to fetch the Fields", args);
			});
		}
	}
};

CC.ProviderManager = function () {
};

CC.ProviderManager.prototype = {
	// Providers collection
	providers: CC.Providers,

	// Current selected provider
	currentProvider: undefined,

	//selectedFields: [],

	//spList: undefined,

	selectProvider: function (type) {
		this.currentProvider = this.providers[type];
		for (var prov in this.providers) {
			if (type === this.providers[prov].type) {
				this.currentProvider = this.providers[prov];
				this.currentProvider.setEntity();

				return true;
			}
		}
		return false;
	}
};

CC.Chart = function () {
	// Charts
	Types = [
		[
			[01, 'Column', '0px -26px', 'column2d', 0, {'divLineIsDashed': '1'}],
			[02, 'Stacked Column', '-36px -26px', 'stackedcolumn2d', 2, ''],
			[03, '100% Stacked Column', '-72px -26px', 'stackedcolumn2d', 2, { 'stack100Percent': "1" }],
			[04, 'Waterfall', '-108px -26px', 'waterfall2d', 99, {'connectordashed': '1'}],
			[05, 'Pareto', '-144px -26px', 'pareto2d', 0, '']
		],
		[
			[11, 'Bar', '-180px -26px', 'bar2d', 0, ''],
			[12, 'Stacked Bar', '-216px -26px', 'stackedbar2d', 2, ''],
			[13, '100% Stacked Bar', '0px -62px', 'stackedbar2d', 2, { 'stack100Percent': "1" }]
		],
		[
			[21, 'Line', '-36px -62px', 'line', 0, { 'xAxisLineThickness': '1', 'showAlternateHGridColor': '1' }],
			[22, 'Spline', '-72px -62px', 'spline', 0, {'showShadow': '1'}]
		],
		[
			[31, 'Area', '-108px -62px', 'area2d', 0, {'usePlotGradientColor': '1', 'plotGradientColor': '#1aaf5d'}],
			[32, 'Spline Area', '-144px -62px', 'splinearea', 7, ''],
			[33, 'Stacked Area', '-180px -62px', 'stackedarea2d', 2, {'divLineIsDashed': '1'}],
			[34, '100% Stacked Area', '-216px -62px', 'stackedarea2d', 2, { 'stack100Percent': "1" }]
		],
		[
			[41, 'Single Y Axis', '0px -98px', 'mscombi2d', 3, ''],
			[42, 'Dual Y Axis', '-36px -98px', 'mscombidy2d', 3, '']
		],
		[
			[51, 'Pie 3D', '-252px -26px', 'pie3d', 0, { 'use3DLighting': '1', 'transposeAnimation': '1' }],
			[52, 'Pie 2D', '-72px -98px', 'pie2d', 0, { 'use3DLighting': '1', 'transposeAnimation': '1' }],
			[53, 'Doughnut 3D', '-252px -62px', 'doughnut3d', 0, { 'use3DLighting': '1', 'transposeAnimation': '1' }],
			[54, 'Doughnut 2D', '-108px -98px', 'doughnut2d', 0, { 'use3DLighting': '1', 'transposeAnimation': '1' }]
		],
		[
			[61, 'Pyramid 3D', '-288px -26px', 'pyramid', 0, { 'alignCaptionWithCanvas': '1', 'showPercentValues': '0', 'chartLeftMargin': '40', 'captionPadding': '25' }],
			[62, 'Pyramid 2D', '-144px -98px', 'pyramid', 0, { 'alignCaptionWithCanvas': '1', 'showPercentValues': '0', 'chartLeftMargin': '40', 'captionPadding': '25', 'is2D': '1' }],
			[63, 'Funnel 3D', '-288px -62px', 'funnel', 0, { 'isHollow': '1', 'showPercentValues': '0' }],
			[63, 'Funnel 2D', '-180px -98px', 'funnel', 0, { 'isHollow': '1', 'showPercentValues': '0', 'is2D': '1' }],
			[64, 'Marimekko', '-216px -98px', 'marimekko', 1, '']
		]
	];

	TypeFields = ['type', 'name', 'pos', 'internalName', 'series', 'distinct'];
	CatFields = ['id', 'name', 'pos'];

	// Chart categories
	Categories = [
		[0, 'Column', '0px 0px'],
		[1, 'Bar', '-26px 0px'],
		[2, 'Line', '-52px 0px'],
		[3, 'Area', '-78px 0px'],
		[4, 'Combo', '-104px 0px'],
		[5, 'Pie', '-130px 0px'],
		[6, 'Others', '-156px 0px']//,
		// [7, 'Column', 'Column.png'],
		// [8, 'Bar', 'Column.png'],
		// [9, 'Line', 'Column.png'],
		// [10, 'Area', 'Column.png'],
		// [11, 'Combo', 'Column.png'],
		// [12, 'Pie', 'Column.png'],
		// [13, 'Others', 'Column.png']
	];

	selComboCls = ['selBar', 'selArea', 'selLine'];

	axisCharts = [[[0, 'Primary Axis', -169, 'P', ''],
				[1, 'Secondary Axis', -182, 'S', '']],
				[[0, 'Bar Chart', -195, 'bar', selComboCls[0]],
				[1, 'Area Chart', -208, 'area', selComboCls[1]],
				[2, 'Line Chart', -221, 'line', selComboCls[2]]]];

	axisChartsType = ['id', 'name', 'pos', 'internalName', 'cls'];


};

CC.Chart.prototype = {
	// Return TypeFields
	getTypeFields: function () {
		return TypeFields;
	},

	getCatFields: function () {
		return CatFields;
	},

	getAxisChartTypeFields: function () {
		return axisChartsType;
	},

	getCategories: function () {
		return Categories;
	},

	// Get charts by category
	// category - number in array
	getTypesByCategory: function (category) {
		return (!isNaN(category)) ? Types[category] : Types;
	},

	getAxisChart: function (indx) {
		return axisCharts[indx];
	},

	getSelComboCls: function () {
		return selComboCls;
	},

	getFullChartName: function (comboChartName) {
	    var chartName;
	    switch (comboChartName) {
	        case 'bar':
	            chartName = 'column2d';
	            break;
	        case 'area':
	            chartName = 'area2d';
	            break;
	        default:
	            chartName = 'line';
	            break;
	    }

	    return chartName;
	},

	getFullComboName: function (chartName) {
	    var comboChartName;
	    switch (chartName) {
	        case 'column2d':
	            comboChartName = 'bar';
	            break;
	        case 'area2d':
	            comboChartName = 'area';
	            break;
	        case 'line':
	            comboChartName = 'line';
	            break;
	        default:
	            break;
	    }

	    return comboChartName;
	}
};

CC.ChartManager = function () {
	var me = this;

	//fields axis
	this.chartConfig = [];

	//Labels and Numbers tab
	this.cosmetics = [];

	this.chart = new CC.Chart;
};

CC.Stores = {
	create: function () {
		this.Lists = new Ext.data.JsonStore({
			fields: ['Id', 'Title']
		});

		this.Views = new Ext.data.JsonStore({
			fields: ['Id', 'Title']
		});

		this.ViewFields = new Ext.data.JsonStore({
			fields: ['InternalName', 'Title', 'TypeAsString']
		});
	}
};

CC.Tabs.Active = function () { };
//Ext.ns('Collabion.App.Charts.Tabs.Active');














//(function () {
//    //CC.CM = new CC.ChartManager();
//    //CC.PM = new CC.ProviderManager();
//    //CC.ST = new CC.Settings();
//    //CC.ST = new CC.Settings();
//    //CC.Med = CC.Mediator.getInstance();
//    //CC.CM.Cache = [];
//})();
