// ExtJS overrides
(function () {
    Ext.override(Ext.form.TextField, {
        afterRender: function () {
            Ext.form.TextField.superclass.afterRender.call(this);
            // Add help plugin for label
            var disableFocus = this.disableFocus;
            if (this.ccTip) {
                $(this.el.dom)
				.focus(function () {
				    if (!disableFocus)
				        $(this).tooltipster('show');
				})
				.blur(function () {
				    if (!disableFocus)
				        $(this).tooltipster('hide');
				})
				.attr({
				    "title": this.ccTip
				})
				//$(this.el.dom).addClass(util.tipCls)
				.addClass(CC.util.getTooltipCls());
            }
        }
    });

    //
    // ComboBox
    //
    Collabion.App.ComboBox = Ext.extend(Ext.form.ComboBox, {
        initComponent: function () {
            Ext.apply(this, {
                mode: 'local',
                triggerAction: 'all',
                editable: false,
                cancelSelection: false,
                overriteValue: '',
                blockError: false
            });

            Collabion.App.ComboBox.superclass.initComponent.call(this);
        }
    });
    Ext.reg('cc:combo', Collabion.App.ComboBox);

    Ext.override(Collabion.App.ComboBox, {
        afterRender: function () {
            var img = this.el.parent().dom.children[1];//Arrow
            this.listWidth = this.width - img.width;
            var disableFocus = this.disableFocus;
            var ccTip = this.ccTip;
            this.ccTip = '';

            Collabion.App.ComboBox.superclass.afterRender.call(this);
            // Add help plugin for label

            if (ccTip) {
                $(this.el.parent().dom)
				.focus(function () {
				    if (!disableFocus)
				        $(this).tooltipster('show');
				})
				.blur(function () {
				    if (!disableFocus)
				        $(this).tooltipster('hide');
				})
				.attr({
				    "title": ccTip
				})
				.addClass(CC.util.getTooltipCls());
            }

            var txt = this.el.parent().dom.children[0];//Actual Box
            //var img = this.el.parent().dom.children[1];

            //txt.style.height = "22px";

            txt.style.width = (parseInt(txt.style.width) - img.width) + "px";
            //txt.style.paddingRight = img.width + "px";
            txt.style.paddingRight = "20px";
            //txt.style.textIndent = "2px";
            img.style.border = "none";
            img.style.marginTop = "4px";
            //img.style.left = (parseInt(txt.style.width) - ((img.width) - 3)) + "px";
            //img.style.left = (parseInt(txt.style.width) - (3)) + "px";
            img.style.left = (parseInt(txt.style.width) + 4) + "px";
        }
    });

    Ext.override(Ext.form.Checkbox, {
        afterRender: function () {
            //
            var disableFocus = this.disableFocus;
            var ccTip = this.ccTip;
            this.ccTip = '';

            Ext.form.Checkbox.superclass.afterRender.call(this);
            // Add help plugin for label

            if (ccTip) {
                $(this.el.parent().dom)
				.focus(function () {
				    if (!disableFocus)
				        $(this).tooltipster('show');
				})
				.blur(function () {
				    if (!disableFocus)
				        $(this).tooltipster('hide');
				})
				.attr({
				    "title": ccTip
				})
				.addClass(CC.util.getTooltipCls());
            }
        }
    });

    Ext.override(Ext.form.Label, {
        afterRender: function () {
            var disableFocus = this.disableFocus;
            var ccTip = this.ccTip;
            this.ccTip = '';

            Ext.form.Label.superclass.afterRender.call(this);
            // Add help plugin for label

            if (ccTip) {
                $(this.el.dom)
				.focus(function () {
				    if (!disableFocus)
				        $(this).tooltipster('show');
				})
				.blur(function () {
				    if (!disableFocus)
				        $(this).tooltipster('hide');
				})
				.attr({
				    "title": ccTip
				})
				.addClass(CC.util.getTooltipCls());
            }

            if (this.ccHelp) {
                //var hlpIcn = document.createElement('img');
                var hlpIcn = document.createElement('span');
                //hlpIcn.src = CC.util.getHelpIconImg();
                //hlpIcn.style.paddingLeft = "10px";
                hlpIcn.style.paddingTop = "1px";
                hlpIcn.style.width = "16px";
                hlpIcn.style.height = "16px";
                hlpIcn.style["float"] = "right";

                //IE 9 Fix
                // if(hlpIcn.classList){
                // hlpIcn.classList.add(CC.util.getTooltipCls());
                // hlpIcn.classList.add('all-images');
                // }
                // else{
                // hlpIcn.className = CC.util.getTooltipCls();
                // hlpIcn.className += " all-images";
                // }

                CC.util.addCssClass(hlpIcn, [CC.util.getTooltipCls(), 'all-images']);
                hlpIcn.title = this.ccHelp;

                $(this.el.dom.parentElement).append(hlpIcn);
            }
        }
    });

    Ext.override(Ext.cux.form.SpinnerField, {
        afterRender: function () {
            var disableFocus = this.disableFocus;
            var ccTip = this.ccTip;
            this.ccTip = '';

            Ext.cux.form.SpinnerField.superclass.afterRender.call(this);
            // Add help plugin for label

            if (ccTip) {
                $(this.el.parent().dom)
				.focus(function () {
				    if (!disableFocus)
				        $(this).tooltipster('show');
				})
				.blur(function () {
				    if (!disableFocus)
				        $(this).tooltipster('hide');
				})
				.attr({
				    "title": ccTip
				})
				.addClass(CC.util.getTooltipCls());
            }

            // var spinnerBtns = $(this.el.parent().dom).find('img');
            // spinnerBtns[0].style.height = "24px";
            // spinnerBtns[1].style.height = "24px";
        }
    });

    var HelpIcon = Ext.extend(Ext.form.Label, {
        afterRender: function () {
            // HelpIcon.html = '<span class="tooltip all-images tooltipstered" style="padding-top: 1px; width: 16px; height: 16px; float: right;"></span>';
            // HelpIcon.superclass.initComponent.call(this);
            hlpIcn = document.createElement('span');
            hlpIcn.style.paddingTop = "1px";
            hlpIcn.style.width = "16px";
            hlpIcn.style.height = "16px";
            hlpIcn.style["float"] = "right";
            hlpIcn.style.margin = "4px 5px";
            CC.util.addCssClass(hlpIcn, [CC.util.getTooltipCls(), 'all-images', 'tooltipstered']);
            hlpIcn.title = this.helpText;
            this.text = '';
            // $(this.el.dom.parentElement).append(hlpIcn);
            $(hlpIcn).insertBefore(this.el.dom.parentElement.parentElement);
            var disableFocus = hlpIcn.disableFocus;
            this.hideLabel = true;
            this.el.dom.innerText = '';
            CC.util.generateTip();
        }
    });

    Ext.reg('CC:HelpIcon', HelpIcon);

    // Adding help plugin for extjs fields
    var helpPlugin = function (cmp, obj) {
        cmp.plugins = cmp.plugins || [];
        if (!Ext.isArray(cmp.plugins)) {
            cmp.plugins = [cmp.plugins]
        }
        cmp.plugins.push(obj);
    }

    //Making combo box work for IE9
    if (typeof Range.prototype.createContextualFragment == "undefined") {
        Range.prototype.createContextualFragment = function (html) {
            var doc = window.document;
            var container = doc.createElement("div");
            container.innerHTML = html;
            var frag = doc.createDocumentFragment(), n;
            while ((n = container.firstChild)) {
                frag.appendChild(n);
            }
            return frag;
        };
    }
})();