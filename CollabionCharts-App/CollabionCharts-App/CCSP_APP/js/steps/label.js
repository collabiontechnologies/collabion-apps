CC.Tabs.Tab.Label = Ext.extend(CC.Tabs.Base, {
    create: function () {
		//var oCheckboxGroup = new Ext.form.CheckboxGroup({
		//		columns: 3,
		//		vertical: true,
		//		width: 550,
		//		itemCls: 'css-checkbox elegant',
		//		style: {
		//				'padding-left': '30px',
		//				'padding-top': '20px'
		//			},
		//		items:[
		//			{
		//				boxLabel: "Data Labels",
		//				inputValue: 1
		//			}, {
		//				boxLabel: "Data Values",
		//				inputValue: 2
		//			}, {
		//				boxLabel: "Chart Legend",
		//				inputValue: 3
		//			}
		//		]
		//	});

		var checkboxes = {
			// xtype: 'fieldset',
			xtype: 'panel',
			layout: 'table',
			id: 'tabChkBoxes',
			layoutConfig: {
				tableAttrs: {
					style: {
						// 'width': '100%',
						'border': 'none'//,
						// 'padding-right': '20px',
						//'height': '103px'
					}
				},
				columns: 7
			},
			// layout: {
			//     type: 'hbox',
			//     pack: 'start',
			//     align: 'stretch'
			// },
			border: false,
			cls: 'checkCntnr',
			width: 600,
			// layout: {
			// 	type: 'table',
			// 	columns: 3,
			// 	rows: 1,
			// 	tableAttrs: {
			// 		border: '0',
			// 		cellspacing: '1px',
			// 		style: {
			// 			width: '100%'
			// 			}
			// 	}
			// },
			// style: {
			// 	'padding-left': '20px',
			// 	'padding-top': '20px'
			// },
			items: [{
					xtype:'checkbox',
					id: 'chkLabels',
					cls: 'css-checkbox elegant sme all-images',
					boxLabel: ' ',
					style: {
						'margin-top': '-1px',
						'vertical-align': 'middle'
					},
					listeners:{
						afterrender:function(){
							//Ext.getCmp('chkLabels').wrap.dom.lastChild.classList.add('css-label','sme');
						    CC.util.addCssClass(Ext.getCmp('chkLabels').wrap.dom.lastChild, ['css-label', 'sme', 'all-images']);
						},
						check:function(chk, val){
						    CC.med.publish(CC.util.tabsActivate('lblUpdt'), { id: this.id, val: (val ? "1" : "0") });

						    if (val)
						        Ext.getCmp('chkLabelsLabel').removeClass('inactive');
						    else
						        Ext.getCmp('chkLabelsLabel').addClass('inactive');
						}
					}
					// inputValue: 1,
					// width: 20
				}, {
				    xtype: 'label',
				    id: 'chkLabelsLabel',
				    forId: 'chkLabels',
				    // cls: 'subHead',
				    cls: 'inactive',
				    text: CC.msgs.lbl151
				    // html: '<label for="chkLabels" class="css-label">Data labels</label>'
				}, {
					xtype:'checkbox',
					id: 'chkValues',
					cls: 'css-checkbox elegant sme all-images',
					boxLabel: ' ',
					style: {
						'margin-top': '-1px',
						'vertical-align': 'middle'
					},
					listeners:{
						afterrender:function(){
							CC.util.addCssClass(Ext.getCmp('chkValues').wrap.dom.lastChild, ['css-label', 'sme', 'all-images']);
						},
						check: function (chk, val) {
						    CC.med.publish(CC.util.tabsActivate('lblUpdt'), { id: this.id, val: (val ? "1" : "0") });

						    if (val)
						        Ext.getCmp('chkValuesLabel').removeClass('inactive');
						    else
						        Ext.getCmp('chkValuesLabel').addClass('inactive');
						}
					}
					// inputValue: 2,
					// width: 20
				}, {
				    xtype: 'label',
				    id: 'chkValuesLabel',
				    forId: 'chkValues',
				    // cls: 'subHead',
				    cls: 'inactive',
				    text: CC.msgs.lbl152
				}, {
					xtype:'checkbox',
					id: 'chkLegends',
					cls: 'css-checkbox elegant sme all-images',
					boxLabel: ' ',
					style: {
						'margin-top': '-1px',
						'vertical-align': 'middle'
					},
					listeners:{
						afterrender:function(){
							CC.util.addCssClass(Ext.getCmp('chkLegends').wrap.dom.lastChild, ['css-label', 'sme', 'all-images']);
						},
						check: function (chk, val) {
						    CC.med.publish(CC.util.tabsActivate('lblUpdt'), { id: this.id, val: (val ? "1" : "0") });

						    if (val)
						        Ext.getCmp('chkLegendsLabel').removeClass('inactive');
						    else
						        Ext.getCmp('chkLegendsLabel').addClass('inactive');
						}
					}
					// inputValue: 3,
					// width: 20
				}, {
				    xtype: 'label',
				    id: 'chkLegendsLabel',
				    forId: 'chkLegends',
				    // cls: 'subHead',
				    cls: 'inactive',
				    text: CC.msgs.lbl153
			}]
				// items: [{
				// 	xtype:'toggleField',
				// 	id: 'tglLabels',
				// 	checked: true,
				// 	boxLabel: "Data labels",
				// 	boxLabelCss: "x-form-item",
				// 	toggleTxtCss: "x-form-item",
				// 	textLeft: true,
				// 	style: {
				// 		"margin-Bottom": '0px'
				// 	},
				// 	toggleTxtStyle:{
				// 		"top": "0px"
				// 	},
				// 	inputValue: 1,
				// 	width: 150
				// }, {
				// 	xtype:'toggleField',
				// 	id: 'tglValues',
				// 	textLeft: true,
				// 	boxLabel: "Data values",
				// 	boxLabelCss: "x-form-item",
				// 	toggleTxtCss: "x-form-item",
				// 	style: {
				// 		"margin-Bottom": '0px'
				// 	},
				// 	toggleTxtStyle:{
				// 		"top": "0px"
				// 	},
				// 	inputValue: 1,
				// 	width: 150,
				// 	listeners:{
				// 		scope: this,
				// 		'toggle': function(ele){
				// 			//Console.log("Toggle fired");
				// 		}
				// 	}
				// }, {
				// 	xtype:'toggleField',
				// 	id: 'tglLegends',
				// 	textLeft: true,
				// 	boxLabel: "Chart legend",
				// 	boxLabelCss: "x-form-item",
				// 	toggleTxtCss: "x-form-item",
				// 	style: {
				// 		"margin-Bottom": '0px'
				// 	},
				// 	toggleTxtStyle:{
				// 		"top": "0px"
				// 	},
				// 	inputValue: 1,
				// 	width: 160
				// }]
		};

		var horizontalLine = {
			xtype: 'box',
			hidden: false,
			style: {
			    'margin': '10px 20px 10px 20px',
			    // 'margin-top': '10px',
			    // 'margin-bottom': '10px',
			    // 'margin-left': '20px'
					},
			autoEl : {
				tag : 'hr'
			}
		};

		var labels = {
				xtype: 'fieldset',
				id: 'fldLbl',
				border: false,
				width: 620,
				cls: 'helpIcon',
				layout: {
					type: 'table',
					columns: 3,
					tableAttrs: {
						border: '0',
						cellspacing: '1px',
						style: {
							width: '100%',
							'float': 'left'
						}
					}
				},
				style: {
					'padding-left': '20px',
					'padding-top': '0px'
				},
				items: [{
					xtype: 'label',
				    //cls: 'x-form-item',
					cls: 'x-form-item-sub',
					text: CC.msgs.lbl154
				},{
					xtype: 'label',
				    //cls: 'x-form-item',
					cls: 'x-form-item-sub',
					text: CC.msgs.lbl155,
					colspan: 3
				},{
                    id: 'txtCrtTitle',
				    xtype: 'textfield',
					// width: '90%'
					width: 180,
					listeners: {
					    change: function (sender, newVal) {
					        CC.med.publish(CC.util.tabsActivate('lblUpdt'), { id: this.id, val: newVal });
					    },
					    specialkey: function (field, e) {
					        // e.HOME, e.END, e.PAGE_UP, e.PAGE_DOWN,
					        // e.TAB, e.ESC, arrow keys: e.LEFT, e.RIGHT, e.UP, e.DOWN
					        if (e.getKey() == e.ENTER) {

					            CC.med.publish(CC.util.tabsActivate('lblUpdt'), { id: this.id, val: field.getValue() });
					        }
					    }
					}
				},{
				    id: 'txtSubTitle',
				    xtype: 'textfield',
					// width: '90%'
					width: 180,
					listeners: {
					    change: function (sender, newVal) {
					        CC.med.publish(CC.util.tabsActivate('lblUpdt'), { id: this.id, val: newVal });
					    },
					    specialkey: function (field, e) {
					        if (e.getKey() == e.ENTER) {
					            CC.med.publish(CC.util.tabsActivate('lblUpdt'), { id: this.id, val: field.getValue() });
					        }
					    }
					}
				},{
					xtype: 'label',
				    //cls: 'x-form-item',
					cls: 'x-form-item-sub',
					text: ''
				},{
					html: '<div style="height:15px"/>',
					border: false,
					colspan: 3
				},{
					xtype: 'label',
				    //cls: 'x-form-item',
					cls: 'x-form-item-sub',
					text: CC.msgs.lbl156
				},{
					xtype: 'label',
				    //cls: 'x-form-item',
					cls: 'x-form-item-sub',
					text: CC.msgs.lbl157
				},{
				    xtype: 'label',
				    id: 'lblScndYTitle',
				    //cls: 'x-form-item',
				    cls: 'x-form-item-sub',
					text: CC.msgs.lbl158,
					disabled: true
				},{
				    id: 'txtXTitle',
				    xtype: 'textfield',
					// width: '90%'
					width: 180,
					listeners: {
					    change: function (sender, newVal) {
					        CC.med.publish(CC.util.tabsActivate('lblUpdt'), { id: this.id, val: newVal });
					    },
					    specialkey: function (field, e) {
					        if (e.getKey() == e.ENTER) {
					            CC.med.publish(CC.util.tabsActivate('lblUpdt'), { id: this.id, val: field.getValue() });
					        }
					    }
					}
				},{
				    id: 'txtYTitle',
				    xtype: 'textfield',
					// width: '90%'
					width: 180,
					listeners: {
					    change: function (sender, newVal) {
					        CC.med.publish(CC.util.tabsActivate('lblUpdt'), { id: this.id, val: newVal });
					    },
					    specialkey: function (field, e) {
					        if (e.getKey() == e.ENTER) {
					            CC.med.publish(CC.util.tabsActivate('lblUpdt'), { id: this.id, val: field.getValue() });
					        }
					    }
					}
				},{
				    id: 'txtScndYTitle',
				    xtype: 'textfield',
					// width: '90%',
					width: 180,
					disabled: true,
					listeners: {
					    change: function (sender, newVal) {
					        CC.med.publish(CC.util.tabsActivate('lblUpdt'), { id: this.id, val: newVal });
					    },
					    specialkey: function (field, e) {
					        if (e.getKey() == e.ENTER) {
					            CC.med.publish(CC.util.tabsActivate('lblUpdt'), { id: this.id, val: field.getValue() });
					        }
					    }
					}
				}]
			};

        return {
            id: 'tabLabel',
            title: CC.msgs.tb113,
			header: false,
			//iconCls: 'x-labels-icon',
            defaults: { autoScroll: false },
            items: [{
                autoScroll: true,
                border: false,
                //items: [oCheckboxGroup, horizontalLine, labels]
                items: [{
                    id: 'chkHelp',
                    xtype: 'CC:HelpIcon',
                    border: false,
                    helpText: CC.tips.lbl151
                }, checkboxes, horizontalLine, labels]
            }],
            listeners: {
                afterlayout: function () {
                    $('#tabLabel').find('span').next().css({ 'width': '100%', 'position': 'absolute' });
                    $('#tabLabel').find('span').css({ 'z-index': '100', 'position': 'relative' });
                }
            }
        }
    },

	validationStep: function () {
        var valid = true;
        return valid;
    },

	activateStep: function () {
	    this.disabled = false;

	    //CC.Tabs.Activate.labels.activate();
	    CC.med.publish(CC.util.tabsActivate('labels'));
	}
});
