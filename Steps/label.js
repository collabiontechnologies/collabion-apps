CC.Tabs.Tab.Label = Ext.extend(CC.Tabs.Base, {
    create: function () {
		var oCheckboxGroup = new Ext.form.CheckboxGroup({
				columns: 3,
				vertical: true,
				width: 550,
				style: {
						'padding-left': '30px',
						'padding-top': '20px'
					},
				items:[
					{boxLabel: "Show data labels", inputValue: 1},
					{boxLabel: "Show data values", inputValue: 2},
					{boxLabel: "Show chart legend", inputValue: 3}
				]
			});
			
		var toggles = {
			xtype: 'fieldset',
				border: false,
				cls: 'toggleCntnr',
				width: 600,
				layout: {
					type: 'table',
					columns: 3,					
					tableAttrs: {
						border: '0',
						cellspacing: '1px',
						style: {						
							width: '100%'						
							}
					}					
				},
				style: {
					'padding-left': '20px',
					'padding-top': '20px'
				},
				items: [{
					xtype:'toggleField',
					id: 'tglLabels',
					checked: true,
					boxLabel: "Data labels",
					boxLabelCss: "x-form-item",
					toggleTxtCss: "x-form-item",
					textLeft: true,
					style: {
						"margin-Bottom": '0px'
					},
					toggleTxtStyle:{
						"top": "0px"
					},
					inputValue: 1,
					width: 150
				}, {
					xtype:'toggleField',
					id: 'tglValues',
					textLeft: true,
					boxLabel: "Data values",
					boxLabelCss: "x-form-item",
					toggleTxtCss: "x-form-item",
					style: {
						"margin-Bottom": '0px'
					},
					toggleTxtStyle:{
						"top": "0px"
					},
					inputValue: 1,
					width: 150,
					listeners:{
						scope: this,
						'toggle': function(ele){
							console.log("Toggle fired");
						}
					}
				}, {
					xtype:'toggleField',
					id: 'tglLegends',
					textLeft: true,
					boxLabel: "Chart legend",
					boxLabelCss: "x-form-item",
					toggleTxtCss: "x-form-item",
					style: {
						"margin-Bottom": '0px'
					},
					toggleTxtStyle:{
						"top": "0px"
					},
					inputValue: 1,
					width: 160
				}]
		};
			
		var horizontalLine = {
			xtype: 'box',
			hidden: false,
			style: {
					'margin': '30px',
					'margin-top': '10px',
					'margin-bottom': '10px',
					'margin-left': '20px'
					},
			autoEl : {
				tag : 'hr'
			}
		};
			
		var labels = {	
				xtype: 'fieldset',
				border: false,
				width: 600,
				layout: {
					type: 'table',
					columns: 3,					
					tableAttrs: {
						border: '0',
						cellspacing: '1px',
						style: {						
							width: '100%'						
							}
					}					
				},
				style: {
					'padding-left': '20px'
				},				
				items: [{
					xtype: 'label',
					cls: 'x-form-item',
					text: 'Chart Title'
				},{
					xtype: 'label',
					cls: 'x-form-item',
					text: 'Chart Sub-title',
					colspan: 3
				},{
					xtype:'textfield',
					ccTip: '12345',
					width: '90%'
				},{
					xtype:'textfield',
					width: '90%'
				},{
					xtype: 'label',
					cls: 'x-form-item',
					text: ''
				},{
					html: '<div style="height:15px"/>',
					border: false,
					colspan: 3
				},{
					xtype: 'label',
					cls: 'x-form-item',
					text: 'X Axis Title'
				},{
					xtype: 'label',
					cls: 'x-form-item',
					text: 'Y Axis Title'
				},{
					xtype: 'label',
					cls: 'x-form-item',
					text: 'Secondary Y Axis Title'
				},{
					xtype:'textfield',
					width: '90%'
				},{
					xtype:'textfield',
					width: '90%'
				},{
					xtype:'textfield',
					width: '90%'
				}]
			};
			
        return {
            id: 'tabLabel',
            title: 'LABELS',
			header: false,
			iconCls: 'x-labels-icon',
            defaults: { autoScroll: false },
            items: [{
				autoScroll: true,
				border: false,
				//items: [oCheckboxGroup, horizontalLine, labels]
				items: [toggles, horizontalLine, labels]
			}]
        }
    },
	
	validationStep: function () {
        var valid = true;
        return valid;
    },
	
	activateStep: function () {
		this.disabled = false;
	}
});
