
CC.Tabs.Tab.Chart = Ext.extend(CC.Tabs.Base, {
    valid: true,

    create: function () {
        //
        // Configuration for Wizard
        //
        //this.config = {
        //	ChartIconsPath: '../CCSP_APP/assets/'
        //};

        //this.Chart = new CC.Charts();
        this.chart = CC.med.cmChart();


        this.prevChrtCat = -1;
        this.prevChrtTyp = -1;

        this.ct_store_charts = new Ext.data.ArrayStore({
            fields: this.chart.getTypeFields(),
            data: this.chart.getTypesByCategory(0)
        });

        this.ct_store_chartsCategories = new Ext.data.ArrayStore({
            fields: this.chart.getCatFields(),
            data: this.chart.getCategories()
        });

        // var ctTemplateCat = new Ext.XTemplate(
        // '<tpl for=".">',
        // '<div class="cc-thumb-wrap-cat" title="{name}">',
        // '<div class="cc-thumb"><img src="' + this.config.ChartIconsPath + '{img}"></div>',
        // '<div class="cc-thumb-label"><div>{name}</div><div class="aux"></div></div>',
        // '<div class="cc-thumb-downArw"><img src="' + this.config.ChartIconsPath + 'down-arrow.png"></div>',
        // '</div>',
        // '</tpl>',
        // '<div class="x-clear"></div>'
        // );

        var ctTemplateCat = new Ext.XTemplate(
			'<ul id="my-list">',
				'<tpl for=".">',
					//'<li style="width:60px; height:100%; text-align: center; padding: 0px 10px 0px 10px;">',
					//'<li style="width:42px; height:100%; text-align: center; padding: 0px 10px 0px 10px;">',
					'<li style="width:42px; height:100%; text-align: center; padding: 0px 20px 0px 6px;">',
						'<div class="cc-thumb-wrap-cat">',
							//'<div class="cc-thumb-cat"><img src="' + this.config.ChartIconsPath + '{img}" alt="{name}" title="{name}" /></div>',
							//'<div class="cc-thumb-cat"><img src="' + CC.util.getImgPath() + '{img}" alt="{name}" /></div>',
                            '<div class="cc-thumb-cat"><span class="all-images" style=" height: 26px; width: 26px; background-position: {pos}"></span></div>',
							'<div class="x-form-item">{name}</div>',
							'<div class="cc-thumb-downArw"><span class="all-images" style=" height: 17px; width: 18px; background-position: -234px -143px"/></div>',
						'</div>',
					'</li>',
				'</tpl>',
			'</ul>'
		);

        var ctTemplateChart = new Ext.XTemplate(
			'<tpl for=".">',
				//'<div class="cc-thumb-wrap tooltip" title="{name}">',
				'<div class="cc-thumb-wrap">',
					//'<div class="cc-thumb-FixWid"><img src="' + CC.util.getImgPath() + '{img}"></div>',
                    '<div class="cc-thumb-FixWid"><span class="all-images" style=" height: 36px; width: 36px; background-position: {pos}"></span></div>',
					//'<div class="cc-thumb-label"><div class="x-form-item">{name}</div><div class="aux"></div></div>',
					'<div class="cc-thumb-label"><div class="x-form-item">{name}</div></div>',
				'</div>',
			'</tpl>' //,
			//'<div class="x-clear"></div>'
		);

        this.chartCatogories = new Ext.DataView({
            id: 'pnlCat',
            store: this.ct_store_chartsCategories,
            tpl: ctTemplateCat,
            //itemSelector: 'div.cc-thumb-wrap',
            itemSelector: 'div.cc-thumb-wrap-cat',
            overClass: 'cc-x-view-over-noborder',
            singleSelect: true,
            listeners: {
                //selectionchange: this.switchChrtCat,
                //selectionchange: CC.Tabs.Activate.charts.switchChrtCat,
                selectionchange: function () {
                    CC.med.publish(this.id, this.getSelectedRecords());
                },
                afterrender: function () {
                    $('#my-list').hoverscroll({
                        fixedArrows: false,
                        rtl: false,
                        arrows: true,
                        vertical: false,
                        arrowsOpacity: 1,
                        width: 620,
                        height: '100%',
                        debug: false,
                        disableTrigger: false,
                        triggerSpeed: 8,
                        triggerMax: 88
                    });

                    CC.med.subscribe({
                        id: this.id,
                        //group: CC.util.target().chartsCat,
                        tab: CC.util.tabs().charts,
                        type: CC.util.xTypes(this.getXType())
                    })

                    //this.chartCatogories.getSelectedRecords()
                } //,
                //scope: this
            }
        });

        this.chartTypes = new Ext.DataView({
            id: 'pnlCType',
            store: this.ct_store_charts,
            tpl: ctTemplateChart,
            itemSelector: 'div.cc-thumb-wrap',
            //overClass: 'cc-x-view-over',
            overClass: 'cc-x-view-over-noborder',
            autoScroll: true,
            singleSelect: true,
            listeners: {
                //selectionchange: this.switchChrtTyp,
                //selectionchange: CC.Tabs.Activate.charts.switchChrtTyp,
                selectionchange: function () {
                    CC.med.publish(this.id, this.getSelectedRecords());
                },
                //scope: this,
                afterrender: function () {
                    // Ext.getCmp('pnlCType').el.dom.lastChild.style.width='110px';
                    //this.chartTypes.el.dom.lastChild.style.width = '110px';
                    this.el.dom.lastChild.style.width = '110px';

                    CC.med.subscribe({
                        id: this.id,
                        //group: CC.util.target().chartTyp,
                        tab: CC.util.tabs().charts,
                        type: CC.util.xTypes(this.getXType())
                    })
                }
            }
        });

        var chartContent = {
            id: 'chartContent',
            border: false,
            autoScroll: false,
            layout: 'vbox',
            items: [this.chartCatogories,
			{
			    xtype: 'panel',
			    id: 'pnlChartTypes',
			    autoScroll: true,
			    height: 180,
			    border: false,
			    items: [this.chartTypes]
			}],
            listeners: {
                scope: this,
                resize: function (a) {
                    //alter the height of the second panel to enable or disable the scroll bar.
                    //84 is the height of the first panel and 10 is the top margin
                    Ext.getCmp('pnlChartTypes').setHeight(a.el.getHeight() - 84 - 10);
                    Ext.getCmp('pnlChartTypes').doLayout();
                    // 
                    //Ext.getCmp('pnlChartTypes').setHeight(a.el.getHeight() - Ext.getCmp('pnlCat').getHeight() - parseInt($('#pnlChartTypes').css('marginTop')));
                },
                afterrender: function () {
                    //removing the bottom-padding to adjust the charts in a single line. Otherwise a scroll-bar will appear.
                    //after setting the padding also need to set the height. The height will be 5 less than its parent because of 5px top padding
                    $('#chartContent').parent().css({
                        "padding-bottom": "0px",
                        "height": ($('#chartContent').parent().parent().height() - 5)
                    });
                    // 
                    //Ext.getCmp('pnlChartTypes').setHeight(a.el.getHeight() - 84 - 10);
                    //Ext.getCmp('pnlChartTypes').doLayout();
                    Ext.getCmp('chartContent').doLayout();

                    this.activateStep();
                }
            }

        };

        return {
            id: 'pnlCharts',
            title: CC.msgs.tb112,
            header: false,
            padding: 5,
            //iconCls: 'x-charts-icon',
            defaults: { autoScroll: false },
            valid: this.valid,
            items: [chartContent] //,
            //listeners:{
            //	scope: this,
            //	afterrender: function(){
            //$('.tooltip').tooltipster(
            //{
            //theme: 'cc-tooltipster-shadow'
            //}
            //);
            //	}
            //}
        }
    },

    validationStep: function () {
        //return this.valid;
        return Ext.getCmp('pnlCharts').initialConfig.valid;
    },

    activateStep: function () {
        this.disabled = false;

        //CC.Tabs.Activate.charts.activate();
        CC.med.publish(CC.util.tabsActivate('charts'));
    }

});