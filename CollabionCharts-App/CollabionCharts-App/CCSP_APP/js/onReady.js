﻿
$(document).ready(function () {
    /* set the height of the parent container to Max.
     * 2 is subtracted for padding 
     * apply dynamic height-width for the wizard */
    //$('#panel-wizard').height($('#ccParent').parent().parent().parent().parent().height() - 5);
    //$('#ccParent').height($('#ccParent').parent().parent().parent().parent().height() - 16);
    //$('#panel-wizard').height($('#ccParent').height() - 10);

    $('#ccParent').height(CC.util.windowHeight() - 16);
    $('#panel-wizard').height($('#ccParent').height() - 10);
    $('#panel-wizard').width('100%');

    $('#panel-wizard').click(function () {
        CC.util.closePopup(100);
    });
    $('#pnlCallout').click(function (event) {
        CC.util.stopPropagation(event);
    });
    $(window).resize(function () {
        //This event is supported in IE9+ browsers
        CC.util.closePopup(100);
    });

    Ext.onReady(function () {

    });

    CC.util.toggleProgressBar(true);

    ////Initializes the Property Bag
    CC.med = CC.Mediator.getInstance();

    /* First request*/
    CC.med.loadAppProperty();

    //CC.Properties.prototype.init();
    //ExecuteOrDelayUntilScriptLoaded(CC.Properties.prototype.init, "sp.js");
    //SP.SOD.executeFunc('AppProperty.js', CC.Properties.prototype.init);
    //SP.SOD.executeFunc('AppProperty.js', 'SP.ClientContext', CC.Properties.prototype.init);
    //SP.SOD.executeOrDelayUntilScriptLoaded(CC.Properties.prototype.init, 'AppProperty.js');

    //CC.util.toggleProgressBar(true);


//    var clientContext = SP.ClientContext.get_current();
//    var web = clientContext.get_web();

//    var appInstance = SP.AppCatalog.getAppInstances(clientContext);
//    clientContext.load(appInstance);

//    clientContext.executeQueryAsync(
// function (args) {  alert("Success!") },
// function (args) { alert("Request failed") }
//);

});