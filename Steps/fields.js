
CC.Tabs.Axis = Ext.extend(Ext.Container, {
	create: function (idCntr, ibBtn) {
		btnId = [util.randomId(ibBtn), util.randomId(ibBtn), util.randomId(ibBtn)];		
		return {
            layout: 'column',
			id: idCntr,
            style: {
                'margin': '0px 3px 0px 0px',
				'border': 'none',
				'padding-top': '5px',
				'height': '22px'
            },
			border: false,
            items: [{
				xtype: 'combo'//,
				//editable: false
			}, {
				html: ' <input type="button" id="' + btnId[0] + '" class="yaxis1 yaxis" onclick="javascript:util.toggleCalloutPnl(event, this, 1)"/>',
				border: false,
				style: 'padding-left: 20px;'
			}, {
				html: ' <input id="' + btnId[1] + '" type="button" class="yaxis yaxis2" onclick="javascript:util.toggleCalloutPnl(event, this, 2)"/>',
				border: false //,
				//style: 'padding-left: 2px;'
			}, {
				html: ' <input id="' + btnId[2] + '" type="button" class="yaxis yaxis3" onclick="javascript:util.toggleCalloutPnl(event, this, 3)"/>',
				border: false,
				style: 'padding-left: 10px;'
			}]
        };
	}
});

CC.Tabs.Tab.Fields = Ext.extend(CC.Tabs.Base, {
    create: function () {
        
		this.config = {
			ChartIconsPath: 'assets/'
		};
		
		var axisCharts = [[[0, 'Primary Axis', 'primary-axis-light-13x9.png'],
				[1, 'Secondary Axis', 'secondary-axis-light-13x9.png']],
				[[0, 'Bar Chart', 'bar-chart-light-13x9.png'],
				[1, 'Area Chart', 'area-chart-light-13x9.png'],
				[2, 'Line Chart', 'line-chart-light-13x9.png']]];
				
		var TypeFields = ['id', 'name', 'img'];
		
		var axis_store = new Ext.data.ArrayStore({
			fields: TypeFields,
			data: axisCharts[0]
		});

		var chart_store = new Ext.data.ArrayStore({
			fields: TypeFields,
			data: axisCharts[1]
		});

		var chrtAxTemplate = new Ext.XTemplate(
			'<table cellspacing="0">',
			'<tpl for=".">',
				'<tr class="chrtAxTemplate" title="{name}">',
					'<td class="tab-fields-wrapLeft"><div style="width: 20px; height: 20px; background-repeat: no-repeat; background-position: center; background-image: url(\'' + this.config.ChartIconsPath + '{img}\');" /></td>',
					//'<div class="tab-fields-wrapLeft"><div class="x-form-item">{name}</div></div>',
					'<td class="tab-fields-wrapLeft x-form-item"><span style="">{name}</span></td>',
				'</tr>',
			'</tpl>',
			'</table>'
		);
		
		this.axisView = new Ext.DataView({
			id: 'axisView',
			store: axis_store,
			tpl: chrtAxTemplate,
			//itemSelector: 'div.cc-thumb-wrap',
			itemSelector: 'tr.chrtAxTemplate',
			overClass: 'chrtAxTemplate-over-noborder',
			singleSelect: true ,
			hidden: true,
			listeners: {
				selectionchange: this.switchAxisView,
				scope: this
			}
		});

		this.chartView = new Ext.DataView({
			id: 'chartView',
			store: chart_store,
			tpl: chrtAxTemplate,
			itemSelector: 'tr.chrtAxTemplate',
			//overClass: 'cc-x-view-over',
			overClass: 'chrtAxTemplate-over-noborder',
			autoScroll: false,
			singleSelect: true,
			hidden: true,
			listeners: {
		        selectionchange: this.switchChartView,
				scope: this
			}
		});
		
		this.confirmation = new Ext.Panel({
			id: 'pnlConfirmation',
			//title: 'Confirm',
			width: 125,
			layout: 'column',
			layoutConfig: { columns: 3 },
			hidden: true,
            items: [{
				xtype: 'label',
				cls: ' x-form-item',
				text: 'Are you sure?',
				colspan: 3,
				style: {
					'padding-bottom': '5px',
					'padding-top': '3px'
				}
			}, {
				colspan: 3,
				style:{
					'border-top': '1px solid #e5e5e5',
					'height': '10px',
					'width': '100%'
				}
			}, {
                html: '<button id="btnYes">Yes</button>',
				width: 60
            }, {
				html: '&nbsp;',
				width: 5
			}, {
				html: '<button id="btnNo">No</button>',
				width: 60
            }],
			listeners: {
				scope: this,
				afterlayout: function(){
					$("#btnNo").click(function(){
						util.closePopup(100);
					});
				}
			}
		});
		
		this.axis = new Ext.Panel({
			header: false,
			renderTo: 'pnlCallout',
			items: [this.axisView, this.chartView, this.confirmation]
			//items: [this.axisView, this.chartView]
		});
		
		var fields = new Ext.Container({
			id: 'cntrParent',			
			layout: 'vbox',
			layoutConfig: {
				align : 'stretch',
				pack  : 'start'
			},
			items:[
				{
					xtype: 'panel',
					autoScroll: true,
					style: {
						//'padding-left': '35px',
						'padding-left': '20px',
						'padding-top': '20px',
						'padding-bottom': '5px'						
					},
					border: false,
					items:[
					{
						xtype: 'label',
						text: 'X Axis',
						cls: ' x-form-item'
					}, {
						xtype: 'combo',
						id: 'fldXAxs',
						editable: false
					}, {
						xtype: 'label',
						text: 'Y Axis',
						cls: ' x-form-item',
						style: {
							'padding-top': '20px'
						}
					}, {
						xtype: 'container',
						id: 'cntrYAxis'
					}, {
						html: '<button class="linkBtn" onclick="javascript:util.addYAxis()">+ Add another Field</button>',
						style: 'margin-bottom: 5px;',
						border: false
					}
					// , {
						// xtype: 'label',
						// text: 'Z Axis',
						// cls: ' x-form-item',
						// style: {
							// 'padding-top': '20px'
						// }						
					// }, {
						// xtype: 'combo',
						// id: 'fldZAxs',
						// editable: false
					// }
					],
					listeners:{
						scope: this,
						afterrender: function(){							
							var obj = Ext.getCmp('cntrParent');
							obj.el.dom.firstChild.style.overflowY = 'auto';
						}
					}
				}
			],
			listeners:{
				scope: this,
				afterlayout: function(p, l){
					var ele = p.el.dom.children[0];
					//var ev = $._data(ele, 'events');
					
					//debugger;
					//Ensure that the event has not already been bound
					//if(!ev || !ev.scroll){
					if(!util.isEventBound(ele, "scroll")){
						$(p.el.dom.children[0]).scroll(function() {
							util.closePopup(100);
						});
					}
				}				
			}
		});
		
		util.addYAxis();
		
		return {
            id: 'tabFields',
			xtype: 'panel',
            title: 'FIELDS',
			header: false,
			iconCls: 'x-charts-icon',
            defaults: { autoScroll: false },
            items: [fields]
        }
    },
	
	switchAxisView: function(){
		//selrec = this.axisView.getSelectedRecords();
		
		this.swapBtnImages("yaxis1", this.config.ChartIconsPath + this.axisView.getSelectedRecords()[0].data.img)
	},
	
	switchChartView: function(){
		this.swapBtnImages("yaxis2", this.config.ChartIconsPath + this.chartView.getSelectedRecords()[0].data.img)
	},
	
	swapBtnImages: function(btnCls, imgPath){
		
		//get the top position of the Y-axis container
		var yTop = Math.round($('#cntrYAxis')[0].getBoundingClientRect().top);
		
		//get the rendered top position of the callout div. It will also get the surplus count like, padding, margin.
		var renderedCaloutTop = Math.round($('#pnlCallout')[0].getBoundingClientRect().top);
		
		//var pnlNo = util.getPanelNumber($('#cntrYAxis')[0].children[0].id);
		//var pnlPad = parseFloat($('#cntrYAxis')[0].children[0].style.paddingTop);
		//var btnHt = $("." + btnCls)[0].offsetHeight;
		//var caloutMrg = parseFloat($('#pnlCallout').css("marginTop"));
		
		//get only the surplus count of the callout div. margin, padding, & height of the triggered button
		var surplusCaloutTop = parseFloat($('#cntrYAxis')[0].children[0].style.paddingTop) + $("." + btnCls)[0].offsetHeight + parseFloat($('#pnlCallout').css("marginTop"));
		
		//get the row index of the item of the Y-axis container to which the callout div is pointing
		//var rowIndx = (caloutTop - (pnlPad + btnHt + caloutMrg) - yTop)/parseFloat($('#cntrYAxis')[0].children[0].offsetHeight);
		var rowIndx = (renderedCaloutTop - surplusCaloutTop - yTop)/parseFloat($('#cntrYAxis')[0].children[0].offsetHeight);
		
		//set the background image of the triggered button
		$(Ext.getCmp('cntrYAxis').items.itemAt(rowIndx).el.dom).find("." + btnCls).css({ "background-image": "url('"+ imgPath + "')" });
	},
	
	validationStep: function () {
        var valid = true;
        return valid;
    },
	
	activateStep: function () {
		this.disabled = false;
	}
});