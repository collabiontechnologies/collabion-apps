/*!
 * Ext JS Library 3.1.0
 * Copyright(c) 2006-2009 Ext JS, LLC
 * licensing@extjs.com
 * http://www.extjs.com/license
 */ 
Ext.ns('Ext.cc.form');

/**
 * @class Ext.cc.form.Toggle
 * @extends Ext.form.Checkbox
 * Creates a field utilizing new Office fabric UI Toggle css
 * @xtype toggle
 */
Ext.cc.form.ToggleField = Ext.extend(Ext.form.Checkbox, {
		
	/**
     * @cfg {Number} width
     * Applies a custom width to the component. If set to 0, will apply the default style width (defaults to
     * <code>'ms-Toggle-field'</code>).
     */
	width: 0,
	
	/**
     * @cfg {Store} style
     * Applies custom styles to the component. If set to default, the styles will be loaded from the css (defaults to
     * <code>'ms-Toggle-field'</code>).
     */
	style: undefined,
	
	/**
     * @cfg {Boolean} textLeft
     * Determines where the toggle description text will be declared. Top or Left (defaults to
     * <code>'false'</code>).
     */
	textLeft: false,
	
	/**
     * @cfg {String} textLeftCss
     * Private field. Use to specify the css the Toggle when the textLeft is set to true (defaults to
     * <code>' ms-Toggle--textLeft'</code>).
     */
	// private
	textLeftCss: ' ms-Toggle--textLeft',
	
	/**
     * @cfg {String} toggleCss
     * Private field. Use to specify the default css the Toggle irrespective of the state of the textLeft (defaults to
     * <code>'ms-Toggle'</code>).
     */
	// private
	toggleCss: 'ms-Toggle',
	
	/**
     * @cfg {String} boxLabelCss
     * Use to specify the default css the label/description of the Toggle. It's value can be replaced as per the requirement (defaults to
     * <code>'ms-Toggle-description'</code>).
     */
	boxLabelCss: 'ms-Toggle-description',
	
	/**
     * @cfg {String} toggleTxtCss
     * Use to specify the default css the On Off text that will be APPENDED to its exisitng css (defaults to
     * <code>''</code>).
     */
	toggleTxtCss: '',
	
	/**
     * @cfg {String} toggleTxtStyle
     * Use to specify the custom style for the On and Off text to be directly appended to the element (defaults to
     * <code>''</code>).
     */
	toggleTxtStyle: '',
	
	/**
     * Resets the checked property value of the control in align to the current state of the control. 'True' for 'On' and 'False' for 'Off'.
     * User can now query the checked property to get the control's current state.
	 * Also fires the toggle event of the parent container.
     */
	// private
    setValue : function(){
		var parent = Ext.getCmp(this.parentElement.children[0].id);
		
		//adjust the checked property to the latest state of the checkbox
		parent.checked = this.checked;
		
		//Fire the toggle event
		parent.fireEvent('toggle', parent);
    },
	
	/**
     * Overites the default onRender function to design the new Toggle control.
     */
	onRender : function(ct, position){
		//Ext.cc.form.Toggle.superclass.onRender.call(this, ct, position);
		
		//Create the parent div
		if(!this.el){
			if(Ext.isString(this.autoEl)){
                this.el = document.createElement(this.autoEl);
            }else{
                var div = document.createElement('div');
                Ext.DomHelper.overwrite(div, this.autoEl);
                this.el = div.firstChild;
            }
            if (!this.el.id) {
                this.el.id = this.getId();
            }
			if(this.el){
				this.el = Ext.get(this.el);
				if(this.allowDomMove !== false){
					ct.dom.insertBefore(this.el.dom, position);
					if (div) {
						Ext.removeNode(div);
						div = null;
					}
				}
			}
		}
		
		var tglCls = this.toggleCss + (this.textLeft ? this.textLeftCss : '');
		
		this.wrap = this.el.wrap({cls: tglCls});
		
		if(this.width > 0)
			this.wrap.setWidth(this.width);
		
		//Toggle description. Top or left
		var desc = this.wrap.createChild({
            tag: 'span',
            cls: this.boxLabelCss,
			html: this.boxLabel
        });
		
		var chkBxId = Ext.id();
		
		//Keep the reference of the checkbox alive for future reference
		this.checkbox = this.wrap.createChild({
            tag: 'input',
			type: 'checkbox',
            cls: 'ms-Toggle-input',
			id: chkBxId
        });
		
		if(this.checked)
			this.checkbox.dom.checked = true;
		
		
		var lbl = this.wrap.createChild({
            'tag': 'label',
            'cls': 'ms-Toggle-field',
			'for': chkBxId
        });
		
		var off = lbl.createChild({
				tag: 'span',
				cls: ('ms-Label ms-Label--off ' + this.toggleTxtCss),
				html: 'Off'
			});
			
		var on = lbl.createChild({
				tag: 'span',
				cls: ('ms-Label ms-Label--on ' + this.toggleTxtCss),
				html: 'On'
			});
		
		if(this.textLeft){
			
			//custom style to be applied to make the text appear on left
			
			off.dom.style.left = "-28px";
			on.dom.style.left = "-28px";
			
			off.dom.style.paddingLeft = "3px";
			on.dom.style.paddingLeft = "3px";
			off.dom.style.top = "-1px";
			on.dom.style.top = "-1px";
			
			//desc.dom.style.verticalAlign = "middle";
			desc.dom.style.verticalAlign = "top";
			desc.dom.style.paddingTop = "2px";
			
			if(this.toggleTxtStyle){
				off.setStyle(this.toggleTxtStyle);
				on.setStyle(this.toggleTxtStyle);
			}
		}
		
		if(this.style){
			//any custom style(s) passed in the config
			this.el.parent().setStyle(this.style);
		}
		                     
		/* appends a listener to the change event of the checkbox to keep 
		* the status of the checked property to latest */
		this.checkbox.dom.addEventListener("change", this.setValue);
		
		// var event = new MouseEvent('click', {
			// 'view': window,
			// 'bubbles': true,
			// 'cancelable': true
			// });
		// var canceled = !cb.dispatchEvent(event);
		// document.getElementById("checkbox").addEventListener("click", 
		// preventDef, false);
	
		// Create the event.
		//var event = document.createEvent('Event');

		// Define that the event name is 'build'.
		//event.initEvent('toggle', true, true);
		
		// Listen for the toggle event.
		this.el.dom.addEventListener('toggle', function (e) {
		  // e.target matches elem
		}, false);
	},
	
	/**
     * Gets the current state of the Toggle control. Alternatively, one can also use the checked property instead of this method to get the same result.
	 * @return {Boolean} Current state, true or false.
     */
	getValue : function(){
		if(this.rendered){
			return this.checkbox.dom.checked;
		}
		return this.checked;
	}
});

Ext.reg('toggleField', Ext.cc.form.ToggleField);

//backwards compat
//Ext.form.ToggleField = Ext.cc.form.Spinner;