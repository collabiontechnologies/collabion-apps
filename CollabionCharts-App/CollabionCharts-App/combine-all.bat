@echo off

::goto end

echo Copying CCSP_APP\JS\DATA\*.* ...
copy /Y "CCSP_APP\js\data\." "Scripts\data\" 

echo Copying CCSP_APP\JS\FUSIONCHARTS\*.* ...
xcopy /S /E /I /R /Y "CCSP_APP\js\fusioncharts\*.*" "Scripts\fusioncharts\."

echo Copying landing page assets
xcopy /S /E /I /R /Y "CCSP_APP\landingPage\." "Scripts\landingPage\"

if not %1=="Debug" (	
	echo Copying minified EXT-ALL.JS...
	copy /Y "CCSP_APP\ext\ext-all.js" "Scripts\ext\ext-all.js" 
	echo Copying minified EXT-BASE.JS...
	copy /Y "CCSP_APP\ext\adapter\ext\ext-base.js" "Scripts\ext\adapter\ext\ext-base.js" 	
) else ( 
	echo Copying debug version of EXT-ALL.JS...
	copy /Y "CCSP_APP\ext\ext-all-debug.js" "Scripts\ext\ext-all.js" 
	echo Copying debug version of EXT-BASE.JS...
	copy /Y "CCSP_APP\ext\adapter\ext\ext-base-debug.js" "Scripts\ext\adapter\ext\ext-base.js" 
)

echo Copying EXT\RESOURCES\.
XCOPY /S /E /I /R /Y "CCSP_APP\ext\resources\*.*" "Scripts\ext\resources\."


REM Joins all Plugin JS files
copy /B /Y "CCSP_APP\ext\plugins\Spinner.js" +nl+ "CCSP_APP\ext\plugins\SpinnerField.js" +nl+ "CCSP_APP\ext\plugins\jquery.tooltipster.js" +nl+ "CCSP_APP\ext\plugins\jquery.hoverscroll.js" "Scripts\ext\plugins\plugins-debug.js" >nul

if not %1=="Debug" ( echo Compiling Scripts\ext\plugins\plugins-debug.js...
java -jar "CCSP_APP\compiler\yuicompressor-2.4.7.jar" "Scripts\ext\plugins\plugins-debug.js" -o "Scripts\ext\plugins\plugins.js"
) else ( 
echo Copying plugins-debug.js...
copy /y "Scripts\ext\plugins\plugins-debug.js" "Scripts\ext\plugins\plugins.js"
)


REM Joins all JS for UI
copy /B /Y "CCSP_APP\js\steps\global.js" +nl+ "CCSP_APP\js\core.js" +nl+ "CCSP_APP\js\steps\data.js" +nl+ "CCSP_APP\js\steps\chart.js"  +nl+ "CCSP_APP\js\steps\label.js"  +nl+ "CCSP_APP\js\steps\number.js"  +nl+ "CCSP_APP\js\steps\tabs.js"  +nl+ "CCSP_APP\js\steps\active\active-data.js"  +nl+ "CCSP_APP\js\steps\active\active-fields.js"  +nl+ "CCSP_APP\js\steps\active\active-charts.js"  +nl+ "CCSP_APP\js\steps\active\active-labels.js"  +nl+ "CCSP_APP\js\steps\active\active-numbers.js"  +nl+ "CCSP_APP\js\mediator.js"  +nl+ "CCSP_APP\js\setting.js"  +nl+ "CCSP_APP\js\spList.js"  +nl+ "CCSP_APP\js\chartBuilder.js"  +nl+ "CCSP_APP\js\wizard.js"  +nl+ "CCSP_APP\js\extOverrides.js"  +nl+ "CCSP_APP\js\onReady.js"  +nl+ "CCSP_APP\js\analytics\track.js" "Scripts\ui\ccsp-ui-debug.js"  >nul

if not %1=="Debug" ( echo Compiling Scripts\ui\ccsp-ui-debug.js...
java -jar "CCSP_APP\compiler\yuicompressor-2.4.7.jar" "Scripts\ui\ccsp-ui-debug.js" -o "Scripts\ui\ccsp-ui.js"
java -jar "CCSP_APP\compiler\yuicompressor-2.4.7.jar" "Scripts\landingPage\theme.js" -o "Scripts\landingPage\theme.js"

echo Minifing CSS...
for /R "Scripts\ext\resources\css\" %%a in (*.css) do (
echo 	%%~na%%~xa
java -jar "CCSP_APP\compiler\yuicompressor-2.4.7.jar" --type css "%%a" -o "%%a"
)

) else ( 
echo Copying ccsp-ui-debug.js...
copy /y "Scripts\ui\ccsp-ui-debug.js" "Scripts\ui\ccsp-ui.js"
)

:end
