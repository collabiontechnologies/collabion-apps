//
// Utility methods
//
CC.util = function(){
	idCnt = 0;
	
	this.tipCls = 'tooltip';
	
	//
    // Merge array objects to one
    // objs - array objects
    //
    this.merge = function (objs) {
        var obj = {};
        for (var i = 0; i < objs.length; i++) {
            for (var p in objs[i]) obj[p] = objs[i][p];
        }
        return obj;
    },
	
	//
    // Retrieve new array based on source array
    // source - source array
    // mobj - mobj can contains add and del arrays
    //		add - array of items that must be added
    //		del - array of items that must be removed
    //
    this.modify = function (source, mobj) {
        var dest = [];
        for (var i = 0; i < source.length; i++) dest[i] = source[i];
        if (mobj) {
            for (var i = 0; mobj.del && i < mobj.del.length; i++) {
                dest.remove(mobj.del[i])
            }
            for (var i = 0; mobj.add && i < mobj.add.length; i++) {
                dest.push(mobj.add[i]);
            }
        }
        return dest;
    },
	
	//
	//Prevents the event chain prpagation both in IE and other browsers
	//
	this.stopPropagation = function(event){
		//event.stopPropagation();
		
		if (!event) var event = window.event
		// handle event
		event.cancelBubble = true; //for IE
		if (event.stopPropagation) event.stopPropagation(); //for other browsers
	},
	
	//
	//returns a unique integer on each call starting from zero
	//
	this.getUniqueId = function(){
		return ++idCnt;
	},
	
	this.addYAxis = function(){
		var rowCnt = Ext.id().replace("ext-gen", "");
		
		//var axisPanel = new CC.Tabs.Axis;
		var axisPanel = new CC.Tabs.Tab.Data.DataFields.Axis;
		if (Ext.getCmp('cntrYAxis').items != undefined)
			Ext.getCmp('cntrYAxis').insert(Ext.getCmp('cntrYAxis').items.length, axisPanel.create((rowCnt + '-pnl'), (rowCnt + '-btn')));
		else
			Ext.getCmp('cntrYAxis').insert(0, axisPanel.create((rowCnt + '-pnl'), (rowCnt + '-btn')));
			
		Ext.getCmp('cntrYAxis').doLayout();
		//Ext.getCmp('cntrParent').doLayout();
	},

	this.toggleCalloutPnl = function(event, sender, btnCount){
		var rect = sender.getBoundingClientRect();
		var pnl = $("#pnlCallout");
		//pnl.css('left', ((rect.left - 20) + "px")) //.left = rect.left - 20;
		//pnl.css('top',  (rect.bottom + "px"));
		//pnl.css('display', 'block');
		
		
		// var _left = ((rect.left - 20) + "px");
		// var _top = (rect.bottom + "px");
		
		
		//Takes care of the scroll bar position as well, in both, IE and non-IE browsers
		var _top = (document.documentElement && document.documentElement.scrollTop) || 
					document.body.scrollTop;
		var _left = (document.documentElement && document.documentElement.scrollLeft) || 
					document.body.scrollLeft;
		
		// pnl.animate({left: ((rect.left - 20) + "px")}, 400, function(){
		   // pnl.animate({top: (rect.bottom + "px")}, 400);
		// });
		
		//20 is deducted because the position of the arrow is 20 px left of the callout
		pnl.animate({
			left: ((_left + (rect.left - 20)) + "px"),
			top: ((_top + rect.bottom) + "px")
		}, 400);
		
		switch(btnCount){
			case 1:
				Ext.getCmp('axisView').show();
				Ext.getCmp('chartView').hide();
				Ext.getCmp('pnlConfirmation').hide();
				break;
			case 2:
				Ext.getCmp('chartView').show();
				Ext.getCmp('axisView').hide();
				Ext.getCmp('pnlConfirmation').hide();
				break;
			case 3:
				Ext.getCmp('chartView').hide();
				Ext.getCmp('axisView').hide();
				Ext.getCmp('pnlConfirmation').show();
				this.deleteRow(sender);
				break;
		}
		
		pnl.fadeIn(50);
		
		util.stopPropagation(event);
		
		//get the index of the item of the Y container for future use
		//console.log("Index of the clicked item " + util.findRowIndexofY(sender.id));
		console.log("Index of the clicked item " + util.getContainerItemIndex('cntrYAxis', util.getContainerItem('cntrYAxis', util.yPanelId(sender.id))));
	},

	this.deleteRow = function(sender){
		$('#btnYes').off('click');
		$("#btnYes").click(function(){
			util.closePopup('fast');
			//Ext.getCmp('cntrYAxis').remove(sender.id.split('-')[0] + '-pnl');
			//Ext.getCmp('cntrYAxis').remove(util.getPanelNumber(sender.id) + '-pnl');
			Ext.getCmp('cntrYAxis').remove(util.yPanelId(sender.id));
			Ext.getCmp('cntrYAxis').doLayout();	
		});
	},
	
	this.yPanelId = function(id){
		return util.getPanelNumber(id) + '-pnl'
	},
	
	this.getPanelNumber = function(id){
		return id.split('-')[0];
	},

	this.randomId = function(initials){
		var id = this.getUniqueId();
		if(initials)
			id = initials + id;
		return id;
	},

	this.closePopup = function(speed) {
		//$("#pnlCallout").fadeOut(isFast?"fast":"slow");
		$("#pnlCallout").fadeOut(speed ? speed : "slow");
	},
	
	this.isEventBound = function(ele, event){
		var ev = $._data(ele, 'events');
		return (ev && ev[event]);
	},
	
	this.getContainerItemIndex = function(cntrId, item){
		return Ext.getCmp(cntrId).items.indexOf(item);
	},
	
	this.getContainerItem = function(cntrId, itemId){
		return Ext.getCmp(cntrId).items.item(itemId);
	},
	
	this.generateTip = function(){
		$('.' + this.tipCls).tooltipster({
			theme: 'tooltipster-shadow', 
			onlyOne: true
		});
	}
	
};

util = new CC.util();