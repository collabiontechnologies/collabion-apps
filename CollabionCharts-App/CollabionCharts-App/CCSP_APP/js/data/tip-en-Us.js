﻿/*
*
* Wizard tooltip definitions
*
*/

CC.tips = {
    /* TABS */

    /* Data Source */

    /* Data Fields */
    df131: 'N/A for single series',
    df132: 'N/A for single series',
    df133: 'N/A for single series',
    df134: 'Select the Y-Axis scale (Primary or Secondary) to which the series is to be attached.',
    df135: 'Render the data series as any one the dataplot types - <b>Bar, Area</b> or <b>Line</b>.',
    df136: 'Remove this series.',

    /* Charts */

    /* Labels */
    lbl151: 'Configure the chart cosmetics',

    /* Numbers */
    nmbr161: 'Specify number prefix and suffix (<i>e.g., $100K p.a.</i>)',
    nmbr162: 'Specify decimal places',
    nmbr163: 'Format numbers using thousand and decimal separators',
    nmbr164: 'Scale down numbers using known identifiers (<i>e.g., 100K</i>)',
}