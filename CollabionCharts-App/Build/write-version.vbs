option explicit
dim args, sFilePath, AppVersion, searchText, newStr

AppVersion = "1.0.0.0"

set args = WScript.Arguments

sFilePath=args(0)

call ReadVersionFromFile()

sub ReadVersionFromFile()
	
	dim fs,tfile
	set fs=CreateObject("Scripting.FileSystemObject")
	set tfile=fs.OpenTextFile("AppVersion")
	AppVersion = tfile.ReadLine()
	tfile.close
	set tfile=nothing
	set fs=nothing	

end sub

call WriteVersionToManifest(sFilePath)

searchText = "this.version ="
sFilePath = "..\CollabionCharts-App\CCSP_APP\js\analytics\track.js"
newStr = " '" & AppVersion & "';"

call WriteVersionToJS(sFilePath, searchText, newStr)


sub WriteVersionToManifest(sFilePath)
	dim oxml, xRoot, node, oAttribute
	
	set oxml = CreateObject("Msxml2.DOMDocument.6.0")
	
	call oxml.load(sFilePath)
	
	set xRoot=oxml.documentElement
	call xroot.setAttribute("Version", AppVersion)
	
	if Err.Number <> 0 Then
		WScript.Quit(errorNumber)
	end if	

	call oxml.save(sFilePath)
	
	set oxml = nothing
	set xRoot = nothing
	
end sub


Sub WriteVersionToJS(strFileName, strToSearch, strNewText)
	Const ForReading = 1    
	Const ForWriting = 2
	Dim objFSO, objFile, sText, sAllText, Position
	Set objFSO = CreateObject("Scripting.FileSystemObject")
	Set objFile = objFSO.OpenTextFile(strFileName, ForReading)
	
	sText = ""
    sAllText = ""
    Position = 0
	
	While (Not objFile.AtEndOfStream)
        sText = objFile.ReadLine() + vbCrLf
		'If (InStr(1,strToSearch,"<script ") > 1 ) Then
			Position = InStr(1, sText, strToSearch, vbTextCompare)
			If Position > 1 Then
				sText = Mid(sText, 1, Position - 1) & strToSearch + strNewText + vbCrLf
			End If		
		'Else
		'	Position = InStr(1, sText, strToSearch, vbTextCompare)
		'	If Position > 1 Then
		'		sText = Mid(sText, 1, Position - 1) & strToSearch + strNewText + vbCrLf
		'	End If		
		'End If
        sAllText = sAllText & sText
    Wend

	objFile.Close

	Set objFile = objFSO.OpenTextFile(strFileName, ForWriting)
	objFile.Write sAllText  'WriteLine adds extra CR/LF
	objFile.Close
End Sub