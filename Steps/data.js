CC.Tabs.Tab.Data = Ext.extend(CC.Tabs.Base, {
    //config: 0,
	//currentStep: 0,
	
	create: function () {
		// Create wizard steps
        var pnlInfo = this.createSteps([
			{ step: 'Configurations', leaf: [new CC.Tabs.Tab.Data.DataSource, 
											 new CC.Tabs.Tab.Data.DataFields] 
			}
		]);
        this.steps = pnlInfo.steps;

        // Content panel for steps
        this.contentPan = new Ext.Panel({
            id: 'content-panel',
            activeItem: CC.Wizard.tabpanel.currentStep,
            border: false,
            region: 'center',
            layout: 'card',
			autoScroll: false,
            defaults: {
                border: false,
                farme: true
            },
            items: pnlInfo.panels
        });

		this.level = 1;
        CC.Wizard.tabpanel.config.level = 0;
		
        // Navigation tree panel
        this.navigationPan = new Ext.tree.TreePanel({
            region: 'west',
            width: 185,
			id: 'tpDataSource',
            split: true,
            border: false,
            autoScroll: true,
            expanded: true,
            lines: false,
            useArrows: true,
            rootVisible: false,
            bodyStyle: { backgroundColor: '#FFFFFF', paddingTop: '10px' },
            root: new Ext.tree.AsyncTreeNode(pnlInfo.nodes),
            listeners: {
                scope: this,
                append: function (tree, parent, node) {					
                    if (node.id == CC.Wizard.tabpanel.currentStep.toString()) {
                        node.select.defer(100, node);
                    }
                },
                afterrender: function (s) {
                    //this.setVisibility(1);
					//this.steps[0].setVisibility(0);
					//debugger;
					this.setVisibility(this.level);
                },
                click: this.treeBeforeDeactive
            }
        });
		
		
		
		//old code
        return {
            id: 'tabData',
            title: 'DATA',
			iconCls: 'x-data-icon',
			header: false,
			region: 'center',
			split: true,
			layout: 'border',
            items: [this.navigationPan, this.contentPan]
        }
    },
	
	// Private: Generating wizard's steps
    createSteps: function (steps) {
        var pnl = [], stp = [];
        var nds = { expanded: true, children: [] };
        for (var i = 0; i < steps.length; i++) {
			
			//debugger;
			
            var len = pnl.length.toString();
            var n = { leaf: true, disabled: true }
			if (steps[i].step instanceof Object) {
                //var p = this.createWizardPanel(steps[i].step);
                var p = CC.Wizard.tabpanel.createTabPanelItem(steps[i].step);
                //var p = steps[i].step.create();
                p.name = len + '-step';
                n.id = len;
                n.text = p.caption || p.title;
                stp.push(steps[i].step);
                pnl.push(p);
            }
            else {
                n.id = (-(i + 1)).toString();
                n.text = steps[i].step;
                n.disabled = false;
            }
            if (steps[i].leaf) {
			
                n.leaf = false;
                n.expanded = true;
                n.children = [];
                for (var k = 0; k < steps[i].leaf.length; k++) {
                    len = pnl.length.toString();
                    //p = this.createWizardPanel(steps[i].leaf[k]);
                    p = CC.Wizard.tabpanel.createTabPanelItem(steps[i].leaf[k]);
                    //p = steps[i].leaf[k].create();
                    p.name = len + '-step';
                    n.children[k] = { id: len, text: p.caption || p.title, leaf: true, disabled: true }
                    stp.push(steps[i].leaf[k]);
                    pnl.push(p);
                }
            }			
            nds.children.push(n);
        }
        stp[0].level = 1;
		
		var treeNodes = { steps: stp, panels: pnl, nodes: nds };
		//Hide leaf node
		treeNodes.nodes = treeNodes.nodes.children[0];	
        return treeNodes; 
    },
	
    // Set steps visibility
    // level - visibility level
    setVisibility: function (level) {
        if (CC.Wizard.tabpanel.config.level === level) return;
        CC.Wizard.tabpanel.config.level = level;
		
		//debugger;
		
        for (var i = 0; i < this.steps.length; i++) {
            var node = this.navigationPan.getNodeById(i);
			//this.navigationPan.getNodeById(i)[this.steps[i].level <= level ? 'enable' : 'disable']();
			node[this.steps[i].level <= level ? 'enable' : 'disable']();		  	
			node.getUI()[this.steps[i].level <= level ? 'show' : 'hide']();
        }
        
    },	
	
	// Tree navigation
    treeBeforeDeactive: function (n) {
        //debugger;
		var sn = this.navigationPan.selModel.selNode || {};
        if (n.id != sn.id) {
            var next = parseInt(n.id);
            //check if moving to the previous step from 'select fields' is true 
            if (this.currentStep == 1 && next == 0) {
                this.steps[this.currentStep].isBackToSelectSource = true;
            }

            if (next < 0 || !this.step_onValidation(this.currentStep)) {
                return false;
            }
            //if (this.steps[this.currentStep].confirmation) {
            //    this.confirm(this.treeDeactive, this.steps[this.currentStep].confirmationMsg, next);
            //}
            //else {
                this.treeDeactive(next)
            //}
        }
        return false;
    },

    treeDeactive: function (next) {
        this.step_onDeactive(CC.Wizard.tabpanel.currentStep);
		this.treeActivateNewStep(next);
    },

    treeActivateNewStep: function (next) {
        this.currentStep = next;
        this.contentPan.layout.setActiveItem(next);
        this.step_onActive(next);
        this.navigationPan.getNodeById(next).select();
    },

    // Called when user comes to step. Need activateStep method into step object
    step_onActive: function (index) {
        this.steps[index].activateStep();
    },

    // Called when user leaves step. Need deactivate step method into step object
    step_onDeactive: function (index) {
		this.steps[index].deactivateStep();
    },

    // Validation step. Need validationStep method into step object
    step_onValidation: function (index) {
        //return this.steps[index].validationStep();
        return true;
    },
	
	validationStep: function () {        
        return true;
    },
	
	activateStep: function () {
		this.disabled = false;
	}
});

CC.Tabs.Tab.Data.DataSource = Ext.extend(CC.Tabs.Base, {
	create: function(){
		var dsContent = {
			collapsible: false,
			region: 'center',
			border: false,
			autoScroll: false,
			items: [{
				xtype: 'panel',
				id: 'pnlDataSource',
				//labelWidth: 74,
				border: false,
				layout: 'table',
				layoutConfig: { columns: 2 },
				autoScroll: false,
				items: [{
					xtype: 'label',
					id: 'lblDataSource',
					cls: 'subHead',
					text: 'Select Data Source',
					colspan: 2
				},{
					xtype: 'label',
					text: 'Select List',
					cls: ' x-form-item'
				},{
					id: 'selectlist',
					xtype: 'combo',
					//width: 200,
					//listWidth: 200,
					ccTip: '123456',
					fieldLabel: 'Select List',
					editable: false
				},{
					xtype: 'label',
					text: 'Select View',
					cls: ' x-form-item'
				},{
					id: 'selectview',
					xtype: 'combo',
					//width: 200,
					//listWidth: 200,
					fieldLabel: 'Select View',
					editable: false
				},{
					id: 'connectsplist',
					xtype: 'button',
					text: 'Connect',
					handler: this.connectSPList,
					style: 'padding-top:0px;padding-left:105px;margin-bottom:5px;width: 100px;',
					colspan: 2
				}]
			}]
		};
		
		return {
			id: 'DataSource',
			title: 'Data Source',
			header: false,
			defaults: { autoScroll: false },
			items: [dsContent],
			tools: [{
				id: 'help'//,
				//handler: CC.Data.Var.step_onHelpClick
			}]
		};
	},
	
	connectSPList: function() {
	
		for (var k = 0; k < CC.Wizard.tabpanel.steps.length; k++) {
			CC.Wizard.tabpanel.steps[k].setDisabled(false);
			//CC.Wizard.tabpanel.items.items[k].setDisabled(false);
		}
		
		//debugger;
		//this.setVisibility(3);
		CC.Wizard.tabpanel.tabs[0].setVisibility(3, this.level);
	}
});

CC.Tabs.Tab.Data.DataFields = Ext.extend(CC.Tabs.Base, {
    create: function () {
        
		this.config = {
			ChartIconsPath: 'assets/'
		};
		
		var axisCharts = [[[0, 'Primary Axis', 'primary-axis-light-13x9.png'],
				[1, 'Secondary Axis', 'secondary-axis-light-13x9.png']],
				[[0, 'Bar Chart', 'bar-chart-light-13x9.png'],
				[1, 'Area Chart', 'area-chart-light-13x9.png'],
				[2, 'Line Chart', 'line-chart-light-13x9.png']]];
				
		var TypeFields = ['id', 'name', 'img'];
		
		var axis_store = new Ext.data.ArrayStore({
			fields: TypeFields,
			data: axisCharts[0]
		});

		var chart_store = new Ext.data.ArrayStore({
			fields: TypeFields,
			data: axisCharts[1]
		});

		var chrtAxTemplate = new Ext.XTemplate(
			'<table cellspacing="0">',
			'<tpl for=".">',
				'<tr class="chrtAxTemplate tooltip" title="{name}">',
					'<td class="tab-fields-wrapLeft"><div style="width: 20px; height: 20px; background-repeat: no-repeat; background-position: center; background-image: url(\'' + this.config.ChartIconsPath + '{img}\');" /></td>',
					//'<div class="tab-fields-wrapLeft"><div class="x-form-item">{name}</div></div>',
					'<td style="margin-top: 1px; margin-right: 3px;" class="tab-fields-wrapLeft x-form-item"><span style="">{name}</span></td>',
				'</tr>',
			'</tpl>',
			'</table>'
		);
		
		this.axisView = new Ext.DataView({
			id: 'axisView',
			store: axis_store,
			tpl: chrtAxTemplate,
			//itemSelector: 'div.cc-thumb-wrap',
			itemSelector: 'tr.chrtAxTemplate',
			overClass: 'chrtAxTemplate-over-noborder',
			singleSelect: true ,
			hidden: true,
			listeners: {
				selectionchange: this.switchAxisView,
				scope: this
			}
		});

		this.chartView = new Ext.DataView({
			id: 'chartView',
			store: chart_store,
			tpl: chrtAxTemplate,
			itemSelector: 'tr.chrtAxTemplate',
			//overClass: 'cc-x-view-over',
			overClass: 'chrtAxTemplate-over-noborder',
			autoScroll: false,
			singleSelect: true,
			hidden: true,
			listeners: {
		        selectionchange: this.switchChartView,
				scope: this
			}
		});
		
		this.confirmation = new Ext.Panel({
			id: 'pnlConfirmation',
			//title: 'Confirm',
			width: 125,
			layout: 'column',
			layoutConfig: { columns: 3 },
			hidden: true,
            items: [{
				xtype: 'label',
				cls: ' x-form-item',
				text: 'Are you sure?',
				colspan: 3,
				style: {
					'padding-bottom': '5px',
					'padding-top': '3px'
				}
			}, {
				colspan: 3,
				style:{
					'border-top': '1px solid #e5e5e5',
					'height': '10px',
					'width': '100%'
				}
			}, {
                html: '<button id="btnYes">Yes</button>',
				width: 60
            }, {
				html: '&nbsp;',
				width: 5
			}, {
				html: '<button id="btnNo">No</button>',
				width: 60
            }],
			listeners: {
				scope: this,
				afterlayout: function(){
					$("#btnNo").click(function(){
						util.closePopup(100);
					});
				}
			}
		});
		
		this.axis = new Ext.Panel({
			header: false,
			renderTo: 'pnlCallout',
			items: [this.axisView, this.chartView, this.confirmation]
			//items: [this.axisView, this.chartView]
		});
		
		var fields = new Ext.Container({
			id: 'cntrParent',			
			layout: 'vbox',
			layoutConfig: {
				align : 'stretch',
				pack  : 'start'
			},
			items:[
				{
					xtype: 'panel',
					autoScroll: true,
					style: {
						//'padding-left': '35px',
						'padding-left': '20px',
						'padding-top': '20px',
						'padding-bottom': '5px'						
					},
					border: false,
					items:[
					{
						xtype: 'label',
						text: 'X Axis',
						cls: ' x-form-item'
					}, {
						xtype: 'combo',
						id: 'fldXAxs',
						ccTip: '1234567890',
						editable: false
					}, {
						xtype: 'label',
						text: 'Y Axis',
						cls: ' x-form-item',
						style: {
							'padding-top': '20px'
						}
					}, {
						xtype: 'container',
						id: 'cntrYAxis'
					}, {
						html: '<button class="linkBtn" onclick="javascript:util.addYAxis()">+ Add another Field</button>',
						style: 'margin-bottom: 5px;',
						border: false
					}
					],
					listeners:{
						scope: this,
						afterrender: function(){							
							var obj = Ext.getCmp('cntrParent');
							obj.el.dom.firstChild.style.overflowY = 'auto';
						}
					}
				}
			],
			listeners:{
				scope: this,
				afterlayout: function(p, l){
					var ele = p.el.dom.children[0];
					//var ev = $._data(ele, 'events');
					
					//debugger;
					//Ensure that the event has not already been bound
					//if(!ev || !ev.scroll){
					if(!util.isEventBound(ele, "scroll")){
						$(p.el.dom.children[0]).scroll(function() {
							util.closePopup(100);
						});
					}
				}				
			}
		});
		
		util.addYAxis();
		
		//debugger;
		
		return {
            id: 'tabFields',
			xtype: 'panel',
            title: 'Data Fields',
			header: false,
			iconCls: 'x-charts-icon',
            defaults: { autoScroll: false },
            items: [fields]
        }
    },
	
	switchAxisView: function(){
		//selrec = this.axisView.getSelectedRecords();
		
		this.swapBtnImages("yaxis1", this.config.ChartIconsPath + this.axisView.getSelectedRecords()[0].data.img)
	},
	
	switchChartView: function(){
		this.swapBtnImages("yaxis2", this.config.ChartIconsPath + this.chartView.getSelectedRecords()[0].data.img)
	},
	
	swapBtnImages: function(btnCls, imgPath){
		
		//get the top position of the Y-axis container
		var yTop = Math.round($('#cntrYAxis')[0].getBoundingClientRect().top);
		
		//get the rendered top position of the callout div. It will also get the surplus count like, padding, margin.
		var renderedCaloutTop = Math.round($('#pnlCallout')[0].getBoundingClientRect().top);
		
		//var pnlNo = util.getPanelNumber($('#cntrYAxis')[0].children[0].id);
		//var pnlPad = parseFloat($('#cntrYAxis')[0].children[0].style.paddingTop);
		//var btnHt = $("." + btnCls)[0].offsetHeight;
		//var caloutMrg = parseFloat($('#pnlCallout').css("marginTop"));
		
		//get only the surplus count of the callout div. margin, padding, & height of the triggered button
		var surplusCaloutTop = parseFloat($('#cntrYAxis')[0].children[0].style.paddingTop) + $("." + btnCls)[0].offsetHeight + parseFloat($('#pnlCallout').css("marginTop"));
		
		//get the row index of the item of the Y-axis container to which the callout div is pointing
		//var rowIndx = (caloutTop - (pnlPad + btnHt + caloutMrg) - yTop)/parseFloat($('#cntrYAxis')[0].children[0].offsetHeight);
		var rowIndx = (renderedCaloutTop - surplusCaloutTop - yTop)/parseFloat($('#cntrYAxis')[0].children[0].offsetHeight);
		
		//set the background image of the triggered button
		$(Ext.getCmp('cntrYAxis').items.itemAt(rowIndx).el.dom).find("." + btnCls).css({ "background-image": "url('"+ imgPath + "')" });
	},
	
	validationStep: function () {
        var valid = true;
        return valid;
    },
	
	activateStep: function () {
		this.disabled = false;
	}
});

CC.Tabs.Tab.Data.DataFields.Axis = Ext.extend(Ext.Container, {
	create: function (idCntr, ibBtn) {
		btnId = [util.randomId(ibBtn), util.randomId(ibBtn), util.randomId(ibBtn)];		
		return {
            layout: 'column',
			id: idCntr,
            style: {
                'margin': '0px 3px 0px 0px',
				'border': 'none',
				'padding-top': '5px',
				'height': '22px'
            },
			border: false,
            items: [{
				xtype: 'combo'//,
				//editable: false
			}, {
				html: ' <input type="button" id="' + btnId[0] + '" class="yaxis1 yaxis" onclick="javascript:util.toggleCalloutPnl(event, this, 1)"/>',
				border: false,
				style: 'padding-left: 20px;'
			}, {
				html: ' <input id="' + btnId[1] + '" type="button" class="yaxis yaxis2" onclick="javascript:util.toggleCalloutPnl(event, this, 2)"/>',
				border: false //,
				//style: 'padding-left: 2px;'
			}, {
				html: ' <input id="' + btnId[2] + '" type="button" class="yaxis yaxis3" onclick="javascript:util.toggleCalloutPnl(event, this, 3)"/>',
				border: false,
				style: 'padding-left: 10px;'
			}]
        };
	}
});