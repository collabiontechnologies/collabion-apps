// Namespaces
Ext.ns('CC');
Ext.ns('CC.Tabs.Tab');

CC.Tabs.Base = function () {
};

CC.Tabs.Base.prototype = {
	// Level of wizard steps visibility
    // 1 - only first step;
    // 2 - select source and select fields steps;
    // 3 - all steps.
    level: 3,


    // Indicate if user press finish button.
    isFinish: false,

    //Confirmation message
    confirmationMsg: "",

    // If confirmation is true before leaving wizard step show confirmation message box.
    confirmation: false,

    isTitleChanged: false,

    // Create new step. Return new panel for wizard steps.
    create: function () { return new Ext.Panel({ title: 'BaseStep' }) },

    // This method is called after the transition to a next wizard step.
    activateStep: Ext.emptyFn,

    // This method is called before leaving step on the wizard.
    deactivateStep: Ext.emptyFn,

    // This method is called after user click any button on the confirmation message box.
    // If method return true then wizard open next step.
    confirmStep: function () { return true },

    // Validate step. Called before deactivateStep. Returns true or false.
    validationStep: function () { return true },

    // This method returns active tab index of main tab panel of step for open documentation. Or returns 0, when no tab panel at the step.
    getCurrentTabIndex: function () { return 0; }
};
