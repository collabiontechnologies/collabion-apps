﻿/*
*
* Wizard message definitions
*
*/


CC.msgs = {
    /* Common */


    /* TABS */
    tb111: 'DATA',
    tb112: 'CHARTS',
    tb113: 'LABELS',
    tb114: 'NUMBERS',

    /* Data Source */
    ds121: 'Select Data Source',
    ds122: 'Select List',
    ds123: 'Select View',
    ds124: 'Data Source',


    /* Data Fields */
    df131: 'Yes',
    df132: 'No',
    df133: 'X Axis',
    df134: 'Y Axis',
    df135: '+ Add another Data Series',
    df136: 'Data Fields',

    /* Charts */


    /* Labels */
    lbl151: 'Data labels',
    lbl152: 'Data values',
    lbl153: 'Chart legend',
    lbl154: 'Chart Title',
    lbl155: 'Chart Sub-title',
    lbl156: 'X Axis Title',
    lbl157: 'Y Axis Title',
    lbl158: 'Secondary Y Axis Title',

    /* Numbers */
    nmbr161: 'Prefix and Suffix',
    nmbr162: 'Prefix',
    nmbr163: 'Suffix',
    nmbr164: 'Rounding Numbers',
    nmbr165: 'No. of decimal places',
    nmbr166: 'Number Format',
    nmbr167: 'Regular <i class="x-item-disabled"> (e.g., 100,213.45)</i>',
    nmbr168: 'European <i class="x-item-disabled"> (e.g., 100.213,45)</i>',
    nmbr169: 'Scaling Numbers',
    nmbr1610: 'Scales to be used',
    nmbr1611: 'Values of each scale',

    /* Wizard */
    wzrd171: 'Theme',
    wzrd172: 'DONE',
    wzrd173: 'Edit Chart',
    wzrd174: ' Working on it...',
    wzrd175: 'This is a preview.',

    /* Error */
    err181: "Data doesn't have any value with numbers.",
    err182: "No data to display.",
    err183: "Unable to render chart with the current configurations."
}