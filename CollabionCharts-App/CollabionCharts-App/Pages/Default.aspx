﻿<%-- The following 4 lines are ASP.NET directives needed when using SharePoint components --%>

<%@ Page Inherits="Microsoft.SharePoint.WebPartPages.WebPartPage, Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" MasterPageFile="~masterurl/default.master" Language="C#" meta:progid="SharePoint.WebPartPage.Document" %>

<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>

<%-- The markup and script in the following Content element will be placed in the <head>
<meta name="WebPartPageExpansion" content="full" />
<meta name="WebPartPageExpansion" content="full" />
<meta name="WebPartPageExpansion" content="full" />
<meta name="WebPartPageExpansion" content="full" /> of the page --%>
<asp:Content ContentPlaceHolderID="PlaceHolderAdditionalPageHead" runat="server">
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">

    <link href="../Scripts/ext/resources/css/landingPage/owl.carousel.css" rel="stylesheet" />
    <link href="../Scripts/ext/resources/css/landingPage/theme.css" rel="stylesheet" />
    <link href="../Scripts/ext/resources/css/landingPage/themecoll.css" rel="stylesheet" />

<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->

    <script type="text/javascript" src="../Scripts/jquery-1.9.1.min.js"></script>
</asp:Content>

<%-- The markup in the following Content element will be placed in the TitleArea of the page --%>
<asp:Content ContentPlaceHolderID="PlaceHolderPageTitleInTitleArea" runat="server">
	<title>Landing Page</title>
</asp:Content>

<%-- The markup and script in the following Content element will be placed in the <body> of the page --%>
<asp:Content ContentPlaceHolderID="PlaceHolderMain" runat="server">

    <div class="wrapper">
<header>
    <nav class="navbar navbar-default navbar-fixed-top" >
  <div class="container">
    <div class="navbar-header">
      	<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      	</button>
      <span itemscope itemtype="http://schema.org/Organization">
		   <a itemprop="url" href="">
		   <img itemprop="logo" src="../Images/collabion-logo.png" class="logo" alt="Collabion from FusionCharts" width="397" heigth="83"/>
		   </a>
	  </span>
    </div>
    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
        <ul class="nav navbar-nav navbar-right" >
            <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown"  aria-haspopup="true" aria-expanded="false">
			Collabion home page<span class="caret visible-xs-inline-block"></span></a>
            </li>
       </ul>
    </div>
  </div>
</nav>
	</header>

	<div class="content">

		<div class="container">
		<h1 class="mt-60 mb-10 lgtash mb-100">Thanks for installing <span class="chartsspe darkash">
		Collabion Charts for SharePoint</span> Online.</h1>
		<p class="lead mb-10 bold-text pwidth">At Collabion, we build products
		for <span class="darkash bold">Microsoft SharePoint</span>, <span class="darkash bold">
		Office 365</span> and <span class="darkash bold">PowerBl</span>. Our
		products enable you to get quick insights from your data, through
		meaningful visualizations that work across desktops and mobile devices. </p>
        <p class="lead mb-10 bold-text pwidth">Collabion Charts for SharePoint
		Online adheres to this philosophy and we sincerely hope it helps you to
		convert your data, stored in your online SharePoint environment, to
		stunning and insightful charts.</p>
		<h2 class="h4 bold-text mb-30">Three steps to get you started: </h2>
            <section class="pt-20 pb-20">
		    <div class="row">

			    <div class="col-sm-6 col-sm-push-6 col-md-5 col-md-push-7">
				    <img src="../Images/display-msg-guide.png" alt="Intuitive interface" width="1000" height="600" class="img-responsive">
			    </div>
			    <div class="col-sm-6 col-sm-pull-6 col-md-7 col-md-pull-5">
				   <span class="numbers">1</span>
				    <p class="pmaxwidth">Insert the Collabion Charts for
					SharePoint Online app part in your SharePoint page</p>
			    </div>

		    </div>
	    </section>
	    <hr>
        <section class="pt-20 pb-20">
		    <div class="row">

			    <div class="col-sm-6 col-sm-push-6 col-md-5 col-md-push-7">
				    <img src="../Images/mini-page.jpg" alt="Intuitive interface" width="1000" height="600" class="img-responsive">
			    </div>
			    <div class="col-sm-6 col-sm-pull-6 col-md-7 col-md-pull-5">
				    <span class="numbers">2</span>
				     <p class="pmaxwidth">This is how the Collabion Charts for
						SharePoint wizard looks like when launched for the first
						time</p>
			    </div>
		    </div>
	    </section>
	    <hr>
        <section class="pt-20 pb-20">
		    <div class="row">

			    <div class="col-sm-6 col-sm-push-6 col-md-5 col-md-push-7">
				    <img src="../Images/mini-page2.jpg" alt="Intuitive interface" width="1000" height="600" class="img-responsive">
			    </div>

			    <div class="col-sm-6 col-sm-pull-6 col-md-7 col-md-pull-5">
				    <span class="numbers">3</span>
				    <p class="pmaxwidth">Select a list and a view in the &quot;Select
					List&quot; and &quot;Select View&quot; drop-down fields respectively. The
					Collabion app part has intelligently determined the best
					possible combination from the selected List view and created
					a chart from it as displayed in the preview pane.</p>
			    </div>

		    </div>
	    </section>
	    <hr>
        <h2 class="h4 bold-text mb-30">Click on &quot;Done&quot; and voila! You have
		created your first chart in just 3 steps!  </h2>
        <p>To learn more about Collabion Charts for SharePoint Online, please
		have a look at our <a href="#" class="linkprod">Product Guide</a>.</p>
        <p>queries, doubts or suggestions.</p>
        <br />
        <p>Till then, happy charting!</p>
        </div>
        <br />
        <br />
        <br />
        <br />






	</div>


	<footer>
    <div class="footer-bottom">
			<div class="container">
				<div class="row">

					<div class="col-xs-12 col-sm-6 col-md-6 col-sm-push-6 col-md-push-6">
						<ul class="list-inline social-icons" itemscope itemtype="http://schema.org/Organization">
							<li class="facebook"><a title="Facebook" itemprop="sameAs" href="http://www.facebook.com/collabion" target="_blank"><i class="fa fa-facebook "></i></a></li>
							<li class="twitter"><a title="Twitter" itemprop="sameAs" href="http://www.twitter.com/collabion"  target="_blank"><i class="fa fa-twitter "></i></a></li>
							<li class="linkedin"><a title="LinkedIn" itemprop="sameAs" href="https://www.linkedin.com/company/collabion"  target="_blank"><i class="fa fa-linkedin"></i></a></li>
							<li class="youtube"><a title="Youtube" itemprop="sameAs" href="https://youtube.com/collabion" target="_blank"><i class="fa fa-youtube"></i></a></li>
						</ul>

					</div>
					<div class="col-xs-12 col-sm-6 col-md-6 col-sm-pull-6 col-md-pull-6">
						<div class="small copyright no-margin">© 2002-<script type="text/javascript">var d=new Date();document.write(d.getFullYear());</script>
							InfoSoft Global Private Limited. All Rights
							Reserved.</div>
					</div>
				</div>
			</div>
		</div>
	</footer>
</div>

    <script type="text/javascript" src="../Scripts/landingPage/bootstrap.min.js"></script>
    <script type="text/javascript" src="../Scripts/landingPage/modernizr.min.js"></script>
    <script type="text/javascript" src="../Scripts/landingPage/theme.js"></script>
</asp:Content>
