﻿CC.Tabs.Active.prototype.charts = {
    activate: function () {
        //var selChart = CC.util.getChart(CC.med.chartConfig().ChartType);
        //var chrtIndx = selChart.pop(selChart[selChart.length - 1]);
        //var catIndx = selChart.pop(selChart[selChart.length - 1]);

        //Ext.getCmp('pnlCat').select(CC.CM.Chart.getCategories()[catIndx], false, true);
        //Ext.getCmp('pnlCType').select(selChart[chrtIndx], false, true);

        //var PopulateData = function () {
        //var selChart = CC.util.getChart(CC.med.chartConfig().Configuration.ChartType);

        var selChart = CC.util.getChart(CC.med.getSetting('cType'));
        var catIndx = selChart.pop(selChart[selChart.length - 1]);
        var chrtIndx = selChart.pop(selChart[selChart.length - 1]);

        Ext.getCmp('pnlCat').select(catIndx, false, true);

        CC.Wizard.tabpanel.tabs[1].ct_store_charts.loadData(CC.med.cmChart().getTypesByCategory(catIndx));
        Ext.getCmp('pnlCType').select(chrtIndx, false, true);
        //}

        //PopulateData();
    },

    /* returns the total count of P & S from the given series */
    getDualYAxixs: function (series) {

        var axis = new Object;
        axis.pIndex = [];
        axis.sIndex = [];

        if (!series) {
            /* if no series is given, initialize it with the default series */
            series = CC.med.getSetting('series');
        }

        $.each(series, function (index, element) {
            if (element.parentYAxis) {
                if (element.parentYAxis === 'P') {
                    axis.pIndex.push(index);
                }
                else {
                    axis.sIndex.push(index);
                }
            }
        });

        return axis;
    },

    /* applies or remove the underlying Dual Y Axis setting */
    resetDualYAxisChartSetting: function (chartSeries, isDualY) {
        if (isDualY) {
            var axis = this.getDualYAxixs(chartSeries);
            var pIndex = -1;
            if (axis.pIndex.length > 1) {
                /* remove extra P Axis */
                for (var i = 1; i < chartSeries.length; i++) {
                    chartSeries[i].parentYAxis = '';
                }

                pIndex = axis.pIndex[0];
            }
            else {
                if (axis.pIndex.length == 0) {
                    /* select at-least 1 index */
                    chartSeries[0].parentYAxis = 'P';
                    pIndex = 0;
                }
            }

            if (axis.sIndex.length > 1) {
                /* remove extra S Axis */
                for (var i = 1; i < chartSeries.length; i++) {
                    chartSeries[i].parentYAxis = '';
                }
            }
            else {
                if (axis.sIndex.length == 0) {
                    /* select at-least 1 index */
                    var index = pIndex - 1 < 0 ? pIndex + 1 : pIndex - 1;
                    chartSeries[index].parentYAxis = 'S';
                }
            }
        }

        var combChart = CC.util.comboChartOptions();
        var combIndx = 0;

        $.each(chartSeries, function (index, element) {
            if (!isDualY) {
                /* remove all Dual Y Axis setting */
                element.parentYAxis = '';
            }

            if (!element.renderAs) {
                element.renderAs = combChart[combIndx++]
            }
        });

            CC.med.setUpdateFlag();
    },

    switchChrtTyp: function (selrec, chartConfig) {

        //selrec = this.chartTypes.getSelectedRecords();
        var _this = CC.Wizard.tabpanel.tabs[1];

        if (selrec.length > 0) {
            CC.util.toggleProgressBar(true);

            //var _chartConfig = CC.med.chartConfig();
            var chartSeries = CC.med.getSetting('series');
            var cacheSeries = CC.med.getSetting('cacheSeries');

            var curSeries = CC.util.getChartSeries(CC.med.getSetting('cType'));
            if ((curSeries == 0 && selrec[0].data.series > 0) || (curSeries > 0 && selrec[0].data.series == 0)) {
                //Selected and the current chart series are different

                var selFields = CC.med.getSetting('dsFld');
                if (curSeries == 0) {
                    if (cacheSeries.length < 2) {
                        /*Current chart is single series :: Selected Chart is single series :: Cache is empty */

                        var newSeries = CC.util.selectAutoField('Number');
                        if (newSeries.length > 0) {
                            //if (newSeries.length == 0)
                            //    newSeries = CC.util.selectAutoField('Calculated');
                            //if (newSeries.length == 0)
                            //    newSeries = CC.util.selectAutoField('Currency');

                            chartSeries.push({
                                "column": newSeries[0].data.InternalName, "seriesName": newSeries[0].data.Title, 'renderAs': 'bar'
                            });

                            cacheSeries.push({
                                "column": newSeries[0].data.InternalName, "seriesName": newSeries[0].data.Title, 'renderAs': 'bar'
                            });

                            if (chartSeries.length == 2) {
                                /* also update the 1st index */
                                chartSeries[0].renderAs = !chartSeries[0].renderAs ? 'bar' : chartSeries[0].renderAs;
                                cacheSeries[0].renderAs = chartSeries[0].renderAs;
                            }

                            /* check selected fields */
                            //var itemExist = false;
                            //$.each(selFields, function (index, element) {
                            //    if (element == newSeries[0].data.InternalName) {
                            //        itemExist = true;
                            //        return false;
                            //    }
                            //});



                            //add the new field if it's not already added
                            if (this.updateSelectedFields(selFields, newSeries[0].data.InternalName))
                                selFields.push(newSeries[0].data.InternalName);
                        }
                        else {
                            CC.med.createChart(undefined, CC.msgs.err181);

                            return;
                        }
                    }
                    else {
                        /*Current chart is single series :: Selected Chart is multi series :: Cache has data */
                        var combChart = CC.util.comboChartOptions();
                        var combIndx = 0;
                        var curScope = this;

                        $.each(cacheSeries, function (index, element) {
                            /* check if the cache series is to be added is combination or other charts */
                            if (CC.util.jsonItemIndex(chartSeries, element.column, "column") == -1) {
                                /* other */
                                if (!CC.util.isCombinationChart(CC.med.getSetting('cType')) || (CC.util.isCombinationChart(CC.med.getSetting('cType')) && element.hasOwnProperty('renderAs')))
                                    chartSeries.push(element);
                                else {
                                    /* combination */
                                    chartSeries.push({
                                        "column": element.column,
                                        "seriesName": element.column,
                                        "renderAs": !element.renderAs ? combChart[combIndx++] : element.renderAs, //if renderAxis exist, re-use it. Otherwise generate a new setting.
                                        "parentYAxis": CC.util.isDualYAxis() && element.parentYAxis ? element.parentYAxis : ""
                                    });
                                }

                                /* check if the current field is in the selected fields or not */
                                if (curScope.updateSelectedFields(selFields, element.column))
                                    selFields.push(element.column);
                            }
                        });
                    }

                    //if (CC.med.getSetting('cType') == 'mscombidy2d') {
                    if (CC.util.isDualYAxis(selrec[0].data.internalName)) {
                        //Dual Y-Axis
                        if (!CC.med.getSetting('chart').pYAxisName && CC.med.getSetting('chart').yAxisName) {
                            CC.med.updateSetting('chart', CC.med.getSetting('chart').yAxisName, 'pYAxisName');
                        }

                        this.resetDualYAxisChartSetting(chartSeries, true);
                    }
                    else {
                        //all the other charts
                        if (CC.med.getSetting('chart').pYAxisName) {
                            if (!CC.med.getSetting('chart').yAxisName) {
                                CC.med.updateSetting('chart', CC.med.getSetting('chart').pYAxisName, 'yAxisName');
                            }
                            CC.med.updateSetting('chart', '', 'pYAxisName');
                        }
                    }
                }
                else {
                    //change multiseries to single series

                    //CC.med.chartConfig()..Series = (JSON.parse(JSON.stringify(CC.med.charts().Cache.Configuration.Series)));
                    chartSeries.splice(1, chartSeries.length);
                    chartSeries[0].parentYAxis = '';
                    chartSeries[0].renderAs = '';

                    //clear selected fields
                    CC.med.updateSetting('dsFld', []);
                    CC.med.updateSetting('dsFld', [chartSeries[0].column, CC.med.getSetting('xAxis').column]);

                    //CC.med.chartConfig()..XAxis = CC.med.charts().Cache.Configuration.XAxis;
                }
            }
            else if (!CC.util.isDualYAxis() && CC.util.isDualYAxis(selrec[0].data.internalName)) {
                /* For Multiseries
                 * Current is NOT Dual Y Axis but selected is
                 */
                this.resetDualYAxisChartSetting(chartSeries, true);

            }
            else if (CC.util.isDualYAxis() && !CC.util.isDualYAxis(selrec[0].data.internalName)) {
                /* For Multiseries
                 * Current is Dual Y Axis but selected is NOT
                 */
                this.resetDualYAxisChartSetting(chartSeries, false);
            }
            //else if (curSeries > 0 && selrec.series == 0) {
            //    //change multiseries to single series

            //}
            //_chartConfig.Configuration.ChartType = selrec[0].data.internalName;
            CC.med.updateSetting('cType', selrec[0].data.internalName);
            //CC.med.updateSetting('cType', CC.util.getChartSeries(selrec[0].data.internalName));
            var chartSeries = CC.util.getChartSeries(selrec[0].data.internalName);

            //check the new modified value
            //if (CC.med.getSetting('cType') > 0) {
            if (chartSeries > 0) {
                //CC.med.charts().Cache.MultiChartType = selrec[0].data.internalName;
                CC.med.updateSetting('cacheMulti', selrec[0].data.internalName);
            }
            else {
                //_chartConfig.Cache.SingleChartType = selrec[0].data.internalName;
                CC.med.updateSetting('cacheSingl', selrec[0].data.internalName);
            }

            //distinct properties checking
            CC.med.updateSetting('distinct', selrec[0].data.distinct ? selrec[0].data.distinct : {});
            CC.med.updateSetting('cacheDistinct', selrec[0].data.distinct ? selrec[0].data.distinct : {});

            //CC.util.createChart(chartConfig);
            CC.med.createChart();


            //Do something
            var thisType = selrec[0].data.type;
            _this.prevChrtTyp = thisType;
            //_chartConfig.Configuration.ChartType = selrec[0].data.internalName;
            //CC.med.updateSetting('cType', selrec[0].data.internalName);
        }
        else if (_this.prevChrtTyp >= 0)
            _this.chartTypes.select(_this.prevChrtTyp, false, true);

        //Ext.getCmp('pnlCharts').initialConfig.valid = true;
    },
    switchChrtCat: function (selrec) {


        //selrec = this.chartCatogories.getSelectedRecords();
        var _this = CC.Wizard.tabpanel.tabs[1];

        if (selrec.length > 0) {
            var thisId = selrec[0].data.id;
            _this.ct_store_charts.loadData(_this.chart.getTypesByCategory(thisId));
            _this.prevChrtCat = thisId;
            _this.prevChrtTyp = -1;
        }
        else if (_this.prevChrtCat >= 0)
            _this.chartCatogories.select(_this.prevChrtCat, false, true);
        //this.ct_store_charts.loadData(this.Chart.getTypesByCategory(1));

        //Ext.getCmp('pnlCharts').initialConfig.valid = false;

        //CC.util.generateTip();

        /* track the chart category */
        CC.track.prototype.chartCatagoryView(selrec[0].data.name);
    },

    /* check if the current field is in the selected fields or not */
    updateSelectedFields: function (selFields, newFieldInternalName) {
        /* check selected fields */
        var itemExist = false;
        $.each(selFields, function (index, element) {
            if (element == newFieldInternalName) {
                itemExist = true;
                return false;
            }
        });
        return !itemExist;
    }
}