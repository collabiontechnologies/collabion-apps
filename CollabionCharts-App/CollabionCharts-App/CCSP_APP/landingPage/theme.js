/* parse the url query string */
var getQueryStringParameter = function (paramToRetrieve) {
    var params = document.URL.split("?")[1].split("&");
    for (var i = 0; i < params.length; i = i + 1) {
        var singleParam = params[i].split("=");
        if (singleParam[0] == paramToRetrieve)
            return singleParam[1];
    }
}

//
//use getViewPortSize() to get the viewport size
//-----------------------------------------

function getViewPortSize() {
    var viewPortSize = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
    return viewPortSize;
    //console.log(viewPortSize);
}




// //
// //Menu hover dropdown for Desktop
// //----------------------------------------

function navHover() {
    $('ul.nav li.dropdown').hover(function () {
        // $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn();
        $(this).addClass("open");
    }, function () {
        // $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut();
        $(this).removeClass("open");
        // });
    });
}



function navClick() {
    $('ul.dropdown-menu [data-toggle=dropdown]').on('click', function (event) {
        // Avoid following the href location when clicking
        event.preventDefault();
        // Avoid having the menu to close when clicking
        event.stopPropagation();
        // Re-add .open to parent sub-menu item
        $(this).parent().addClass('open');
        $(this).parent().find("ul").parent().find("li.dropdown").addClass('open');
    });
}



//
//DOCUMENT READY
//-----------------------------------------

$(document).ready(function () {
    var ws = getViewPortSize(),
	isTouch = Modernizr.touch;
    if ((ws <= 768) || (isTouch)) {

        navClick(); //Click dropdown nav menu.
    }
    else {

        navHover(); //Hover dropdown nav menu.
    }

    !function () {
        var analytics = window.analytics = window.analytics || []; if (!analytics.initialize) if (analytics.invoked) window.console && console.error && console.error("Segment snippet included twice."); else {
            analytics.invoked = !0; analytics.methods = ["trackSubmit", "trackClick", "trackLink", "trackForm", "pageview", "identify", "reset", "group", "track", "ready", "alias", "page", "once", "off", "on"]; analytics.factory = function (t) { return function () { var e = Array.prototype.slice.call(arguments); e.unshift(t); analytics.push(e); return analytics } }; for (var t = 0; t < analytics.methods.length; t++) { var e = analytics.methods[t]; analytics[e] = analytics.factory(e) } analytics.load = function (t) { var e = document.createElement("script"); e.type = "text/javascript"; e.async = !0; e.src = ("https:" === document.location.protocol ? "https://" : "http://") + "cdn.segment.com/analytics.js/v1/" + t + "/analytics.min.js"; var n = document.getElementsByTagName("script")[0]; n.parentNode.insertBefore(e, n) }; analytics.SNIPPET_VERSION = "3.1.0";
            analytics.load("sD8xNzKcfNTYQgC3KgyUrxyAAgUdMloe");
            //analytics.page()
            analytics.page({
                title: 'CA-LandingPage',
                url: decodeURIComponent(getQueryStringParameter('SPHostUrl'))
            });
        }
    }();

    $('#aBackToSite').prop('href', decodeURIComponent(getQueryStringParameter("SPHostUrl")));

});

//
//DOCUMENT RESSIZE & ORIENTATION  CHANGE
//------------------------------------------

$(window).on('resize orientationchange', function () {
    //console.log("document ready");
    var ws = getViewPortSize(),
	isTouch = Modernizr.touch;
    if ((ws <= 768) || (isTouch)) {
        //console.log("small device");
        navClick(); //Click dropdown nav menu.
    }
    else {
        //console.log("desktop");
        navHover(); //Hover dropdown nav menu.
    }
});


//
//WINDOW LOAD
//------------------------------------------

//$(window).on('load', function () {
//    //console.log("window load");
//});



