﻿
/*
 * Mediator patterns
 *
 * https://addyosmani.com/largescalejavascript/#mediatorpattern
 *
 */

CC.Mediator = (function () {

    /*
     * Singleton
     *
     * https://github.com/shichuan/javascript-patterns/blob/master/design-patterns/singleton.html
     * https://scotch.io/bar-talk/4-javascript-design-patterns-you-should-know
     *
     */
    var thisInstance;
    var isClosedByUser = false;

    var appProperty = {
        ds: "PM.dataSource",                                                    //*
        dsData: "PM.dataSource.data",
        dsType: "PM.dataSource.type",
        dsId: "PM.dataSource.data.id",
        dsView: "PM.dataSource.data.viewID",
        dsFld: "PM.dataSource.selectedFields",
        chrtCnfg: "CM.chartConfig",                                             //*
        cache: "CM.cache",                                                      //*
        cacheData: "CM.cache.data",
        cacheConfig: "CM.cache.configuration",
        cacheDistinct: "CM.chartConfig.configuration.distinct",
        cacheXAxis: "CM.cache.configuration.xAxis",
        cacheSeries: "CM.cache.configuration.series",
        cacheCType: "CM.cache.configuration.chartType",
        cacheFld: "CM.cache.selectedFields",
        cacheSingl: "CM.cache.singleChartType",
        cacheMulti: "CM.cache.multiChartType",
        config: "CM.chartConfig.configuration",
        distinct: "CM.chartConfig.configuration.distinct",
        xAxis: "CM.chartConfig.configuration.xAxis",
        series: "CM.chartConfig.configuration.series",
        cType: "CM.chartConfig.configuration.chartType",
        theme: "CM.chartConfig.configuration.theme",
        cosmetics: "CM.chartConfig.cosmetics",                                  //*
        chart: "CM.chartConfig.cosmetics.chart",
        //dataPlot: "CM.chartConfig.cosmetics.dataPlot",
        //numberFormatting: "CM.chartConfig.cosmetics.numberFormatting",
        //showLegend: "CM.chartConfig.cosmetics.showLegend"
    };

    var requests = new Object;
    //private properties
    requests.count = 0;
    requests.currentReqStatus = CC.util.requestsStatus().None;

    var properties = [];
    properties.documentHeader = {
        param: {
            hostWebUrl: null,           // Host web url
            appWebUrl: null,            // App web url
            senderId: null,             // Sender id
            editMode: null,             // Ensure app part is in edit mode = 1 or view mode = 0
            wpId: null,                 // App part id
            wpq: null,                  // App part container id
            hostPage: null,             // Relative host page
            scriptBase: null,           // Default SP2013 scriptbase
            pageInEdit: null
        }
    };

    //var currentReqStatus = CC.util.requestsStatus().None;
    var listItemProperties = function () {
        //Determines if the config of this chart already exist or not. Based on it item create or update method will be triggered
        this.exist = false;

        //stores the etag value of the item to be used for item update. No delete event is handled otherwise it would have been required for delete as well
        this.etag = "*";

        //the unique SharePoint ListItem Id to be used for item update. If item is being created then this value will also get set there apart from
        //the usual get
        this.itemId = -1;
    };

    var curAppSeting = new listItemProperties();
    var createProperty = function () {
        return !curAppSeting.exist;
    }

    var activateSteps = CC.Tabs.Active.prototype;

    properties.appProperty = [];
    properties.appProperty.PM = new CC.ProviderManager();
    properties.appProperty.CM = new CC.ChartManager();

    var timer = null;

    var createMed = function () {
        //

        CC.util.toggleProgressBar(true);

        if (!properties.appProperty.PM.currentProvider && properties.appProperty.CM.chartConfig.length == 0) {

            //#region First Load

            $.when(properties.appProperty.PM, properties.appProperty.CM, properties.documentHeader.param, curAppSeting, CC.Setting.prototype.LoadFromQueryString(properties.documentHeader.param)).done(function (__PM, __CM, __param, __curAppSeting) {
                $.when(__PM, __CM, __param, __curAppSeting, CC.Setting.prototype.getAppConfig(__param)).done(function (_PM, _CM, _param, _curAppSeting, data) {
                    try {

                        //#region Loads the Chart setting for the page in both edit and non-edit mode

                        //_PM = new CC.ProviderManager();
                        //_CM = new CC.ChartManager();
                        _CM.cache = []; //Initialing an empty cache

                        var chartCreationTask = new Ext.util.DelayedTask(function () {
                            //thisInstance.createChart('panel-wizard');
                            InitWiz();
                        });
                        var delay = 1;
                        var hasSetting = true;

                        if (data.length > 0) {
                            //#region Chart has setting

                            /*Set the private properties only once during the get
                             * Irrespective of whether or not the page or web part is in edit mode, these properties will be initialised and a chart will be displayed */

                            //_curAppSeting.exist = true;
                            //_curAppSeting.etag = data[0].__metadata.etag;
                            //_curAppSeting.itemId = data[0].Id;

                            intializeSetting(data[0]);

                            var config = JSON.parse(data[0].Config);
                            _PM.selectProvider(config.ProviderType);
                            _PM.dataSource = config.ProviderConfig;
                            _CM.chartConfig.configuration = config.ChartConfig.Configuration;
                            _CM.chartConfig.cosmetics = config.ChartConfig.Cosmetics;

                            /*Create chart */

                            CC.util.toggleProgressBar(true);
                            //thisInstance.createChart('panel-wizard');

                            if (Object.keys(_PM.dataSource.data).length == 0 && CC.util.isPageEdit()) {
                                InitWiz();
                            }

                            //#endregion
                        }
                        else if (!_PM.currentProvider) {
                            //#region Chart has no setting

                            /* Chart not configured.Set default type to SpList. */

                            _PM.selectProvider(0);
                            _PM.dataSource = {
                                data: {}
                            }
                            _CM.chartConfig.configuration = {};
                            _CM.chartConfig.cosmetics = {};

                            //thisInstance.createChart('panel-wizard');

                            /* Add a border for empty containers */
                            //$('#panel-wizard').css('border-width', '1px');

                            if (CC.util.isPageEdit()) {
                                //InitWiz();
                                delay = 100;
                            }
                            else {
                                CC.util.addCssClass($('#panel-wizard')[0], ['noSetting']);
                            }

                            hasSetting = false;

                            //#endregion
                        }

                        thisInstance.createChart('panel-wizard');
                        if (delay > 1) {
                            /* directly load chart if it's in edit mode with no setting */
                            chartCreationTask.delay(delay);
                        }

                        if (CC.util.isPageEdit()) {
                            /* add a preview message for edit mode*/
                            $('#ccParent').append('<label id="lblPrevChartMsg" class="x-form-item-sub" style="font-size: 11px; color: grey">' + CC.msgs.wzrd175 + '</label>');
                        }

                        /* first load triggered */
                        CC.track.prototype.identify(hasSetting);

                        //#endregion
                    }
                    catch (error) {
                        thisInstance.logError({}, '140', 'Med-DefInitialize', error.body ? error.body : error.message ? error.message : error);
                    }

                }).fail(function (err) { });

            }).fail(function (err) { });

            //#endregion
        }
            //else if (properties.documentHeader.param.editMode && !Ext.getCmp("invokeButton") && !timer) {
        else if (CC.util.isPageEdit() && !Ext.getCmp("invokeButton") && !timer) {

            //#region App part is in edit mode and the creation of the wizard has just been triggered.

            try {
                /* apply fixed height-width for the wizard */
                $('#panel-wizard').height('580px');
                $('#panel-wizard').width('645px');

                // bind the unload event to track any unexpected frame reload
                $(window).bind('beforeunload', function () {
                    if (!isClosedByUser) {
                        CC.track.prototype.wizardClosed(false);
                    }
                });

                //clear the chart panel
                Ext.fly('panel-wizard').update('');
                $('#lblPrevChartMsg').remove();

                //initiate the timer
                timer = setInterval(function () {
                    //
                    if (properties.appProperty.PM.currentProvider &&
                        properties.appProperty.CM.chartConfig.configuration.series &&
                        properties.appProperty.CM.chartConfig.configuration.series.length > 0) {
                        if (requests.count > 0 && requests.currentReqStatus != CC.util.requestsStatus().InProcess) {
                            //Console.log("Attempting to save changes - timer.");

                            //set the flag to in-process.
                            requests.currentReqStatus = CC.util.requestsStatus().InProcess;
                            CC.Setting.prototype.setAppConfig(allSetting(), properties.documentHeader.param, curAppSeting.itemId, curAppSeting.etag, createProperty(), false, requests, curAppSeting)
                            .done(function (data) {
                                intializeSetting(data);
                            });
                        }
                        //else
                            //Console.log("No changes to save - timer.");
                    }
                }, 3000);

                //Console.log("Subscribing activate events.");

                //#region subscribing to the activate events selectively
                $.Topic(CC.util.tabsActivate('data')).subscribe(function () {
                    try {
                        activateSteps.data.activate();
                    }
                    catch (error) {
                        thisInstance.logError({}, '143', 'dataActivate', error.body ? error.body : error.message ? error.message : error);
                    }
                });

                $.Topic(CC.util.tabsActivate('fields')).subscribe(function () {
                    //try {
                    activateSteps.fields.activate();
                    //}
                    //catch (error) {
                    //    thisInstance.logError({}, '144', 'dataFields', error.body ? error.body : error.message ? error.message : error);
                    //}
                });

                $.Topic(CC.util.tabsActivate('charts')).subscribe(function () {
                    try {
                        activateSteps.charts.activate();
                    }
                    catch (error) {
                        thisInstance.logError({}, '145', 'dataCharts', error.body ? error.body : error.message ? error.message : error);
                    }
                });

                $.Topic(CC.util.tabsActivate('labels')).subscribe(function () {
                    try {
                        activateSteps.labels.activate();
                    }
                    catch (error) {
                        thisInstance.logError({}, '146', 'dataLabels', error.body ? error.body : error.message ? error.message : error);
                    }
                });

                $.Topic(CC.util.tabsActivate('numbers')).subscribe(function () {
                    try {
                        activateSteps.numbers.activate();
                    }
                    catch (error) {
                        thisInstance.logError({}, '147', 'dataNumbers', error.body ? error.body : error.message ? error.message : error);
                    }
                });

                $.Topic(CC.util.tabsActivate('lblUpdt')).subscribe(function (args) {
                    try {
                        activateSteps.labels.update(args.id, args.val);

                        if (activateSteps.labels.componentLoaded)
                            thisInstance.createChart();
                    }
                    catch (error) {
                        thisInstance.logError({}, '148', 'LabelTabUpdate', error.body ? error.body : error.message ? error.message : error);
                    }
                });

                $.Topic(CC.util.tabsActivate('nmbrUpdt')).subscribe(function (args) {
                    try {

                        activateSteps.numbers.update(args.id, args.val);

                        if (activateSteps.numbers.componentLoaded)
                            thisInstance.createChart();
                    }
                    catch (error) {
                        thisInstance.logError({}, '149', 'NumberTabUpdate', error.body ? error.body : error.message ? error.message : error);
                    }
                });

                $.Topic(CC.util.tabsActivate('yaxis')).subscribe(function (args) {
                    try {
                        activateSteps.fields.toggleCalloutPnl(args.e, args.sender);
                    }
                    catch (error) {
                        thisInstance.logError({}, '1410', 'CalloutPanel', error.body ? error.body : error.message ? error.message : error);
                    }
                });

                //$.Topic(CC.util.tabsActivate('yaxis')).subscribe(function (args) {
                //    activateSteps.fields.toggleCalloutPnl(args.e, args.sender);
                //})

                $.Topic(CC.util.tabsActivate('yaxisRender')).subscribe(function (args) {
                    try {
                        activateSteps.fields.loadComboChartSetting(args.id);
                    }
                    catch (error) {
                        thisInstance.logError({}, '1411', 'yaxisRender', error.body ? error.body : error.message ? error.message : error);
                    }
                });

                $.Topic(CC.util.tabsActivate('cmbTheme')).subscribe(function (args) {
                    try {
                        thisInstance.updateSetting('theme', args.val);

                        thisInstance.createChart();
                    }
                    catch (error) {
                        thisInstance.logError({}, '1415', 'theme', error.body ? error.body : error.message ? error.message : error);
                    }
                });

                $.Topic(CC.util.target('fldYAxs')).subscribe(function (args) {
                    activateSteps.fields.selectYAxis(args.c, args.r, properties.appProperty.CM.chartConfig);
                })

                //CC.med.subscribe({
                //    id: CC.util.target('yaxis'),
                //    tab: CC.util.tabs().fields,
                //    type: CC.util.xTypes('button')
                //})

                //$.Topic(CC.util.tabsActivate().addY).subscribe(function () {
                //    activateSteps.numbers.activate();
                //})

                //#endregion

                fetchSpList();
            }
            catch (error) {
                thisInstance.logError({}, '141', 'Med-EditInitialize', error.body ? error.body : error.message ? error.message : error);
            }

            //#endregion
        }
        else if (timer) {


            //stop the timer if called for any unknown reasons
            //clearTimer();
            //timer = undefined;
            thisInstance.resetWizard();

            thisInstance.logError({}, '142', 'Med-UnknownInitialize', error.body ? error.body : error.message ? error.message : error);
        }
    };

    var clearTimer = function () {
        if (timer)
            clearInterval(timer);
    };

    var intializeSetting = function (data) {
        if (!curAppSeting.exist) {
            curAppSeting.exist = true;
            curAppSeting.etag = data.__metadata.etag;
            curAppSeting.itemId = data.Id;
        }
    };

    var topics = {};

    /*
     * Publisher-Subscriber model
     *
     * https://github.com/shichuan/javascript-patterns/blob/master/jquery-patterns/pubsub-callback.html
     *
     */
    jQuery.Topic = function (id) {
        var callbacks,
            topic = id && topics[id];
        if (!topic) {
            callbacks = jQuery.Callbacks("unique");
            topic = {
                publish: callbacks.fire,
                subscribe: callbacks.add,
                unsubscribe: callbacks.remove
            };
            if (id) {
                topics[id] = topic;
            }
        }
        return topic;
    };

    var subscription = {
        subscribe: function (jsonKey) {
            switch (jsonKey.tab) {
                case CC.util.tabs().data:
                    this.dataTab(jsonKey);
                    break;
                case CC.util.tabs().fields:
                    this.fieldsTab(jsonKey);
                    break;
                case CC.util.tabs().charts:
                    this.chartsTab(jsonKey);
                    break;
                case CC.util.tabs().labels:
                    this.labelsTab(jsonKey);
                    break;
                case CC.util.tabs().numbers:
                    this.numbersTab(jsonKey);
                    break;
            }
        },
        dataTab: function (jsonKey) {
            try {
                reset();

                switch (jsonKey.type) {
                    case CC.util.xTypes('cc:combo'):
                        //
                        if (jsonKey.id == CC.util.target('ccList')) {
                            $.Topic(jsonKey.id).subscribe(function (args) {
                                CC.Wizard.tabpanel.tabs[0].toggleEnable(false);
                                activateSteps.data.populateView(args.listId, args.viewId);
                            })
                        }
                        else if (jsonKey.id == CC.util.target('ccView')) {
                            $.Topic(jsonKey.id).subscribe(function (args) {
                                activateSteps.data.selectFields(args.id, properties.appProperty.CM.chartConfig, args.auto);
                            })
                        }
                        break;
                }
            }
            catch (error) {
                thisInstance.logError({}, '1412', 'dataTab', error.body ? error.body : error.message ? error.message : error);
            }
        },
        fieldsTab: function (jsonKey) {
            try {
                reset();

                switch (jsonKey.type) {
                    case CC.util.xTypes('dataview'):
                        if (jsonKey.id == CC.util.target('axisView')) {
                            $.Topic(jsonKey.id).subscribe(function (selRec) {
                                activateSteps.fields.switchAxisView(selRec);
                            })
                        }
                        else if (jsonKey.id == CC.util.target('chartView')) {
                            $.Topic(jsonKey.id).subscribe(function () {
                                activateSteps.fields.switchChartView(properties.appProperty.CM.chartConfig);
                            })
                        }
                        break;
                    case CC.util.xTypes('cc:combo'):
                        if (jsonKey.id == CC.util.tabsActivate('swpFld')) {
                            $.Topic(jsonKey.id).subscribe(function (args) {
                                activateSteps.fields.swapValues(args.r, args.c, args.excludeCurrent);
                            })
                        }
                        else if (jsonKey.id == CC.util.target('fldXAxs')) {
                            $.Topic(jsonKey.id).subscribe(function (args) {

                                activateSteps.fields.selectXAxis(args, properties.appProperty.CM.chartConfig);
                            })
                        }
                        //else if (jsonKey.id == CC.util.target('fldYAxs')) {
                        //    $.Topic(jsonKey.id).subscribe(function (args) {
                        //
                        //        activateSteps.fields.selectYAxis(args.c, args.r, properties.appProperty.CM.chartConfig);
                        //    })
                        //}
                        break;
                    case CC.util.xTypes('button'):
                        if (jsonKey.id == CC.util.tabsActivate('addY')) {
                            $.Topic(jsonKey.id).subscribe(function () {
                                activateSteps.fields.addYAxis();
                            })
                        }
                        else if (jsonKey.id == CC.util.target('yaxis')) {
                            $.Topic(jsonKey.id).subscribe(function (args) {
                                activateSteps.fields.toggleCalloutPnl(args.e, args.sender);
                            })
                        }
                        break;
                    default:
                        if (jsonKey.id == CC.util.target('btnYes')) {
                            $.Topic(jsonKey.id).subscribe(function () {
                                activateSteps.fields.deleteYAxis(properties.appProperty.CM.chartConfig);
                            })
                        }
                        break;
                }
            }
            catch (error) {
                thisInstance.logError({}, '1413', 'fieldsTab', error.body ? error.body : error.message ? error.message : error);
            }
        },
        chartsTab: function (jsonKey) {
            try {
                switch (jsonKey.type) {
                    case CC.util.xTypes('dataview'):
                        if (jsonKey.id == CC.util.target('pnlCat')) {
                            $.Topic(jsonKey.id).subscribe(function (selRec) {
                                activateSteps.charts.switchChrtCat(selRec);
                            })
                        }
                        else if (jsonKey.id == CC.util.target('pnlCType')) {
                            $.Topic(jsonKey.id).subscribe(function (selRec) {
                                reset();

                                activateSteps.charts.switchChrtTyp(selRec, properties.appProperty.CM.chartConfig);
                            })
                        }
                        break;
                }
            }
            catch (error) {
                thisInstance.logError({}, '1414', 'chartsTab', error.body ? error.body : error.message ? error.message : error);
            }
        },
        labelsTab: function (jsonKey) {
            //check update setting
        },
        numbersTab: function (jsonKey) {
            //check update setting
        }
    };

    Object.resolve = function (path, obj, safe) {
        return path.split('.').reduce(function (prev, curr) {
            return !safe ? prev[curr] : (prev ? prev[curr] : undefined)
        }, obj || self)
    };

    //var saveSettingsType = function (type) {
    //    var retObj;
    //    switch (type) {
    //        case 'PM':
    //            retObj = properties.appProperty.PM;
    //            break;
    //        case 'CM':
    //            retObj = properties.appProperty.CM;
    //            break;
    //        case 'Cos':
    //            retObj = properties.appProperty.CM.Cosmetics;
    //            break;
    //    }
    //    return retObj;
    //}

    //let's you access any specific saved property
    var getProperties = function (identifier) {
        //return propKeys[identifier];
        return thisInstance.keys()[identifier];
    }

    var keys = function (key) {
        return appProperty[key];
    }

    //returns the entire chart setting in the prescribed format
    var allSetting = function () {
        return JSON.stringify({
            "ProviderType": thisInstance.currentProvider().type,
            "ProviderConfig": properties.appProperty.PM.dataSource,
            "ChartConfig": {
                "Configuration": properties.appProperty.CM.chartConfig.configuration,
                "Cosmetics": properties.appProperty.CM.chartConfig.cosmetics
            }
        });
    };

    //reset setting
    var reset = function () {
        thisInstance.updateSetting('cacheData', []);
    };

    //Loads the current sites Lists into the select List combo
    var fetchSpList = function () {
        //SPList Manager
        if (!CC.SPList)
            CC.SPList = new CC.Data.SPList();

        CC.Stores.create();
        CC.med.currentProvider().getAllDataSource(function (lists) {
            lists.unshift({ 'Id': '-1', 'Title': '--- Select ---' });

            CC.Stores.Lists.loadData(lists);



            Ext.getCmp("ccList").bindStore(CC.Stores.Lists);

            CC.util.toggleProgressBar();
        });
    };

    var initializeMed = function () {
        return {

            //#region previously exposed global variables

            //providerConfig: function () {
            //    return properties.appProperty.PM;
            //},

            //charts: function () {
            //    return properties.appProperty.CM;
            //},

            //chartConfig: function () {
            //    return properties.appProperty.CM.ChartConfig;
            //},

            //chartCosmetics: function () {
            //    return properties.appProperty.CM.Cosmetics;
            //},

            //#endregion

            cmChart: function () {
                return properties.appProperty.CM ? properties.appProperty.CM.chart : null;
            },

            createChart: function (renderArea, errorMsg) {
                renderArea = !renderArea ? 'pnlPrev' : renderArea;

                var cb = new CC.ChartBuilder(properties.appProperty.CM.chartConfig, properties.appProperty.PM, renderArea);
                cb.ChartWidth = Ext.getCmp(renderArea) ? Ext.getCmp(renderArea).getWidth() : Ext.get(renderArea).getWidth();
                cb.ChartHeight = Ext.getCmp(renderArea) ? Ext.getCmp(renderArea).getHeight() : Ext.get(renderArea).getHeight();
                cb.DataEmptyMessage = errorMsg;
                cb.createChart();
            },

            currentProvider: function () {
                return properties.appProperty.PM ? properties.appProperty.PM.currentProvider : null;
            },

            docHeader: function (strKey) {
                return Object.resolve(strKey, properties.documentHeader.param)
                //return properties.documentHeader.param[strKey];
            },

            getSetting: function (identifier) {
                try {
                    //return Object.resolve(getProperties(identifier), properties.appProperty);
                    return Object.resolve(keys(identifier), properties.appProperty);
                }
                catch (error) {
                    thisInstance.logError({ 'property': identifier }, '122', 'GetErrorKey', error.body ? error.body : error.message ? error.message : error);
                }
            },

            isChartConfigExist: function () {
                return properties.appProperty.CM.chartConfig.configuration.chartType != undefined && properties.appProperty.CM.chartConfig.configuration.series.length > 0;
            },

            loadAppProperty: function () {
                createMed();
            },

            logError: function (error, errorId, errorMsg, body) {

                if (error && error.statusCode == 403) {
                    /* re-load the frame */
                    CC.track.prototype.recordError('403');

                    thisInstance.resetWizard();
                }
                else {
                    var replacer = function (name, val) {
                        if (name == 'body' || name == 'state' || name == 'statusCode' || name == 'errorId' || name == 'errorMsg' || (typeof (val) === 'object' && name == ''))
                            return val;
                        else if (typeof (val) === 'object')
                            return JSON.stringify(val);

                        return undefined;
                    }

                    error.errorId = errorId;
                    error.errorMsg = errorMsg;

                    if (body) {
                        error.body = body;
                    }

                    var clientContext = SP.ClientContext.get_current();
                    SP.Utilities.Utility.logCustomAppError(clientContext, JSON.stringify(error, replacer, 4));
                    clientContext.executeQueryAsync();
                    //function () { },
                    //function (data) { alert("Unable to log error. Please contact your administrator if you see this message often."); });

                    //
                    ////Console.log('ERROR:: ');
                    ////Console.log(error);

                    CC.track.prototype.recordError(errorId);
                }
            },

            publish: function (key, args) {
                $.Topic(key).publish(args);
            },

            resetWizard: function () {
                clearTimer();
                //reset();
                location.reload();
            },

            saveSetting: function (sender) {
                //
                if (sender && sender.getXType() == 'button' && sender.id == 'btnDone') {
                    //will be triggered only when the DONE button is clicked. Should be used to clear the wizard
                    //will stop the timer permanently

                    isClosedByUser = true;
                    CC.util.toggleProgressBar(true);
                    CC.Setting.prototype.setAppConfig(allSetting(), properties.documentHeader.param, curAppSeting.itemId, curAppSeting.etag, createProperty(), true, requests);
                }
                else {
                    this.logError({}, 133, 'customSaveSetting', 'Unknown call made to save setting!');
                    this.resetWizard();
                }
            },

            setUpdateFlag: function () {
                //increase the counter value to signal the timer about the changes
                requests.count++;
            },

            subscribe: function (jsonKey) {
                return subscription.subscribe(jsonKey);
            },

            //updates a json setting value based on the identifier and its value. Optional param can also be used to update a given child prop of
            //a parent setting
            updateSetting: function (identifier, val, optionalChild) {
                try {
                    //

                    //Object.resolve(strKey, saveSettingsType(identifier)) = val;
                    //
                    //var strKey = getProperties(identifier) + (optionalChild ? "." + optionalChild : "");
                    var strKey = keys(identifier) + (optionalChild ? "." + optionalChild : "");
                    var currKey = strKey.substring(strKey.lastIndexOf('.') + 1);
                    //var parKey = strKey.slice(0, -(currKey.length + 1));
                    //var _json = this.getSetting(strKey.slice(0, -(currKey.length + 1)));
                    //this.getSetting(strKey.slice(0, -(currKey.length + 1)))[currKey] = val;
                    Object.resolve(strKey.slice(0, -(currKey.length + 1)), properties.appProperty)[currKey] = val;

                    if (!identifier.startsWith('cache')) {
                        this.setUpdateFlag();
                    }

                    if (identifier === 'cType') {
                        /* track chart creation */
                        CC.track.prototype.chartCreated(val);
                    }
                }
                catch (error) {
                    thisInstance.logError({ 'property': identifier, 'child': optionalChild }, '121', 'UpdateErrorKey', error.body ? error.body : error.message ? error.message : error);
                }
            }
        }
    }

    return {
        getInstance: function () {
            //if (!thisInstance || (properties.documentHeader.param.editMode && !Ext.getCmp("invokeButton") && !timer))
            //    createMed();
            if (!thisInstance) {
                /* create only a single instance of the self */

                thisInstance = initializeMed();
            }
            return thisInstance;
        }
    }
}
)();