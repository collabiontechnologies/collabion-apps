﻿//namespace("Collabion.App.Data");

/*********************************
 * Collabion Data Access layer
 * In the constructor host web url, app web url and script base url is determined
 ********************************/
CC.Data.SPList = function () {
};

/**************
 * Execute request method
 * All other ethods must use this method to execute REST request
 *******************/
CC.Data.SPList.prototype.executeRequest = function (url, method, headers, payload, ajaxKey) {
	var currentScope = this;
	var dfd = $.Deferred();

	$.cachedScript(CC.med.docHeader('scriptBase') + "SP.RequestExecutor.js").done(function () {
		method = method || 'GET';
		headers = headers || {};
		headers["Accept"] = "application/json;odata=verbose";
		if (method == "POST") {
			headers["X-RequestDigest"] = $("#__REQUESTDIGEST").val();
			headers["content-type"] = "application/json; odata=verbose";
		}

		var ajaxOptions =
		{
			url: url,
			method: method,
			headers: headers,
			success: function (data) {
				if (data.body) {
					var jsonObject = JSON.parse(data.body);
					dfd.resolve(jsonObject);
				}
				else
					dfd.resolve();
			},
			error: function (sender) {
				dfd.reject(sender);
			}
		};

		if (typeof payload != 'undefined') {
			if (!ajaxKey)
				ajaxOptions.body = JSON.stringify(payload);
			else
				ajaxOptions[ajaxKey] = JSON.stringify(payload);
		}

		var executor = new SP.RequestExecutor(CC.med.docHeader('appWebUrl'));

		executor.executeAsync(ajaxOptions);
	});
	return dfd.promise();
}

/*********
 * Get array of SharePoint lists from current site collection
 ********/
CC.Data.SPList.prototype.getSPLists = function () {
	var dfd = $.Deferred();

	this.executeRequest(CC.med.docHeader('appWebUrl') + "/_api/Sp.AppContextSite(@target)/Web/Lists?$select=Title,%20Id&@target='" + CC.med.docHeader('hostWebUrl') + "'")
	.done(function (data) {
		dfd.resolve(data.d.results);
	}).fail(function (args) {
		//var ex = new CC.Exception("ListsNotFetched", "Unable to fetch lists of current site.", args);
		CC.med.logError(args, '110', 'ListsNotFetched');

		dfd.reject(args);
	});

	return dfd.promise();
}


/********
 * Get SharePoint views collection from SharePoint list guid
 *******/
CC.Data.SPList.prototype.getSPViews = function (listID) {
	var dfd = $.Deferred();

	if (!listID) {
		var ex = new CC.Exception("ListIDBlank", "List ID can not be blank.");
		dfd.reject(ex);
	}
	else {
		this.executeRequest(CC.med.docHeader('appWebUrl') + "/_api/Sp.AppContextSite(@target)/Web/Lists(guid'" + listID + "')/Views?$select=Title,%20Id&@target='" + CC.med.docHeader('hostWebUrl') + "'")
		.done(function (data) {
			dfd.resolve(data.d.results);
		}).fail(function (args) {
			//var ex = new CC.Exception("ViewsNotFetched", "Unable to fetch views of list : " + listID, args);
			CC.med.logError(args, '111', 'ViewsNotFetched');

			dfd.reject(args);
		});
	}

	return dfd.promise();
}


/********
 * Get view fields from SharePoint list guid and corresponding view guid
 * If view guid is not provided then all fields of the lists are returned
 *******/
CC.Data.SPList.prototype.getFields = function (listID, viewID, blockGroupCheck) {
	var currentScope = this, dfd = $.Deferred();

	if (!listID) {
		var ex = new CC.Exception("ListIDBlank", "List ID can not be blank.");
		dfd.reject(ex);
	}
	else {
		//First get all list fields regardless of the view
		//This is required as we can fetch view fields from view directly, but
		//only the internal name is given, we need to map the internal name with title
		//which is available from the list
		this.executeRequest(CC.med.docHeader('appWebUrl') + "/_api/Sp.AppContextSite(@target)/Web/Lists(guid'" + listID + "')/Fields?$select=InternalName,%20Title,%20TypeAsString&$filter=Hidden%20eq%20false&@target='" + CC.med.docHeader('hostWebUrl') + "'")
		.done(function (listFields) {
			//All list fields that are not hidden, only internal name and title is fetched

			//If view id provided then fetch the fields used in view
			if (viewID) {
				currentScope.executeRequest(CC.med.docHeader('appWebUrl') + "/_api/Sp.AppContextSite(@target)/Web/Lists(guid'" + listID + "')/Views(guid'" + viewID + "')/ViewFields?$select=Items&@target='" + CC.med.docHeader('hostWebUrl') + "'")
				.done(function (viewFieldObj) {
					//Get the internal name of the view fields

					//Search the corresponding object of (Title, InternalName) in list fields
					//This is required as view does not contain any information of field display name and type
					var viewFields = $.grep(listFields.d.results, function (elem, index) {
						return $.inArray(elem.InternalName, viewFieldObj.d.Items.results) > -1;
					});



					/* Cache the current view fields */
					viewFields.unshift({ 'InternalName': '-1', 'Title': '--- Select ---', 'TypeAsString': '-1' });
					CC.Stores.ViewFields.loadData(viewFields);

					if (!blockGroupCheck) {
						currentScope.getGroupByFields(listID, viewID)
						.done(function (res) {


							viewFields.aggregations = res.aggregations;
							viewFields.viewQuery = res.viewQuery;
						})
					}

					dfd.resolve(viewFields);

					//dfd.resolve(viewFields);
				})
				.fail(function (args) {
					//fail silently
					//need to decide ac tion for this one
					CC.med.logError(args, '112', 'ViewsFieldsNotFetched');
				});
			}
			else {
				dfd.resolve(listFields.d.results);
			}

		})
		.fail(function (args) {
			//var ex = new CC.Exception("FieldsNotFetched", "Unable to fetch fields of list : " + listID + ", Views : " + viewID, args);
			CC.med.logError(args, '113', 'FieldsNotFetched');

			dfd.reject(args);
		});
	}

	return dfd.promise();
}


CC.Data.SPList.prototype.getViewDetails = function (listID, viewID) {
	var currentScope = this, dfd = $.Deferred();

	var info = {};

	if (!listID) {
		var ex = new CC.Exception("ListIDBlank", "List ID can not be blank.");
		dfd.reject(ex);
	}
	else {
		this.executeRequest(CC.med.docHeader('appWebUrl') + "/_api/Sp.AppContextSite(@target)/Web/Lists(guid'" + listID + "')/Fields?$select=InternalName,%20Title,%20TypeAsString&$filter=Hidden%20eq%20false&@target='" + CC.med.docHeader('hostWebUrl') + "'")
		.done(function (listFields) {
			if (viewID) {

			}
			else {
				info["fields"] = listFields.d.results;
			}
			dfd.resolve(info);
		})
		.fail(function (args) {
			//var ex = new CC.Exception("FieldsNotFetched", "Unable to fetch fields of list : " + listID + ", Views : " + viewID, args);
			CC.med.logError(args, '114', 'FieldsNotFetched');

			dfd.reject(args);
		});
	}

	return dfd.promise();
}

/*****************
 * Get Data from CAML Query
 * Function takes 3 arguments - list guid, caml query and fields to fetch
 ****************/
CC.Data.SPList.prototype.getDataFromQuery = function (listID, query, fields) {
	var dfd = $.Deferred();

	if (!listID) {
		var ex = new CC.Exception("ListIDBlank", "List ID can not be blank.");
		dfd.reject(ex);
	}
	else {

		var isViewFiltered = false;

		var fetchGroupedData = function (viewQuery, fieldName) {
			var isGrouped = false;
			$.each($(viewQuery), function (index, value) {
				if (value.nodeName === 'GROUPBY') {
					$.each($(value.childNodes), function (index, value) {
						if (fieldName == $(value).attr('Name')) {
							isGrouped = true;
							return false;
						}
					});
				}
				else if (!isViewFiltered && value.nodeName == "WHERE") {
					// data is filtered by view
					isViewFiltered = true;
				}
			});

			return isGrouped;
		}

		var isGroupBy = fetchGroupedData(query.ViewQuery, CC.med.getSetting('xAxis').column);

		var selectedFields = '';
		$.each(fields.split(','), function (index, element) {
		    selectedFields += CC.util.hexFormattedFieldName(element) + ',';
		});

		//fields = "OData__x0031_23_x0020__x002d__x0020_Nu,LinkTitle";

		fields = selectedFields.substring(0, selectedFields.length - 1)

		var queryPayload = {
		    'query': (!isGroupBy) ? {
		        '__metadata': { 'type': 'SP.CamlQuery' },
		        'ViewXml': '<View><Query>' + query.ViewQuery + '</Query></View>'
		    } :
			'<View><Query>' + query.ViewQuery + '</Query><Aggregations Value=\"On\">' + query.Aggregations + '</Aggregations></View>'
		};

		//Need the below headers for post request
		var headers = {
			"Accept": "application/json;odata=verbose",
			"X-RequestDigest": $("#__REQUESTDIGEST").val(),
			"content-type": "application/json; odata=verbose"
		};

		//preview query
		var prev = CC.util.isPageEdit() ? "&$top=" + CC.util.chartPrevItemLimit() : "";

		//if (!query.Aggregations) {
		if (!isGroupBy) {
			//A post request to fetch data
			this.executeRequest(CC.med.docHeader('appWebUrl') + "/_api/Sp.AppContextSite(@target)/Web/Lists(guid'" + listID + "')/getitems?$select=" + fields + "&@target='" + CC.med.docHeader('hostWebUrl') + "'" + prev, "POST", headers, queryPayload)
			.done(function (data) {
				var res = data.d.results;
				res.isViewFiltered = isViewFiltered;
				dfd.resolve(res);
			})
			.fail(function (args) {
				//var ex = new CC.Exception("DataFromQueryNotFetched", "Unable to fetch data from List : " + listID + ", query : " + query + ", Fields : " + fields, args);
				CC.med.logError(args, '115', 'DataFromQueryNotFetched');

				dfd.reject(args);
			});
		}
		else {
			//GroupBy values
			$.when(function (_fields) {
				//$xml = $(query.Aggregations)
				var keys = new Object;
				$.each($(query.Aggregations), function (index, value) {
					if ($.inArray($(value).attr('Name'), _fields) > -1) {
						/* ensure that the grouped field in Sharepoint is one of the selected field in the wizard */

						//keys.push($(value).attr('Name') + "." + $(value).attr('Type') + ".agg");
						keys[$(value).attr('Name')] = $(value).attr('Name') + "." + $(value).attr('Type') + ".agg";
					}
				});

				//CC.med.getSetting('xAxis').column
				var chartSeries = CC.med.getSetting('series');
				if (Object.keys(keys).length < chartSeries.length) {
					/* ECMA5 http://kangax.github.io/compat-table/es5/ */
					/* support for non-ECMA5 browsers https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/keys */

					/* custom GroupBy needs to be applied */
					$.each(chartSeries, function (index) {
						if (!keys.hasOwnProperty([chartSeries[index].column])) {
							/* apply custom aggregation on this field */

							/* get the fields which do not have any total function */
							var fieldType = jQuery.grep(CC.Stores.ViewFields.data.items, function (val) {
								return val.data.InternalName == chartSeries[index].column;
							});

							/* Data Type string or number */
							var fieldsType = CC.util.isFieldNumber(fieldType[0].data.TypeAsString) ? "SUM" : "COUNT";
							keys[fieldType[0].data.InternalName] = fieldType[0].data.InternalName + "." + fieldsType + ".agg";
							//Console.log(queryPayload);

							/* Update queryPayLoad */
							var queryPayLoadXml = $(jQuery.parseXML(queryPayload.query));
							////$(jQuery.parseXML(queryPayload.query)).find('Aggregations').append('<FieldRef Name="' + chartSeries[i].column + '" Type="' + fieldsType + '" />');
							//queryPayLoadXml.find('Aggregations').append('<FieldRef Name="' + chartSeries[index].column + '" Type="' + fieldsType + '" />');
							//queryPayload.query = '<View>' + $(queryPayLoadXml[0].childNodes).html() + '</View>';

						    /* Update queryPayLoad */
							//var aggregation = queryPayLoadXml.find('Aggregations')[0];
							//var fieldRef = document.createElement('FieldRef');
							//fieldRef.setAttribute('Name', chartSeries[index].column);
							//fieldRef.setAttribute('Type', fieldsType);
							//aggregation.appendChild(fieldRef);

							//queryPayload.query = (new XMLSerializer()).serializeToString(queryPayLoadXml[0]).replace(/<([a-zA-Z0-9 ]+)(?:xml)ns=\"([^\"]*)\"/g, "<$1")

						    /* Update queryPayLoad */
							var aggregation = queryPayLoadXml.find('Aggregations')[0];
							var fieldRef = (queryPayLoadXml[0]).createElement('FieldRef');
							fieldRef.setAttribute('Name', chartSeries[index].column);
							fieldRef.setAttribute('Type', fieldsType);
							aggregation.appendChild(fieldRef);

						    //var xml = (new XMLSerializer()).serializeToString(queryPayLoadXml[0]).replace(/<([a-zA-Z0-9 ]+)(?:xml)ns=\"([^\"]*)\"/g, "<$1")
							queryPayload.query = (new XMLSerializer()).serializeToString(queryPayLoadXml[0])

						}
					});
				}

				return keys;

			}(fields.split(',')), this.executeRequest(CC.med.docHeader('appWebUrl') + "/_api/Sp.AppContextSite(@target)/Web/Lists(guid'" + listID + "')/renderlistdata(@viewXml)?@viewXml='" + queryPayload.query + "'&$select=" + fields + "&@target='" + CC.med.docHeader('hostWebUrl') + "'" + prev, "POST", headers))
				//.then(getGroupbyKeys())
				.done(function (jsonKeys, data) {


					var res = JSON.parse(data.d.RenderListData);
					res.caKeys = jsonKeys;
					res.isViewFiltered = isViewFiltered;
					dfd.resolve(res);
					//return data.d;
				})
			//var fail =
			.fail(function (args) {
				//var ex = new CC.Exception("DataFromQueryNotFetched", "Unable to fetch data from List : " + listID + ", query : " + query + ", Fields : " + fields, args);
				CC.med.logError(args, '116', 'DataFromGroupByQueryNotFetched');

				dfd.reject(args);
				//return ex;
			});
			//})(getGroupbyKeys(), req);
		}
	}

	return dfd.promise();
}


/*********************
 * Get data from SP View
 * Function takes 3 arguments - list id, view id and fields
 ********************/
CC.Data.SPList.prototype.getViewData = function (listID, viewID, fields) {
	var currentScope = this, dfd = $.Deferred();

	if (!listID) {
		var ex = new CC.Exception("ListIDBlank", "List ID can not be blank.");
		dfd.reject(ex);
	}
	else {
		new function () {
			var _dfd = $.Deferred();
			/* Load ViewFields if it's not loaded already */
			if (!CC.Stores.ViewFields) {
				CC.Stores.create();

				currentScope.getFields(listID, viewID, true).done(function (_fields) { _dfd.resolve(); }).fail(function (args) { _dfd.reject(); });
			}
			else {
				_dfd.resolve();
			}
			return _dfd.promise();
		}()
		.done(function () {
			currentScope.executeRequest(CC.med.docHeader('appWebUrl') + "/_api/Sp.AppContextSite(@target)/Web/Lists(guid'" + listID + "')/Views(guid'" + viewID + "')?$select=Aggregations,%20ViewQuery&@target='" + CC.med.docHeader('hostWebUrl') + "'")
			//this.executeRequest(currentScope.appWebUrl + "/_api/Sp.AppContextSite(@target)/Web/Lists(guid'" + listID + "')/Views(guid'" + viewID + "')?@target='" + currentScope.hostWebUrl + "'")
			.done(function (viewQueryObj) {

				//currentScope.getDataFromQuery(listID, viewQueryObj.d.ViewQuery, fields, isPreview)
				currentScope.getDataFromQuery(listID, viewQueryObj.d, fields)
				.done(function (data) {
					dfd.resolve(data);
				})
				.fail(function (args) {
					//var ex = new CC.Exception("DataNotFetched", "Unable to fetch data from list : " + listID + ", Views : " + viewID, args);
					CC.med.logError(args, '117', 'DataNotFetched');

					dfd.reject(args);
				});
			})
			.fail(function (args) {
				//var ex = new CC.Exception("ViewQueryNotFetched", "Unable to fetch view query of list : " + listID + ", Views : " + viewID, args);
				CC.med.logError(args, '118', 'ViewQueryNotFetched');

				dfd.reject(args);
			})
		});
	}

	return dfd.promise();
}

/*********************
 * Get groupBy details from SP View
 * Function takes 2 arguments - list id, view id
 ********************/
CC.Data.SPList.prototype.getGroupByFields = function (listID, viewID) {
	var currentScope = this, dfd = $.Deferred();

	if (!listID) {
		var ex = new CC.Exception("ListIDBlank", "List ID can not be blank.");
		dfd.reject(ex);
	}
	else {
		this.executeRequest(CC.med.docHeader('appWebUrl') + "/_api/Sp.AppContextSite(@target)/Web/Lists(guid'" + listID + "')/Views(guid'" + viewID + "')?$select=Aggregations,%20ViewQuery&@target='" + CC.med.docHeader('hostWebUrl') + "'")
		//this.executeRequest(CC.med.docHeader('appWebUrl') + "/_api/Sp.AppContextSite(@target)/Web/Lists(guid'" + listID + "')/Views(guid'" + viewID + "')?@target='" + CC.med.docHeader('hostWebUrl') + "'")
		.done(function (viewQueryObj) {


			var res = {};
			res.aggregations = viewQueryObj.d.Aggregations;
			res.viewQuery = viewQueryObj.d.ViewQuery;
			dfd.resolve(res);
		})
		.fail(function (args) {
			//var ex = new CC.Exception("ViewQueryNotFetched", "Unable to fetch view query of list : " + listID + ", Views : " + viewID, args);
			CC.med.logError(args, '119', 'GroupByViewQueryNotFetched');

			dfd.reject(args);
		});
	}

	return dfd.promise();
}

//#region The following code has now been moved to settings.js
//CC.Data.SPList.prototype.getAppConfig = function () {
//    var dfd = $.Deferred();

//    var queryPayLoad = {
//        '__metadata': { 'type': 'SP.CamlQuery' },
//        'ViewXml': CC.util.getAppSettingViewXml(CC.med.docHeader().wpId)
//    }

//    this.executeRequest(CC.med.docHeader().appWebUrl + "/_api/Web/Lists/getbytitle('" + CC.util.getConfigListName() + "')/Items?$select=Id,%20AppPartId,%20Config&$filter=AppPartId%20eq%20'" + CC.med.docHeader().wpId + "'")
//    //this.executeRequest(CC.med.docHeader().appWebUrl + "/_api/Web/Lists/getbytitle('" + CC.util.getConfigListName() + "')/Items", "GET", null, queryPayLoad)
//		.done(function (data) {
//
//		    dfd.resolve(data.d.results);
//		})
//		.fail(function (args) {
//

//		    var ex = new CC.Exception("App Config Not Found", "Unable to fetch the app config for : " + CC.med.docHeader().wpId, args);
//		    dfd.reject(ex);
//		});

//    return dfd.promise();
//}


//CC.Data.SPList.prototype.setAppConfig = function (itemId, eTag) {
//    var dfd = $.Deferred();

//    var queryPayload = {
//        //"__metadata": { 'type': CC.util.getItemTypeForListName() }
//        "__metadata": { 'type': "SP.Data.OData__x005f_CA_x005f_ConfigListItem" }
//    };

//    //queryPayload["Config"] = JSON.stringify(CC.util.getAppSetting());
//    queryPayload["Config"] = JSON.stringify({
//        "ProviderType": CC.med.providerConfig().currentProvider.type,
//        "ProviderConfig": CC.med.providerConfig().DataSource,
//        "ChartConfig": CC.CM.ChartConfig.Configuration,
//        "Cosmetics": CC.CM.Cosmetics
//    });
//    queryPayload["AppPartId"] = CC.med.docHeader().wpId;

//

//    var header = null;
//    var url = null;
//    if (!CC.Properties.AppProperty.createProperty()) {
//        //update a new setting
//        header = {
//            "accept": "application/json;odata=verbose",
//            //"IF-MATCH": "*",
//            "IF-MATCH": eTag,
//            "content-type": "application/json;odata=verbose",
//            "X-HTTP-Method": "MERGE"
//        };
//        url = CC.med.docHeader().appWebUrl + "/_api/Web/Lists/getbytitle('" + CC.util.getConfigListName() + "')/Items(" + parseInt(itemId) + ")";
//    }
//    else {
//        //create a new setting
//        url = CC.med.docHeader().appWebUrl + "/_api/Web/Lists/getbytitle('" + CC.util.getConfigListName() + "')/Items";
//    }

//    //var header = !CC.Properties.AppProperty.createProperty() ?
//    //    {
//    //        "accept": "application/json;odata=verbose",
//    //        //"IF-MATCH": "*",
//    //        "IF-MATCH": eTag,
//    //        "content-type": "application/json;odata=verbose",
//    //        "X-HTTP-Method": "MERGE"
//    //    } : null;

//    //this.executeRequest(CC.med.docHeader().appWebUrl + "/_api/Web/Lists/getbytitle('" + CC.util.getConfigListName() + "')/Items", "POST", header, queryPayload, "data")
//    //this.executeRequest(CC.med.docHeader().appWebUrl + "/_api/Web/Lists/getbytitle('" + CC.util.getConfigListName() + "')/Items", "POST", header, queryPayload)
//    this.executeRequest(url, "POST", header, queryPayload)
//		.done(function (data) {
//		    if (data) {
//		        if (data.d.results)
//		            dfd.resolve(data.d.results[0]);
//		        else
//		            dfd.resolve(data.d);
//		    }
//		    else
//		        dfd.resolve();
//		})
//		.fail(function (args) {
//		    var ex = new CC.Exception("App Config Not Found", "Unable to fetch the app config for : " + CC.med.docHeader().wpId, args);
//		    dfd.reject(ex);
//		});

//    return dfd.promise();
//}
//#endregion