// ExtJS overrides
(function () {
	Ext.override(Ext.form.TextField, {
		afterRender: function () {
			//debugger;
			Ext.form.TextField.superclass.afterRender.call(this);
			// Add help plugin for label
			var disableFocus = this.disableFocus;
			if(this.ccTip){
				$(this.el.dom)
				.focus(function(){
					if(!disableFocus)
						$(this).tooltipster('show');
				})
				.blur(function(){
					if(!disableFocus)
						$(this).tooltipster('hide');
				})
				.attr({
					"title": this.ccTip
				})
				//$(this.el.dom).addClass(util.tipCls)
				.addClass(util.tipCls);
			}
		}
	});
	
	Ext.override(Ext.form.ComboBox, {
		afterRender: function () {
			var disableFocus = this.disableFocus;
			var ccTip = this.ccTip;
			this.ccTip = '';
			
			Ext.form.ComboBox.superclass.afterRender.call(this);
			// Add help plugin for label
			
			if(ccTip){
				$(this.el.parent().dom)
				.focus(function(){
					if(!disableFocus)
						$(this).tooltipster('show');
				})
				.blur(function(){
					if(!disableFocus)
						$(this).tooltipster('hide');
				})
				.attr({
					"title": ccTip
				})
				.addClass(util.tipCls);
			}
		}
	});
	
	Ext.override(Ext.form.Checkbox, {
		afterRender: function () {
			//debugger;
			var disableFocus = this.disableFocus;
			var ccTip = this.ccTip;
			this.ccTip = '';
			
			Ext.form.Checkbox.superclass.afterRender.call(this);
			// Add help plugin for label
			
			if(ccTip){
				$(this.el.parent().dom)
				.focus(function(){
					if(!disableFocus)
						$(this).tooltipster('show');
				})
				.blur(function(){
					if(!disableFocus)
						$(this).tooltipster('hide');
				})
				.attr({
					"title": ccTip
				})
				.addClass(util.tipCls);
			}
		}
	});
	
	Ext.override(Ext.form.Label, {
		afterRender: function () {
			//debugger;
			var disableFocus = this.disableFocus;
			var ccTip = this.ccTip;
			this.ccTip = '';
			
			Ext.form.Label.superclass.afterRender.call(this);
			// Add help plugin for label
			
			if(ccTip){
				$(this.el.dom)
				.focus(function(){
					if(!disableFocus)
						$(this).tooltipster('show');
				})
				.blur(function(){
					if(!disableFocus)
						$(this).tooltipster('hide');
				})
				.attr({
					"title": ccTip
				})
				.addClass(util.tipCls);
			}
		}
	});
	
	Ext.override(Ext.cux.form.SpinnerField, {
		afterRender: function () {
			var disableFocus = this.disableFocus;
			var ccTip = this.ccTip;
			this.ccTip = '';
			
			Ext.cux.form.SpinnerField.superclass.afterRender.call(this);
			// Add help plugin for label
			
			if(ccTip){
				$(this.el.parent().dom)
				.focus(function(){
					if(!disableFocus)
						$(this).tooltipster('show');
				})
				.blur(function(){
					if(!disableFocus)
						$(this).tooltipster('hide');
				})
				.attr({
					"title": ccTip
				})
				.addClass(util.tipCls);
			}
		}
	});
})();