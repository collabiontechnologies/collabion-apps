CC.Tabs.Tab.Number = Ext.extend(CC.Tabs.Base, {
    create: function () {
        
		var numberContent = {
			id: 'numberContent',
			border: false,
			autoScroll: true,
			style: {
					//'padding-left': '30px',
					'padding-left': '20px',
					'padding-top': '10px'
				},			
			layout: 'table',
			layoutConfig: {
				tableAttrs: {
					style: {
						"width": "580px",
						//border: 'none',
						//margin: '20px 0px 0px 30px'
					}
				},			
				columns: 2 
			},
			items: [{
				xtype: 'panel',
				//labelWidth: 74,
				id: 'prefixSufix',
				layout: 'table',
				layoutConfig: { 
					tableAttrs: {
						style: {
							'width': '100%',
							'border': 'none',
							'padding-right': '20px'//,
							//'height': '103px'
						}
					},
					columns: 2
				},
				//defaults: { border: false },
				items: [{
					xtype: 'label',
					text: 'Prefix and Suffix',
					cls: 'subHead',
					colspan: 2
				},{
					xtype: 'label',
					//text: 'Prefix'
					html: 'Prefix',
					cls: ' x-form-item'
				},
				{
					xtype: 'textfield',
					id: 'txtPrefix'//,
					//style: 'text-align: center;'
				},{
					xtype: 'label',
					html: 'Suffix',
					cls: ' x-form-item'
				},
				{
					xtype: 'textfield',
					id: 'txtSuffix'
				}]
			},{
				xtype: 'panel',
				id: 'roundNo',
				layout: 'table',
				layoutConfig: { 
					tableAttrs: {
						style: {
							width: '100%',
							border: 'none',
							'padding-left': '20px',
							'height': '10px'
						}
					},				
					columns: 3
				},
				defaults: { border: false },
				items: [{
					xtype: 'label',
					text: 'Rounding Numbers',
					cls: 'subHead',
					colspan: 3
				},{
					xtype: 'label',
					text: 'No. of decimal places',
					cls: ' x-form-item',
					colspan: 2
				},{
					xtype: 'spinnerfield',
					id: 'spnrDcml',
					minValue: 0,
					maxValue: 100,
					width: 108,
					rightLeftBtns: true,
					ccTip: 'Spinner'
				}]
			},{
				xtype: 'panel',
				//labelWidth: 74,
				id: 'decimalStep',
				layout: 'table',
				layoutConfig: { 
					tableAttrs: {
						style: {
							'width': '100%',
							'border': 'none',
							'padding-right': '20px'//,
							//'height': '103px'
						}
					},
					columns: 2
				},
				//defaults: { border: false },
				items: [
				{
					xtype: 'checkbox',
					id: 'chkDecimal',
					width: 15,
					style: {
						'margin-top': '-1px',
						'vertical-align': 'middle'
					}
				},
				{
					xtype: 'label',
					cls: 'subHead',
					text: 'Number Format',
					style: {
						'margin-left': '7px',
						'vertical-align': 'middle'
					}
				}
				// {
					// xtype:'toggleField',
					// //id: 'tglLabels',
					// checked: true,
					// boxLabel: "Number Format",
					// boxLabelCss: "subHead",
					// textLeft: true,
					// style: {
						// "margin-Bottom": '0px',
						// "width": '100%'
					// },
					// //width: 250,
					// colspan: 2,
					// listeners:{
						// scope: this,
						// 'toggle': function(ele){
							// //var nxt = $(ele.el.dom).closest("tr").next();
							// var nxt = $(ele.el.dom).closest("tr");
							// //while(nxt.length > 0){
							// while((nxt = nxt.next()).length > 0){
								// if(!ele.checked){
									// nxt.find("input,label").addClass("x-item-disabled");
									// nxt.find("input,label").prop("disabled", true);
								// }
								// else{
									// nxt.find("input,label").removeClass("x-item-disabled");
									// nxt.find("input,label").prop("disabled", false);
								// }
								// //nxt = nxt.next();
							// }
						// }
					// }
				// }
				,{},
				{
					xtype: 'fieldset',
					items: [{ border: false,

						xtype: 'radiogroup',
						cls: 'x-form-item',
						itemCls: 'x-radiogroup',
						hideLabel: true,
						columns: 1,
						items: [{
							id: 'rdRegular',
							name: 'rb-col',
							columnWidth: 1,
							boxLabel: 'Regular (e.g., 100,213.45)',
							inputValue: 1,
							// style:{"margin-top:3px;"},
							checked: true
						}, {
							id: 'rdEurope',
							name: 'rb-col',
							columnWidth: 1,
							boxLabel: 'European (e.g., 100.213,45)',
							inputValue: 2,
							// style:{"margin-top:3px;"}
						}],
						listeners: {
							afterrender: function(){
								// $('#rdRegular').parent().css('top','2px');
								// $('#rdEurope').parent().css('top','5px');
							}
						}

					}]
				}]
			},{
				xtype: 'panel',
				id: 'scaleNo',
				layout: 'table',
				layoutConfig: { 
					tableAttrs: {
						style: {
							width: '100%',
							border: 'none',
							'padding-left': '20px'//,
							//'height': '103px'
						}
					},
					columns: 3
				},
				defaults: { border: false },
				items: [
				// {
					// xtype:'toggleField',
					// checked: true,
					// boxLabel: "Scaling Numbers",
					// boxLabelCss: "subHead",
					// textLeft: true,
					// style: {
						// "margin-Bottom": '0px',
						// "width": '100%'
					// },
					// //width: 240,
					// colspan: 3,
					// listeners:{
						// scope: this,
						// 'toggle': function(ele){
							// //var _dom = ele.el.dom;
							// //var nxt = $(ele.el.dom).closest("tr").next();
							// var nxt = $(ele.el.dom).closest("tr");
							// //while(nxt.length > 0){
							// while((nxt = nxt.next()).length > 0){
								// if(!ele.checked){
									// nxt.find("input,label").addClass("x-item-disabled");
									// nxt.find("input,label").prop("disabled", true);
								// }
								// else{
									// nxt.find("input,label").removeClass("x-item-disabled");
									// nxt.find("input,label").prop("disabled", false);
								// }
								// //nxt = nxt.next();
							// }
						// }
					// }
				// }
				{
					xtype: 'checkbox',
					id: 'chkScaling',
					width: 15,
					style: {
						'margin-top': '-1px',
						'vertical-align': 'middle'
					}
				},{
					xtype: 'label',
					text: 'Scaling Numbers',
					cls: 'subHead',
					colspan: 2,
					style: {
						//'margin-left': '10px',
						'vertical-align': 'middle'
					}
				}
				,{},{
					xtype: 'label',
					text: 'Scales to be used',
					cls: ' x-form-item'
				},{
					xtype: 'textfield',
					id: 'txtScales'
				},{},{
					xtype: 'label',
					text: 'Values of each scale',
					cls: ' x-form-item'
				},{
					xtype: 'textfield',
					id: 'txtScalesVal'
				}]
			}]
		};
		
		return {
            id: 'tabNumber',
            title: 'NUMBER',
			header: false,
			iconCls: 'x-numbers-icon',
            defaults: { autoScroll: false },
            items: [numberContent]
        }
    },
	
	validationStep: function () {
        var valid = true;
        return valid;
    },
	
	activateStep: function () {
		this.disabled = false;
	}
});
