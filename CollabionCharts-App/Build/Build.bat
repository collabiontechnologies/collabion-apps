@echo off
cls
echo Loading MSBuild commands...
call VsMSBuildCmd.bat
echo Done.
echo.
echo Reading version information...
cscript /NoLogo read-version.vbs "..\CollabionCharts-App\AppManifest.xml"
echo Done.
echo.
set /p prodVersion=<AppVersion
if "%prodVersion%"=="" set prodVersion=1.0.0.0
echo.
set /p newver=Please enter the new version (Currently using: %prodVersion%): 
if not "%newver%"=="" (
	set prodVersion=%newver%
	echo %newver%>AppVersion
)
echo.
echo Updating new version...
cscript /NoLogo write-version.vbs "..\CollabionCharts-App\AppManifest.xml"
echo Done.
echo.
echo.
echo Starting package creation process...
msbuild /t:Build /t:CreatePackage  /p:Configuration=Release;PackageDependsOn=Build;IsDebugging=False "..\CollabionCharts-App\CollabionCharts-App.csproj"
set BUILD_STATUS=%ERRORLEVEL%

if %BUILD_STATUS%==0 (
echo Done.
explorer.exe /select,"..\CollabionCharts-App\bin\Release\app.publish\%prodVersion%\CollabionCharts-App.app"
) else (
echo Build Failed.
)
echo.
pause