﻿CC.Tabs.Active.prototype.labels = {
    componentLoaded: false,
    activate: function () {
        if (!Ext.getCmp('txtCrtTitle').getValue()) {
            //should be called on change of DataSource as well

            this.componentLoaded = false;

            var chartCosmetics = CC.med.getSetting('chart');
            //var chartPlot = CC.med.getSetting('dataPlot');

            Ext.getCmp('chkLabels').setValue(chartCosmetics.showLabels);
            Ext.getCmp('chkValues').setValue(chartCosmetics.showValues);
            Ext.getCmp('chkLegends').setValue((CC.util.getChartSeries(CC.med.getSetting('cType')) > 1 && !chartCosmetics.showLegend) ? true : chartCosmetics.showLegend);

            Ext.getCmp('txtCrtTitle').setValue(chartCosmetics.caption);
            Ext.getCmp('txtSubTitle').setValue(chartCosmetics.subCaption);
            Ext.getCmp('txtXTitle').setValue(chartCosmetics.xAxisname);

            //if (CC.med.getSetting('cType') == 3) {
            //if(CC.util.isCombinationChart()){
            if (CC.util.isDualYAxis()) {
                //combination chart
                Ext.getCmp('txtYTitle').setValue(chartCosmetics.pYAxisName);
                Ext.getCmp('txtScndYTitle').setValue(chartCosmetics.sYAxisName);
            }
            else {
                Ext.getCmp('txtYTitle').setValue(chartCosmetics.yAxisName);
            }

            this.componentLoaded = true;
        }
        Ext.getCmp('txtScndYTitle').setDisabled(!CC.util.isDualYAxis());
        Ext.getCmp('lblScndYTitle').setDisabled(!CC.util.isDualYAxis());

        CC.util.generateTip();
    },

    update: function (id, val) {
        switch (id) {
            case "chkLabels":
                CC.med.updateSetting('chart', val, 'showLabels');
                break;
            case "chkValues":
                CC.med.updateSetting('chart', val, 'showValues');
                break;
            case "chkLegends":
                CC.med.updateSetting('chart', val, 'showLegend');
                break;
            case "txtCrtTitle":
                CC.med.updateSetting('chart', val, 'caption');
                break;
            case "txtSubTitle":
                CC.med.updateSetting('chart', val, 'subCaption');
                break;
            case "txtXTitle":
                CC.med.updateSetting('chart', val, 'xAxisname');
                break;
            case "txtYTitle":
                CC.med.updateSetting('chart', val, CC.util.getChartSeries(CC.med.getSetting('cType')) == 3 ? 'pYAxisName' : 'yAxisName');
                break;
            case "txtScndYTitle":
                CC.med.updateSetting('chart', val, 'sYAxisName');
                break;
        }
    }
}