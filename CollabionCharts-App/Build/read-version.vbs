option explicit
dim args, sFilePath

set args = WScript.Arguments

sFilePath=args(0)

ReadVersion(sFilePath)

sub ReadVersion(sFilePath)
	dim oxml, xRoot, node, oAttribute, AppVersion
	
	set oxml = CreateObject("Msxml2.DOMDocument.6.0")
	

	call oxml.load(sFilePath)
	
	set xRoot=oxml.documentElement
	AppVersion = xroot.getAttribute("Version")
	
	if Err.Number <> 0 Then
		WScript.Quit(errorNumber)
	end if	

	set oxml = nothing
	set xRoot = nothing
	
	dim fs,tfile
	set fs=CreateObject("Scripting.FileSystemObject")
	set tfile=fs.CreateTextFile("AppVersion", True)
	tfile.Write(AppVersion)
	tfile.close
	set tfile=nothing
	set fs=nothing	
	
end sub