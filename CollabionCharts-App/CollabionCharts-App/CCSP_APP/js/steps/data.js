
CC.Tabs.Tab.Data = Ext.extend(CC.Tabs.Base, {
	//config: 0,
	//currentStep: 0,

	create: function () {
		// Create wizard steps
		var pnlInfo = this.createSteps([
			{
				step: 'Configurations', leaf: [new CC.Tabs.Tab.Data.DataSource,
											   new CC.Tabs.Tab.Data.DataFields]
			}
		]);
		this.steps = pnlInfo.steps;

		// Content panel for steps
		this.contentPan = new Ext.Panel({
			id: 'content-panel',
			activeItem: CC.Wizard.tabpanel.currentStep,
			border: false,
			region: 'center',
			layout: 'card',
			autoScroll: false,
			defaults: {
				border: false,
				farme: true
			},
			items: pnlInfo.panels
		});

		this.level = 1;
		CC.Wizard.tabpanel.config.level = 0;

		// Navigation tree panel
		this.navigationPan = new Ext.tree.TreePanel({
			region: 'west',
			width: 165,
			id: 'tpDataSource',
			split: false,
			border: false,
			autoScroll: true,
			expanded: true,
			lines: false,
			useArrows: true,
			rootVisible: false,
			bodyStyle: { backgroundColor: '#FFFFFF', paddingTop: '10px' },
			style: {
				'border-right': '1px solid #cbcbcb'
			},
			root: new Ext.tree.AsyncTreeNode(pnlInfo.nodes),
			isActivated: false,
			listeners: {
				scope: this,
				append: function (tree, parent, node) {
					if (node.id == CC.Wizard.tabpanel.currentStep.toString()) {
						node.select.defer(100, node);
					}
				},
				afterrender: function (s) {
					//this.setVisibility(1);
					//this.steps[0].setVisibility(0);
					//
					//this.setVisibility(this.level);
					this.toggleEnable(false);

					//this.activateStep();

					//
					//var record = this.navigationPan.getStore().getNodeById('item_id');
					//this.navigationPan.getSelectionModel().select(record)
				},
				afterlayout: function () {
					//

					if (!this.isActivated) {
						this.isActivated = true;

						//Select the Data Source view by default
						this.navigationPan.getSelectionModel().select(this.navigationPan.getRootNode().childNodes[0]);
						this.activateStep();
					}
				},
				//activate: function () {
				//
				//},
				//beforechildrenrendered: function(){
				//
				//},
				//load: function(node){
				//
				//},
				click: this.treeBeforeDeactive
			}
		});



		//old code
		return {
			id: 'tabData',
			title: CC.msgs.tb111,
			//iconCls: 'x-data-icon',
			header: false,
			region: 'center',
			split: true,
			layout: 'border',
			items: [this.navigationPan, this.contentPan]
		}
	},

	// Private: Generating wizard's steps
	createSteps: function (steps) {
		var pnl = [], stp = [];
		var nds = { expanded: true, children: [] };
		for (var i = 0; i < steps.length; i++) {

			//

			var len = pnl.length.toString();
			var n = { leaf: true, disabled: false }
			if (steps[i].step instanceof Object) {
				//var p = this.createWizardPanel(steps[i].step);
				var p = CC.Wizard.tabpanel.createTabPanelItem(steps[i].step);
				//var p = steps[i].step.create();
				p.name = len + '-step';
				n.id = len;
				n.text = p.caption || p.title;
				stp.push(steps[i].step);
				pnl.push(p);
			}
			else {
				n.id = (-(i + 1)).toString();
				n.text = steps[i].step;
				n.disabled = false;
			}
			if (steps[i].leaf) {

				n.leaf = false;
				n.expanded = true;
				n.children = [];
				for (var k = 0; k < steps[i].leaf.length; k++) {
					len = pnl.length.toString();
					//p = this.createWizardPanel(steps[i].leaf[k]);
					p = CC.Wizard.tabpanel.createTabPanelItem(steps[i].leaf[k]);
					//p = steps[i].leaf[k].create();
					p.name = len + '-step';
					n.children[k] = { id: len, text: p.caption || p.title, leaf: true, disabled: false }
					stp.push(steps[i].leaf[k]);
					pnl.push(p);
				}
			}
			nds.children.push(n);
		}
		stp[0].level = 1;

		var treeNodes = { steps: stp, panels: pnl, nodes: nds };
		//Hide leaf node
		treeNodes.nodes = treeNodes.nodes.children[0];
		return treeNodes;
	},

	// Set steps visibility
	// level - visibility level
	setVisibility: function (level) {
		if (CC.Wizard.tabpanel.config.level === level) return;
		CC.Wizard.tabpanel.config.level = level;

		//

		for (var i = 0; i < this.steps.length; i++) {
			var node = this.navigationPan.getNodeById(i);
			//this.navigationPan.getNodeById(i)[this.steps[i].level <= level ? 'enable' : 'disable']();
			node[this.steps[i].level <= level ? 'enable' : 'disable']();
			node.getUI()[this.steps[i].level <= level ? 'show' : 'hide']();
		}

	},

	toggleEnable: function (enable) {
		var toggle = enable ? "enable" : "disable";
		for (var i = 1; i < this.steps.length; i++)
			this.navigationPan.getNodeById(i)[toggle]();
		for (var k = 1; k < CC.Wizard.tabpanel.steps.length; k++)
			CC.Wizard.tabpanel.steps[k].setDisabled(!enable);
		Ext.getCmp('ccView').setDisabled(!enable);
	},

	// Tree navigation
	treeBeforeDeactive: function (n) {
		//
		var sn = this.navigationPan.selModel.selNode || {};
		if (n.id != sn.id) {
			var next = parseInt(n.id);
			//check if moving to the previous step from 'select fields' is true
			if (this.currentStep == 1 && next == 0) {
				this.steps[this.currentStep].isBackToSelectSource = true;
			}

			if (next < 0 || !this.step_onValidation(this.currentStep)) {
				return false;
			}
			//if (this.steps[this.currentStep].confirmation) {
			//    this.confirm(this.treeDeactive, this.steps[this.currentStep].confirmationMsg, next);
			//}
			//else {
			this.treeDeactive(next)
			//}
		}
		return false;
	},

	treeDeactive: function (next) {
		this.step_onDeactive(CC.Wizard.tabpanel.currentStep);
		this.treeActivateNewStep(next);
	},

	treeActivateNewStep: function (next) {
		this.currentStep = next;
		this.contentPan.layout.setActiveItem(next);
		this.step_onActive(next);
		this.navigationPan.getNodeById(next).select();
	},

	// Called when user comes to step. Need activateStep method into step object
	step_onActive: function (index) {



		/* track tree node visit */
		var curNode = index == 0 ? '' : '0';
		var prevNode = index == 0 ? '0' : '';
		CC.track.prototype.activate(curNode, prevNode);

		this.steps[index].activateStep();
	},

	// Called when user leaves step. Need deactivate step method into step object
	step_onDeactive: function (index) {
		this.steps[index].deactivateStep();
	},

	// Validation step. Need validationStep method into step object
	step_onValidation: function (index) {
		//return this.steps[index].validationStep();
		return true;
	},

	validationStep: function () {
		return true;
	},

	activateStep: function () {
		//
		//if (!Ext.getCmp('tpDataSource').getSelectionModel().selNode || Ext.getCmp('tpDataSource').getSelectionModel().selNode.id == "0") {
		//    //trigger Data Source activate
		//    this.disabled = false;
		//}
		//else {
		//    //trigger the corresponding Tree Panel item
		//    Ext.getCmp('tabData').items.items[1].items.items[CC.util.convertToInt(Ext.getCmp('tpDataSource').getSelectionModel().selNode.id)].activateStep();
		//}

		if (Ext.getCmp('tpDataSource').getSelectionModel().selNode) {

			//trigger the corresponding Tree Panel item
			//Ext.getCmp('tabData').items.items[1].items.items[CC.util.convertToInt(Ext.getCmp('tpDataSource').getSelectionModel().selNode.id)].activateStep();
			var selectedItemIndex = parseInt(Ext.getCmp('tpDataSource').getSelectionModel().selNode.id);

			var prevTab = CC.Wizard.tabpanel.oldTabIndex ? CC.Wizard.tabpanel.oldTabIndex : CC.Wizard.tabpanel.currentTab.index;
			var curTab = selectedItemIndex == 0 ? '' : '0';
			CC.track.prototype.activate(curTab, prevTab);

			CC.Wizard.tabpanel.oldTabIndex = curTab;

			Ext.getCmp('tabData').items.items[1].items.items[selectedItemIndex].activateStep();


		}

		//trigger Data Source activate
		this.disabled = false;
	}
});

CC.Tabs.Tab.Data.DataSource = Ext.extend(CC.Tabs.Base, {
	create: function () {
		var dsContent = {
			collapsible: false,
			region: 'center',
			border: false,
			autoScroll: false,
			items: [{
				xtype: 'panel',
				id: 'pnlDataSource',
				//labelWidth: 74,
				border: false,
				layout: 'table',
				layoutConfig: { columns: 2 },
				autoScroll: false,
				items: [{
					xtype: 'label',
					id: 'lblDataSource',
					cls: 'subHead',
					text: CC.msgs.ds121,
					colspan: 2
				}, {
					xtype: 'label',
					text: CC.msgs.ds122,
					//cls: ' x-form-item',
					cls: 'x-form-item-sub',
					style: {
						'display': 'inherit'
					}
				}, {
					id: 'ccList',
					//xtype: 'combo',
					xtype: 'cc:combo',
					width: 215,
					//mode: 'local',
					//listWidth: 200,
					//ccTip: 'Show a box with legend item for each data series. Legend helps viewers in associating dataplots with their respective series. A legend item contains the name of a data series and a small icon ( legend key ) denoting the type of dataplot ( like column, line, area etc. ). Legend is not available in Single Series charts except pie and doughnut charts.',
					//fieldLabel: CC.msgs.ds122,
					store: CC.Stores.Lists,
					displayField: 'Title',
					valueField: 'Id',
					//triggerAction: 'all',
					//editable: false,
					listeners: {
						//scope: this,
						afterrender: function () {

							CC.med.subscribe({
								id: this.id,
								//group: CC.util.target().spList,
								tab: CC.util.tabs().data,
								type: CC.util.xTypes(this.getXType())
							});
						},
						beforeselect: function (c, r, i) {
							if (i > 0) {

								if (r.data.Id != c.getValue()) {
									Ext.getCmp("ccView").clearValue();
								}
								else {
									/* do not throw error */
									this.blockError = true;
									this.cancelSelection = true;
								}
							}
							else {
								/* don't fire the event for same selection */
								this.cancelSelection = true;
								this.overriteValue = c.getValue();
							}
						},
						select: function (c, r, i) {
							if (!this.cancelSelection) {
								//CC.Wizard.tabpanel.tabs[0].toggleEnable(false);

								////this.populateView(r.data.Id);
								//CC.Tabs.Activate.data.populateView(r.data.Id);
								//CC.med.publish(this.id, r.data.Id);
								CC.med.publish(this.id, { listId: r.data.Id });
								CC.util.removeCssClass(c.el.dom, [CC.util.getErrorCls()]);
							}
							else if(!this.blockError) {
								CC.util.markComboError(this);
							}
							this.blockError = false;
						},
						beforequery: function (queryEvent) {
							/* If the empty Select has not been added then add it */
							if (queryEvent.combo.store.data.items[0].data.Id !== '-1') {
								var recordType = Ext.data.Record.create(
									{ name: "Id", mapping: "Id" },
									{ name: "Title", mapping: "Title" }
								);

								var newRecord = new recordType({ 'Id': '-1', 'Title': '--- Select ---' });
								queryEvent.combo.store.data.insert(0, newRecord);
								//queryEvent.combo.store.commitChanges();

								/* Without binding the new item was not showing up */
								queryEvent.combo.bindStore(CC.Stores.Lists);
							}
						}
					}
				}, {
					xtype: 'label',
					text: CC.msgs.ds123,
					//cls: ' x-form-item',
					cls: 'x-form-item-sub',
					style: {
						'display': 'inherit'
					}
				}, {
					id: 'ccView',
					//xtype: 'combo',
					xtype: 'cc:combo',
					width: 215,
					//listWidth: 200,
					//fieldLabel: CC.msgs.ds123,
					//mode: 'local',
					//editable: false,
					store: CC.Stores.Lists,
					displayField: 'Title',
					valueField: 'Id',
					//triggerAction: 'all',
					listeners: {
						//scope: this,
						afterrender: function () {
							CC.med.subscribe({
								id: this.id,
								//group: CC.util.target().spView,
								tab: CC.util.tabs().data,
								type: CC.util.xTypes(this.getXType())
							});
						},
						select: function (c, r, i) {
							if (!this.cancelSelection) {
								//this.selectFields(r.data.Id, true);
								//CC.Tabs.Activate.data.selectFields(r.data.Id, true);
								CC.med.publish(this.id, { id: r.data.Id, auto: true });
								CC.util.removeCssClass(c.el.dom, [CC.util.getErrorCls()]);
								CC.util.removeCssClass(Ext.getCmp('ccList').el.dom, [CC.util.getErrorCls()]);
							}
							else if(!this.blockError) {
								CC.util.markComboError(this);
							}
							this.blockError = false;
						},
						beforeselect: function (c, r, i) {
							if (i == 0) {
								/* don't fire the event for same selection */
								this.cancelSelection = true;
								this.overriteValue = c.getValue();
							}
							else {
								if (r.data.Id == c.getValue()) {
									/* don't throw error */
									this.blockError = true;
									this.cancelSelection = true;
								}
							}
						}
					}
				}
				]
			}]
		};

		return {
			id: 'DataSource',
			title: CC.msgs.ds124,
			header: false,
			defaults: { autoScroll: false },
			//items: [dsContent, {
			//    id: 'dsHelp',
			//    xtype: 'CC:HelpIcon',
			//    helpText: 'abcd'
			//}],
			items: [dsContent],
			tools: [{
				id: 'help'//,
				//handler: CC.Data.Var.step_onHelpClick
			}]
		};
	},

	//connectSPList: function () {

	//    for (var k = 0; k < CC.Wizard.tabpanel.steps.length; k++) {
	//        CC.Wizard.tabpanel.steps[k].setDisabled(false);
	//        //CC.Wizard.tabpanel.items.items[k].setDisabled(false);
	//    }

	//    //
	//    //this.setVisibility(3);
	//    CC.Wizard.tabpanel.tabs[0].setVisibility(3, this.level);
	//},

	activateStep: function () {
		//
		//currently being called thrice. Need to inspect
		//CC.Tabs.Activate.data.activate();
		CC.med.publish(CC.util.tabsActivate('data'));
	}
});

CC.Tabs.Tab.Data.DataFields = Ext.extend(CC.Tabs.Base, {
	create: function () {

		//this.config = {
		//    ChartIconsPath: 'assets/'
		//};
		var axis_store = new Ext.data.ArrayStore({
			fields: CC.med.cmChart().getAxisChartTypeFields(),
			data: CC.med.cmChart().getAxisChart(0)
		});

		var chart_store = new Ext.data.ArrayStore({
			fields: CC.med.cmChart().getAxisChartTypeFields(),
			data: CC.med.cmChart().getAxisChart(1)
		});

		var chrtAxTemplate = new Ext.XTemplate(
			'<table cellspacing="0">',
			'<tpl for=".">',
				//'<tr class="chrtAxTemplate tooltip" title="{name}">',
				'<tr class="chrtAxTemplate">',
					'<td class="tab-fields-wrapLeft"><div class="hover-btn" onmouseover="javascript:CC.util.cancelFadeOut(event);" style="width: 20px; height: 20px;"><span class="all-images" style="height: 9px; width: 13px; background-position: {pos}px -134px;"/></div></td>',
					'<td style="padding-top: 1px; margin-right: 3px;" class="tab-fields-wrapLeft x-form-item"><span class="hover-btn" onmouseover="javascript:CC.util.cancelFadeOut(event);" style="">{name}</span></td>',
				'</tr>',
			'</tpl>',
			'</table>'
		);

		this.axisView = new Ext.DataView({
			id: 'axisView',
			store: axis_store,
			tpl: chrtAxTemplate,
			//itemSelector: 'div.cc-thumb-wrap',
			itemSelector: 'tr.chrtAxTemplate',
			overClass: 'chrtAxTemplate-over-noborder',
			singleSelect: true,
			hidden: true,
			width: 118,
			listeners: {
				afterrender: function () {
					CC.med.subscribe({
						id: this.id,
						//group: CC.util.target().calloutPanelAxis,
						tab: CC.util.tabs().fields,
						type: CC.util.xTypes(this.getXType())
					})
				},
				//selectionchange: this.switchAxisView,
				//selectionchange: CC.Tabs.Activate.fields.switchAxisView,
				selectionchange: function () {
					CC.med.publish(this.id, this.getSelectedRecords());
				}
				//scope: this
			}
		});

		this.chartView = new Ext.DataView({
			id: 'chartView',
			store: chart_store,
			tpl: chrtAxTemplate,
			itemSelector: 'tr.chrtAxTemplate',
			//overClass: 'cc-x-view-over',
			overClass: 'chrtAxTemplate-over-noborder',
			autoScroll: false,
			singleSelect: true,
			hidden: true,
			width: 94,
			listeners: {
				afterrender: function () {
					CC.med.subscribe({
						id: this.id,
						//group: CC.util.target().calloutPanelChart,
						tab: CC.util.tabs().fields,
						type: CC.util.xTypes(this.getXType())
					})
				},
				//selectionchange: this.switchChartView,
				//selectionchange: CC.Tabs.Activate.fields.switchChartView,
				selectionchange: function () {
					CC.med.publish(this.id);
				}
				//scope: this
			}
		});

		this.confirmation = new Ext.Panel({
			id: 'pnlConfirmation',
			//title: 'Confirm',
			width: 92,
			layout: 'column',
			layoutConfig: { columns: 5 },
			hidden: true,
			items: [{
				html: '<span class="all-images cnfIcon cnfBtn buttonYes"></span>',
				// width: 13,
				// height: 9
			}, {
				// html: '<button id="btnYes">Yes</button>',
				html: '<span id="btnYes" class="cnfBtn buttonYes x-form-item" onclick="javascript:CC.med.publish(CC.util.target(\'btnYes\'));">' + CC.msgs.df131 + '</span>',
				width: 30
			}, {
				html: '&nbsp;',
				width: 3,
				style: 'cursor: default;'
			}, {
				html: '<span class="all-images cnfIcon cnfBtn buttonNo"></span>',
				// width: 13,
				// height: 9
			}, {
				// html: '<button id="btnNo">No</button>',
				html: '<span id="btnNo" class="cnfBtn buttonNo x-form-item">' + CC.msgs.df132 + '</span>',
				width: 30
			}],
			listeners: {
				//scope: this,
				afterlayout: function () {
					$("#btnNo").click(function () {
						CC.util.closePopup(100);
					});

					$(".buttonNo").click(function () {
						CC.util.closePopup(100);
					});
					$(".buttonYes").mouseover(function () {
						$('.buttonYes')[0].style.backgroundPositionX = '-75px'
						//$('.buttonYes')[1].style.color = '#49657B';
					});
					$(".buttonYes").mouseout(function () {
						$('.buttonYes')[0].style.backgroundPositionX = '-62px'
						//$('.buttonYes')[1].style.color = 'darkgrey';
					});
					$(".buttonNo").mouseover(function () {
						$('.buttonNo')[0].style.backgroundPositionX = '-102px'
						//$('.buttonNo')[1].style.color = '#49657B';
					});
					$(".buttonNo").mouseout(function () {
						$('.buttonNo')[0].style.backgroundPositionX = '-89px'
						//$('.buttonNo')[1].style.color = 'darkgrey';
					});

					//Console.log("Layout called for btnNo");
				},
				afterrender: function () {
					//var deleteRow = function (sender) {
					//$('#btnYes').off('click');
					//$("#btnYes").click(function () {
					//    CC.med.publish(CC.util.target().btnYes);
					//});

					CC.med.subscribe({
						id: CC.util.target('btnYes'),
						tab: CC.util.tabs().fields,
						type: CC.util.xTypes(this.getXType())
					})
				}
			}
		});

		this.axis = new Ext.Panel({
			header: false,
			renderTo: 'pnlCallout',
			items: [this.axisView, this.chartView, this.confirmation],
			listeners: {
				afterlayout: function () {
					$('.hover-btn').mouseover(function () {
					    var parentEle = $('.chrtAxTemplate-over-noborder');
					    if (!$(parentEle).hasClass('x-view-selected')) {
					        if (parentEle.length > 0) {
					            parentEle[0].children[0].firstChild.firstChild.style["background-position-y"] = "-143px";
					            parentEle[0].children[1].firstChild.style.color = '#49657B';
					        }
					    }
					});
					$('.hover-btn').mouseout(function () {
						if ($(this.parentElement.parentElement).hasClass('x-view-selected'))
							return;
						if ($(this).find('span').length > 0) {
							this.firstChild.style["background-position-y"] = "-143px";

							/* previous disabled view */
							//this.firstChild.style["background-position-y"] = "-134px";
							//this.parentElement.parentElement.lastChild.lastChild.style.color = '#AAAAAA';
						}
						else {
							this.parentElement.parentElement.firstChild.firstChild.firstChild.style["background-position-y"] = "-143px";

							/* previous disabled view */
							//this.parentElement.parentElement.firstChild.firstChild.firstChild.style["background-position-y"] = "-134px";
							//this.style.color = '#AAAAAA';
						}
					});
					//$('.hover-btn').click(function () {

					//})
				}
			}
		});

		var fields = new Ext.Container({
			id: 'cntrParent',
			layout: 'vbox',
			layoutConfig: {
				align: 'stretch',
				pack: 'start'
			},
			items: [
				{
					xtype: 'panel',
					autoScroll: true,
					style: {
						//'padding-left': '35px',
						'padding-left': '20px',
						'padding-top': '20px',
						'padding-bottom': '5px'
					},
					border: false,
					items: [
					{
						xtype: 'label',
						text: CC.msgs.df133,
						//cls: ' x-form-item'
						cls: 'x-form-item-sub'
					}, {
						//xtype: 'combo',
						xtype: 'cc:combo',
						id: 'fldXAxs',
						width: 250,
						//ccTip: '1234567890',
						//triggerAction: 'all',
						//mode: 'local',
						store: CC.Stores.ViewFields,
						valueField: 'InternalName',
						displayField: 'Title',
						//editable: false,
						//cancelSelection: false,
						listeners: {
							//scope: this,
							afterrender: function () {
								//Subscribe this swap event only once from XAxis combo. Fire it for all the combo rendered in this tab
								CC.med.subscribe({
									id: CC.util.tabsActivate('swpFld'),
									tab: CC.util.tabs().fields,
									type: CC.util.xTypes(this.getXType())
								})

								CC.med.subscribe({
									id: this.id,
									tab: CC.util.tabs().fields,
									type: CC.util.xTypes(this.getXType())
								})
							},
							beforeselect: function (c, r, i) {
								if (r.data.InternalName != c.getValue() && i > 0) {
									//CC.util.swapValues(i, c, false);
									CC.med.publish(CC.util.tabsActivate('swpFld'), { r: r, c: c, excludeCurrent: false });
								}
								else {
									//return false;
									this.cancelSelection = true;
									this.overriteValue = c.getValue();
								}
							},
							select: function (c, r, i) {
								if (!this.cancelSelection) {
									CC.med.publish(this.id, r);

									CC.util.removeCssClass(c.el.dom, [CC.util.getErrorCls()]);
								}
								else {
									//this.cancelSelection = false;
									//if (!this.overriteValue) {
									//    this.setValue('');
									//}
									//else {
									//    this.setValue(this.overriteValue);
									//}

									//this.overriteValue = '';
									//CC.util.addCssClass(c.el.dom, [CC.util.getErrorCls()]);
									CC.util.markComboError(this);
								}
							}
						}
					}, {
						xtype: 'label',
						text: CC.msgs.df134,
						//cls: ' x-form-item',
						cls: 'x-form-item-sub',
						style: {
							'padding-top': '20px'
						}
					}, {
						xtype: 'container',
						id: 'cntrYAxis'
					}, {
						//html: '<button class="linkBtn" onclick="javascript:CC.Var.util.addYAxis()">+ Add another Field</button>',
						html: '<input type="button" class="linkBtn" id="addYBtn" value="' + CC.msgs.df135 + '" onclick="javascript:CC.med.publish(CC.util.tabsActivate(\'addY\'));">',
						style: 'margin-bottom: 5px; margin-top: 2px;',
						border: false,
						listeners: {
							//scope: this,
							afterrender: function () {
								////var _addYAxis = this.addYAxis;
								////$('.linkBtn').click(function () { _addYAxis(); });
								//$('.linkBtn').click(function () {
								//    //CC.Tabs.Activate.fields.addYAxis();
								//    CC.med.publish(CC.util.tabsActivate().addY);
								//});
								CC.med.subscribe({
									id: CC.util.tabsActivate('addY'),
									tab: CC.util.tabs().fields,
									type: CC.util.xTypes('button')
								})
								$('#addYBtn').parent().css('margin-top', '2px');
							}
						}
					}
					],
					listeners: {
						scope: this,
						afterrender: function () {
							var obj = Ext.getCmp('cntrParent');
							obj.el.dom.firstChild.style.overflowY = 'auto';
						}
					}
				}
				/* Help-Icon */
				//, {
				//    id: 'dfHelp',
				//    xtype: 'CC:HelpIcon',
				//    //text: 'abcd',
				//    helpText: 'abcd'
				//    // border: false,
				//    // width: 16,
				//    // cls: CC.util.getTooltipCls(),
				//    // html: '<span class="tooltip all-images tooltipstered" style="padding-top: 1px; width: 16px; height: 16px; float: right;"></span>',
				//    // ccHelp: 'abcd'
				//}
			],
			listeners: {
				scope: this,
				afterlayout: function (p, l) {
					var ele = p.el.dom.children[0];
					//var ev = $._data(ele, 'events');

					//
					//Ensure that the event has not already been bound
					//if(!ev || !ev.scroll){
					if (!CC.util.isEventBound(ele, "scroll")) {
						$(p.el.dom.children[0]).scroll(function () {
							CC.util.closePopup(100);
						});
					}
				}
			}
		});
		//$('.linkBtn').click(addYAxis);
		//CC.Var.util.addYAxis();
		//addYAxis();

		//

		return {
			id: 'tabFields',
			xtype: 'panel',
			title: CC.msgs.df136,
			header: false,
			//iconCls: 'x-charts-icon',
			defaults: { autoScroll: false },
			items: [fields]
		}
	},



	validationStep: function () {
		var valid = true;
		return valid;
	},

	activateStep: function () {
		this.disabled = false;

		//CC.Tabs.Activate.fields.activate();
		CC.med.publish(CC.util.tabsActivate('fields'));
	}
});

CC.Tabs.Tab.Data.DataFields.Axis = Ext.extend(Ext.Container, {
	create: function (idCntr, ibBtn, seriesCount) {
		var btnId = [(CC.util.randomId(ibBtn) + "-1"), (CC.util.randomId(ibBtn) + "-2"), (CC.util.randomId(ibBtn) + "-3")];
		//var seriesCount = CC.util.getSeriesCount();

		var yContainerLength = Ext.getCmp('cntrYAxis').items ? Ext.getCmp('cntrYAxis').items.items.length : 0;
		var seriesLength = CC.med.getSetting('series') ? CC.med.getSetting('series').length - 1 : 0;

		yContainerLength = yContainerLength < seriesLength ? seriesLength : yContainerLength;

		return {
			layout: 'column',
			id: idCntr,
			style: {
				'margin': '0px 3px 0px 0px',
				'border': 'none',
				'padding-top': '5px',
				//'height': '22px'
				'height': '28px'
			},
			border: false,
			items: [{
				//xtype: 'combo',
				xtype: 'cc:combo',
				id: CC.util.randomId(ibBtn),
				//editable: false,
				width: 250,
				//mode: 'local',
				store: CC.Stores.ViewFields,
				valueField: 'InternalName',
				displayField: 'Title',
				//triggerAction: 'all',
				//cancelSelection: false,
				listeners: {
					//scope: this,
					beforeselect: function (c, r, i) {

						if (r.data.InternalName != c.getValue() && i > 0) {

							//CC.util.swapValues(i, c, true);
							CC.med.publish(CC.util.tabsActivate('swpFld'), { r: r, c: c, excludeCurrent: false });
						}
						else {
							/* don't fire the event for same selection */
							//return false;
							this.cancelSelection = true;
							this.overriteValue = c.getValue();
						}
					},
					select: function (c, r, i) {
						if (!this.cancelSelection) {
							CC.med.publish(CC.util.target('fldYAxs'), { c: c, r: r });
							CC.util.removeCssClass(c.el.dom, [CC.util.getErrorCls()]);
						}
						else {
							//this.cancelSelection = false;
							//if (!this.overriteValue) {
							//    this.setValue('');
							//}
							//else {
							//    this.setValue(this.overriteValue);
							//}
							//this.overriteValue = '';
							//CC.util.addCssClass(c.el.dom, [CC.util.getErrorCls()]);
							CC.util.markComboError(this);
						}
					}
				}
			}, {
				//html: ' <input type="button" id="' + btnId[0] + '" class="yaxis1 yaxis" onclick="javascript:CC.util.toggleCalloutPnl(event, this, 1)"/>',
				//html: ' <div id="' + btnId[0] + '" class="yaxis yaxis1" onclick="javascript:CC.util.toggleCalloutPnl(event, this, 1)"><span class="all-images btn1" style="width: 13px; height: 9px; background-position: -169px -134px;"></span></div>',
				//html: ' <div id="' + btnId[0] + '" class="yaxis yaxis1" onclick="javascript:CC.med.publish(CC.util.tabsActivate(\'yaxis\'), {e: event, sender: this});"><span class="all-images btn1" style="width: 13px; height: 9px; background-position: -169px -134px;"></span></div>',
				html: ' <div id="' + btnId[0] + '" data-cc-series="' + seriesCount + '" title="' + (yContainerLength >= 1 ? CC.tips.df134 : CC.tips.df131) + '" class="yaxis yaxis1 ' + CC.util.getTooltipCls() + '" onclick="javascript:CC.med.publish(CC.util.tabsActivate(\'yaxis\'), {e: event, sender: this});"><span class="all-images btn1" style="width: 13px; height: 9px;"></span></div>',
				border: false,
				style: 'padding-left: 20px;'
			}, {
				// html: ' <div id="' + btnId[1] + '" class="yaxis yaxis2" onclick="javascript:CC.util.toggleCalloutPnl(event, this, 2)"><span class="buttonSpan btn2" style="background-position-x: -26px;"></span></div>',
				//html: ' <div id="' + btnId[1] + '" class="yaxis yaxis2" onclick="javascript:CC.util.toggleCalloutPnl(event, this, 2)"><span class="all-images btn2" style="width: 13px; height: 9px; background-position: -195px -134px;"></span></div>',
				//html: ' <div id="' + btnId[1] + '" class="yaxis yaxis2" onclick="javascript:CC.med.publish(CC.util.tabsActivate(\'yaxis\'), {e: event, sender: this});"><span class="all-images btn2" style="width: 13px; height: 9px; background-position: -195px -134px;"></span></div>',
				html: ' <div id="' + btnId[1] + '" data-cc-series="' + seriesCount + '" title="' + (yContainerLength >= 1 ? CC.tips.df135 : CC.tips.df132) + '" class="yaxis yaxis2 ' + CC.util.getTooltipCls() + '" onclick="javascript:CC.med.publish(CC.util.tabsActivate(\'yaxis\'), {e: event, sender: this});"><span class="all-images btn2" style="width: 13px; height: 9px;"></span></div>',
				border: false //,
				//style: 'padding-left: 2px;'
			}, {
				// html: ' <div id="' + btnId[2] + '" disabled="true" class="yaxis yaxis3" onclick="javascript:CC.util.toggleCalloutPnl(event, this, 3)"><span class="buttonSpan btn3" style="background-position-x: -65px; width: 9px; margin-left: 2px;"></span></div>',
				//html: ' <div id="' + btnId[2] + '" disabled="true" class="yaxis yaxis3" onclick="javascript:CC.util.toggleCalloutPnl(event, this, 3)"><span class="all-images" style="margin-left: 2px; width: 9px; height: 9px; background-position: -234px -134px;"></span></div>',
				//html: ' <div id="' + btnId[2] + '" disabled="true" class="yaxis yaxis3" onclick="javascript:CC.med.publish(CC.util.tabsActivate(\'yaxis\'), {e: event, sender: this});"><span class="all-images" style="margin-left: 2px; width: 9px; height: 9px; background-position: -234px -134px; margin-top: 5px;"></span></div>',
				html: ' <div id="' + btnId[2] + '" data-cc-series="' + seriesCount + '" disabled="true" title="' + (yContainerLength >= 1 ? CC.tips.df136 : CC.tips.df133) + '" class="yaxis yaxis3 ' + CC.util.getTooltipCls() + '" onclick="javascript:CC.med.publish(CC.util.tabsActivate(\'yaxis\'), {e: event, sender: this});"><span class="all-images" style="margin-left: 2px; width: 9px; height: 9px; margin-top: 5px;"></span></div>',
				border: false,
				style: 'padding-left: 12px;',
				parentId: idCntr,
				listeners: {
					//scope: this,
					afterrender: function () {
						//
						CC.med.publish(CC.util.tabsActivate('yaxisRender'), { id: this.parentId });
					}
				}
			}]
			//,

			//listeners: {
			//    afterrender: function () {
			//        //if (Ext.getCmp('cntrYAxis').items.length == 1) {
			//        //    //subscribe only once

			//        //}

			//


			//    }
			//}
		};
	}
});