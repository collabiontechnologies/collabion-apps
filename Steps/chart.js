CC.Charts = function(){
	// Charts
	Types = [
		[
			[0, 'Column', 'column-chart-31x31.png', 'Bar2D.swf', CC.Data.Bar2D],
			[1, 'Stacked Column', 'column-chart-31x31.png', 'Bar2D.swf', CC.Data.Bar2D],
			[2, '100% Stacked Column', 'column-chart-31x31.png', 'Bar2D.swf', CC.Data.Bar2D],
			[3, 'Waterfall', 'column-chart-31x31.png', 'Bar2D.swf', CC.Data.Bar2D],
			[4, 'Pareto', 'column-chart-31x31.png', 'Bar2D.swf', CC.Data.Bar2D]
		],
		[
			[0, 'Bar', 'column-chart-31x31.png', 'Bar2D.swf', CC.Data.Bar2D],
			[1, 'Stacked Bar', 'column-chart-31x31.png', 'Bar2D.swf', CC.Data.Bar2D],
			[2, '100% Stacked Bar', 'column-chart-31x31.png', 'Bar2D.swf', CC.Data.Bar2D]
		],
		[
			[0, 'Line', 'column-chart-31x31.png', 'Bar2D.swf', CC.Data.Bar2D],
			[1, 'Spline', 'column-chart-31x31.png', 'Bar2D.swf', CC.Data.Bar2D]
		],
		[
			[0, 'Area', 'column-chart-31x31.png', 'Bar2D.swf', CC.Data.Bar2D],
			[1, 'Spline Area', 'column-chart-31x31.png', 'Bar2D.swf', CC.Data.Bar2D],
			[2, 'Stacked Area', 'column-chart-31x31.png', 'Bar2D.swf', CC.Data.Bar2D],
			[3, '100% Stacked Area', 'column-chart-31x31.png', 'Bar2D.swf', CC.Data.Bar2D]
		],
		[
			[0, 'Single Y Axis', 'column-chart-31x31.png', 'Bar2D.swf', CC.Data.Bar2D],
			[1, 'Dual Y Axis', 'column-chart-31x31.png', 'Bar2D.swf', CC.Data.Bar2D]
		],
		[
			[0, 'Pie', 'column-chart-31x31.png', 'Bar2D.swf', CC.Data.Bar2D],
			[1, 'Doughnut', 'column-chart-31x31.png', 'Bar2D.swf', CC.Data.Bar2D]
		],
		[
			[0, 'Pyramid', 'column-chart-31x31.png', 'Bar2D.swf', CC.Data.Bar2D],
			[0, 'Funnel', 'column-chart-31x31.png', 'Bar2D.swf', CC.Data.Bar2D],
			[0, 'Marimekko', 'column-chart-31x31.png', 'Bar2D.swf', CC.Data.Bar2D]
			
		]
		
	]

	TypeFields = ['type', 'name', 'img', 'swf', 'config'];
	CatFields = ['id', 'name', 'img'];

	// Chart categories
	Categories = [
		[0, 'Column', 'Column-25x25.png'],
		[1, 'Bar', 'Column-25x25.png'],
		[2, 'Line', 'Column-25x25.png'],
		[3, 'Area', 'Column-25x25.png'],
		[4, 'Combo', 'Column-25x25.png'],
		[5, 'Pie', 'Column-25x25.png'],
		[6, 'Others', 'Column-25x25.png'] //,
		// [7, 'Column', 'Column.png'],
		// [8, 'Bar', 'Column.png'],
		// [9, 'Line', 'Column.png'],
		// [10, 'Area', 'Column.png'],
		// [11, 'Combo', 'Column.png'],
		// [12, 'Pie', 'Column.png'],
		// [13, 'Others', 'Column.png']
	];	
};

CC.Charts.prototype = {
    // Return TypeFields
    getTypeFields: function () {
        return TypeFields;
    },

    getCategories: function () {
        return Categories;
    },

    // Get charts by category
    // category - number in array
    getTypesByCategory: function (category) {
        return Types[category];
    },	
};

CC.Tabs.Tab.Chart = Ext.extend(CC.Tabs.Base, {
    valid: true,
	
	create: function () {
		//
		// Configuration for Wizard
		//
		this.config = {
			ChartIconsPath: 'assets/'
		};
		
		this.Chart = new CC.Charts();
		
				
	    this.prevChrtCat = -1;
	    this.prevChrtTyp = -1;

	    this.ct_store_charts = new Ext.data.ArrayStore({
			fields: TypeFields,
			data: this.Chart.getTypesByCategory(0)
		});

		this.ct_store_chartsCategories = new Ext.data.ArrayStore({
			fields: CatFields,
			data: this.Chart.getCategories()
		});		
		
		// var ctTemplateCat = new Ext.XTemplate(
			// '<tpl for=".">',
				// '<div class="cc-thumb-wrap-cat" title="{name}">',
					// '<div class="cc-thumb"><img src="' + this.config.ChartIconsPath + '{img}"></div>',
					// '<div class="cc-thumb-label"><div>{name}</div><div class="aux"></div></div>',
					// '<div class="cc-thumb-downArw"><img src="' + this.config.ChartIconsPath + 'down-arrow.png"></div>',
				// '</div>',
			// '</tpl>',
			// '<div class="x-clear"></div>'
		// );
		
		var ctTemplateCat = new Ext.XTemplate(
			'<ul id="my-list">',
				'<tpl for=".">',
					//'<li style="width:60px; height:100%; text-align: center; padding: 0px 10px 0px 10px;">',
					'<li style="width:42px; height:100%; text-align: center; padding: 0px 10px 0px 10px;">',
						'<div class="cc-thumb-wrap-cat tooltip" title="{name}">',
							//'<div class="cc-thumb-cat"><img src="' + this.config.ChartIconsPath + '{img}" alt="{name}" title="{name}" /></div>',
							'<div class="cc-thumb-cat"><img src="' + this.config.ChartIconsPath + '{img}" alt="{name}" /></div>',
							'<div class="x-form-item">{name}</div>',
							'<div class="cc-thumb-downArw"><img src="' + this.config.ChartIconsPath + 'down-arrow-20x17.png"></div>',
						'</div>',
					'</li>',
				'</tpl>',
			'</ul>'
		);

		var ctTemplateChart = new Ext.XTemplate(
			'<tpl for=".">',
				'<div class="cc-thumb-wrap tooltip" title="{name}">',
					'<div class="cc-thumb-FixWid"><img src="' + this.config.ChartIconsPath + '{img}"></div>',
					'<div class="cc-thumb-label"><div class="x-form-item">{name}</div><div class="aux"></div></div>',
				'</div>',
			'</tpl>',
			'<div class="x-clear"></div>'
		);		
		
		this.chartCatogories = new Ext.DataView({
			id: 'pnlCat',
			store: this.ct_store_chartsCategories,
			tpl: ctTemplateCat,
			//itemSelector: 'div.cc-thumb-wrap',
			itemSelector: 'div.cc-thumb-wrap-cat',
			overClass: 'cc-x-view-over-noborder',
			singleSelect: true ,
			listeners: {
				selectionchange: this.switchChrtCat,
				afterrender: function(){
					//debugger;
					$('#my-list').hoverscroll({
						fixedArrows: false,
						rtl: false,
						arrows: true,
						vertical: false,
						arrowsOpacity: 1,
						width: 620,
						height: '100%',
						debug: false,
						disableTrigger: false,
						triggerSpeed: 8,
						triggerMax: 88
					});
				},
				scope: this
			}
		});

		this.chartTypes = new Ext.DataView({
			id: 'pnlCType',
			store: this.ct_store_charts,
			tpl: ctTemplateChart,
			itemSelector: 'div.cc-thumb-wrap',
			//overClass: 'cc-x-view-over',
			overClass: 'cc-x-view-over-noborder',
			autoScroll: true,
			singleSelect: true,
			listeners: {
		        selectionchange: this.switchChrtTyp,
				scope: this
			}
		});
		
		var chartContent = {
			id: 'chartContent',
			border: false,
			autoScroll: false,
			layout: 'vbox',
			items: [this.chartCatogories, 
			{
				xtype: 'panel',
				id: 'pnlChartTypes',
				autoScroll: true,
				height: 180,
				border: false,
				items: [this.chartTypes]
			}],
			listeners: {
				scope: this,
				resize: function(a){
					//alter the height of the second panel to enable or disable the scroll bar.
					//84 is the height of the first panel and 10 is the top margin
					Ext.getCmp('pnlChartTypes').setHeight(a.el.getHeight() - 84 - 10);
					Ext.getCmp('pnlChartTypes').doLayout();
					//debugger;
					//Ext.getCmp('pnlChartTypes').setHeight(a.el.getHeight() - Ext.getCmp('pnlCat').getHeight() - parseInt($('#pnlChartTypes').css('marginTop')));
                },
				afterrender: function(){
					//removing the bottom-padding to adjust the charts in a single line. Otherwise a scroll-bar will appear.
					//after setting the padding also need to set the height. The height will be 5 less than its parent because of 5px top padding
					$('#chartContent').parent().css({
						"padding-bottom": "0px",
						"height": ($('#chartContent').parent().parent().height() - 5)
					});
					//debugger;
					//Ext.getCmp('pnlChartTypes').setHeight(a.el.getHeight() - 84 - 10);
                    //Ext.getCmp('pnlChartTypes').doLayout();
                    Ext.getCmp('chartContent').doLayout();
				}
			}
					
		};
		
		return {
            id: 'pnlCharts',
            title: 'CHART',
			header: false,
			padding: 5,
			iconCls: 'x-charts-icon',
            defaults: { autoScroll: false },
			valid: this.valid,
			items: [chartContent] //,
			//listeners:{
			//	scope: this,
			//	afterrender: function(){
					//$('.tooltip').tooltipster(
					//{
						//theme: 'cc-tooltipster-shadow'
					//}
					//);
			//	}
			//}
        }
    },
	
	validationStep: function () {
		//return this.valid;
		return Ext.getCmp('pnlCharts').initialConfig.valid;
    },
	
	activateStep: function () {
		this.disabled = false;
	},
	
	switchChrtTyp: function(){
	    selrec = this.chartTypes.getSelectedRecords();

	    if (selrec.length > 0) {
	        //Do something
	        var thisType = selrec[0].data.type;
			this.prevChrtTyp = thisType;
	    }
	    else if (this.prevChrtTyp >= 0)
	        this.chartTypes.select(this.prevChrtTyp, false, true);
		
		Ext.getCmp('pnlCharts').initialConfig.valid = true;
	},

	switchChrtCat: function () {
	    selrec = this.chartCatogories.getSelectedRecords();

	    if (selrec.length > 0) {
	        var thisId = selrec[0].data.id;
			this.ct_store_charts.loadData(this.Chart.getTypesByCategory(thisId));
	        this.prevChrtCat = thisId;
	        this.prevChrtTyp = -1;
	    }
	    else if (this.prevChrtCat >= 0)
	        this.chartCatogories.select(this.prevChrtCat, false, true);
		//this.ct_store_charts.loadData(this.Chart.getTypesByCategory(1));
		
		Ext.getCmp('pnlCharts').initialConfig.valid = false;
		
		util.generateTip();
	}
	
	
});